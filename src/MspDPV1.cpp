#include <iostream>
#include <syslog.h>
#include <sys/types.h>
#include <fcntl.h> 
#include <unistd.h>
#include <termios.h>
#include <string.h>
#include <math.h>
#include <array>
#include "modal_journal.h"

// MPA / CJSON
#include <modal_pipe_interfaces.h>

#include "mavlink_to_msp_dp.h"
#include "msp_dp_defines.h"
#include "config_file_msp.h"
#include "MspDPV1.h"
#include <cstring>
#include "msp_osd_symbols.h"

#include <map>
#include <iomanip>
#include "msp_dp_defines.h"

// MSP state machine states
#define MSP_IDLE            0
#define MSP_HEADER_START    1
#define MSP_HEADER_M        2
#define MSP_DIRECTION       3
#define MSP_PAYLOAD_SIZE    4
#define MSP_COMMAND_ID      5
#define MSP_PAYLOAD_V1      6
#define MSP_CHECKSUM_V1     7



// Function to reset MSP port
void MspDPV1::reset_msp_port() {
    mspPort.packetState = MSP_IDLE;
    mspPort.checksum1 = 0;
    mspPort.checksum2 = 0;
}

MspDPV1::MspDPV1(bool input_only)
{
	if(!input_only) load_config_file(&m_parameters);

	// print_config_file(m_parameters);
	// Initialize the receiveRunning variable
    receiveRunning.store(true);

	if(!setup_comms()) throw -EINVAL;

	// MenuManager Disabled for camera server implementation (minimal processing done here in order to not add latency to video streams)
	// m_menu_manger = std::make_unique<MenuManager>(m_parameters);

	if(!input_only){
		const auto clear_osd_msg = msp_dp_osd::construct_OSD_clear();
		Send(MSP_CMD_DISPLAYPORT, &clear_osd_msg, MSP_DIRECTION_REPLY);
	
		const auto osd_canvas_msg = msp_dp_osd::construct_OSD_canvas(column_max[m_parameters.resolution], row_max[m_parameters.resolution]);
		Send(MSP_SET_OSD_CANVAS, &osd_canvas_msg, MSP_DIRECTION_REPLY);
	
		const auto osd_config_msg = msp_dp_osd::construct_OSD_config(m_parameters.resolution, m_parameters.font_type);
		Send(MSP_CMD_DISPLAYPORT, &osd_config_msg, MSP_DIRECTION_REPLY);
	}

	sleep(1);
}

MspDPV1::~MspDPV1() {
	if (m_input_fd > 0) {
		close(m_input_fd);
	}
	if (m_output_fd > 0) {
		close(m_output_fd);
	}

	pipe_server_close_all();
	pipe_client_close_all();
}

void MspDPV1::stopReceive() {
    receiveRunning.store(false);
}

void MspDPV1::receive() {
    M_DEBUG("Starting to receive data on the input UART port...\n");
    if (mspPort.port_fd <= 0) {
        M_ERROR("Invalid file descriptor for UART input port. Port FD: %i\n",mspPort.port_fd);
        return;
    }

    reset_msp_port();
    while (receiveRunning) {	
        read_uart(); // Read data from the UART
        usleep(1000); // Sleep to prevent CPU overuse
    }
	M_DEBUG("Receive function stopped.\n");
}

bool MspDPV1::setup_comms() {
    // Check if both UART and MPA are set as input, which is invalid
    if (m_parameters.input_type == (INPUT_MPA | INPUT_UART)) {
        M_ERROR("Only one input at a time: UART or MPA.\n");
        return false;
    }

    // Ensure the input port is correctly configured
    if (m_parameters.input_type == INPUT_UART) {
        if (m_parameters.input_port == nullptr || strlen(m_parameters.input_port) == 0) {
            M_ERROR("Input UART port is not specified!\n");
            return false;
        }

        M_DEBUG("Opening input UART: %s\n", m_parameters.input_port);
        m_input_fd = open_uart(m_parameters.input_port);  
        if (m_input_fd < 0) {
            M_ERROR("Failed to open input UART port: %s\n", m_parameters.input_port);
            return false;
        } else {
            M_DEBUG("Successfully opened input UART port: %s with file descriptor: %i\n", m_parameters.input_port, m_input_fd);
            mspPort.port_fd = m_input_fd;  
        }
    }

    // Ensure the output port is correctly configured
    if (m_parameters.output_type & OUTPUT_UART) {
        if (m_parameters.output_port == nullptr || strlen(m_parameters.output_port) == 0) {
            M_ERROR("Output UART port is not specified!\n");
            return false;
        }

        M_DEBUG("Opening output UART: %s\n", m_parameters.output_port);
        m_output_fd = open_uart(m_parameters.output_port); 
        if (m_output_fd < 0) {
            M_ERROR("Failed to open output UART port: %s\n", m_parameters.output_port);
            return false;
        } else {
            M_DEBUG("Successfully opened output UART port: %s with file descriptor: %i\n", m_parameters.output_port, m_output_fd);
        }
    }

    // Handle MPA output setup
    if (m_parameters.output_type & OUTPUT_MPA) {
        M_DEBUG("Setting up MPA output pipe: %s\n", MSP_OUT_PIPE_NAME);
        pipe_info_t info;
        init_output_pipe_location(&info, MSP_PIPE_LOCATION, MSP_PROCESS_NAME);
        if (pipe_server_create(MSP_OUT_PIPE_CH, info, 0)) {
            M_ERROR("[%s] Failed to create pipe: %s\n," MSP_PROCESS_NAME, MSP_OUT_PIPE_NAME);
            return false;
        }
    }

    return true;
}

int MspDPV1::open_uart(char* device){
	int fd{-1};
	struct termios t;
	fd = open(device, O_RDWR | O_NONBLOCK); 

	if (fd  < 0) {
		M_ERROR("Failed to open port: %s\n", device); 
		return -1;
	}

	// Creates and sets the parameters for the UART port
	tcgetattr(fd , &t);
	cfsetspeed(&t, B115200); // baud = 115200 
	t.c_cflag &= ~(CSTOPB | PARENB | CRTSCTS);
	t.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
	t.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK | INPCK | ISTRIP | IXON);
	t.c_oflag = 0;
	tcsetattr(fd, TCSANOW, &t); // sets the parameters
	return fd;
}

void MspDPV1::processIncomingByte(uint8_t byte) {
    static int byteCount = 0;
    byteCount++;

    switch (mspPort.packetState) {

        // Start Byte Case
        case MSP_IDLE:
            if (byte == '$') {  // Start byte
                mspPort.packetState = MSP_HEADER_START;
            }
            break;

        // Header Byte Case
        case MSP_HEADER_START:
            if (byte == 'M') {  // Header byte, expected to be 'M'
                mspPort.packetState = MSP_DIRECTION;
            } else {
                mspPort.packetState = MSP_IDLE;  // Reset to idle if incorrect header
            }
            break;

        // Direction Byte Case
        case MSP_DIRECTION:
            if (byte == '>' || byte == '<') {  // Direction byte, either '>' (response) or '<' (command)
                mspPort.packetState = MSP_PAYLOAD_SIZE;
            } else {
                mspPort.packetState = MSP_IDLE;  // Reset to idle if invalid direction
            }
            break;

        // Payload Size Case
        case MSP_PAYLOAD_SIZE:
            mspPort.dataSize = byte;  // Payload size (actual payload bytes)
            mspPort.checksum1 = byte;  // Initialize checksum with payload size
            mspPort.packetState = MSP_COMMAND_ID;
            break;

        // Command ID Case
        case MSP_COMMAND_ID:
            mspPort.cmdMSP = byte;  // Command ID (the actual message ID)
            mspPort.checksum1 ^= byte;  // Update checksum
            mspPort.packetState = mspPort.dataSize == 0 ? MSP_CHECKSUM_V1 : MSP_PAYLOAD_V1;
            mspPort.offset = 0;
            break;

        // Payload Case
		case MSP_PAYLOAD_V1:
			if (byte == '$') {  // Detect new header in the middle of payload
				M_ERROR("New header detected in the middle of payload, resetting state!\n");
				mspPort.packetState = MSP_HEADER_START;
				return;
			}

			mspPort.inBuf[mspPort.offset++] = byte;  // Store incoming payload byte
			mspPort.checksum1 ^= byte;  // Update checksum with each byte
			
			// When payload is complete, transition to the checksum verification
			if (mspPort.offset == mspPort.dataSize) {
				mspPort.packetState = MSP_CHECKSUM_V1;
			}
			break;


        // Checksum Case
        case MSP_CHECKSUM_V1:
            if (mspPort.checksum1 == byte) {  // Compare calculated checksum with received byte
                processMessage();  // Call message processing handler
            } else {
				M_ERROR("MSP checksum error! Expected: 0x%X but got: 0x%X\n", (int)mspPort.checksum1, (int)byte);
            }
            // Always transition back to MSP_IDLE after processing a message
            mspPort.packetState = MSP_IDLE;
            break;

        default:
            M_ERROR("Unknown state, resetting to MSP_IDLE\n");
            mspPort.packetState = MSP_IDLE;
            break;
    }
}

void MspDPV1::processMessage() {
	int subcmd{-1};
    switch (mspPort.cmdMSP) {
        case MSP_RC:
			break;
		case MSP_CMD_DISPLAYPORT:
			subcmd = (int)mspPort.inBuf[0];
			if(subcmd == (uint8_t)MSP_DP_WRITE_STRING){
				if(clear_screen) {
					reset_osd();
					clear_screen = false;
				}
				if (mspPort.dataSize >= sizeof(msp_dp_cmd_t)) {
					uint8_t row = mspPort.inBuf[1];
					uint8_t col = mspPort.inBuf[2];
					std::string receivedString;

					// Build the received string
					for (int i = sizeof(msp_dp_cmd_t); i < mspPort.dataSize; ++i) {
						receivedString += (char) mspPort.inBuf[i];
					}

					// Update displayDataMap and print debug information
					if (!receivedString.empty()) {
						// M_DEBUG("Row: %d\tCol: %d\tPayload: %s\n", row, col, receivedString.c_str());
						displayDataMap[{row, col}] = receivedString;
					}
				}
			} else if (subcmd == MSP_DP_CLEAR_SCREEN){
				if(time_monotonic_ns()-last_clear > 500000000 || betaflight_menu){
					clear_screen = true;
					last_clear = time_monotonic_ns();
				}
			} else if (subcmd == MSP_DP_CONFIG){
				// M_DEBUG("Received MSP_DP_CONFIG command\n");
			} else if (subcmd == MSP_DP_DRAW_SCREEN){
				// M_DEBUG("Received DRAW command\n");
			}
            break;
		case MSP_SET_OSD_CANVAS:
			msp_dp_canvas_t osd_canvas;
			osd_canvas.row_max = mspPort.inBuf[0];
			osd_canvas.col_max = mspPort.inBuf[1];
			set_osd_canvas(osd_canvas);
			break;
		case MSP_STATUS:
			if(mspPort.inBuf[6] == MSP_ARMED) heartbeat_msg->base_mode |= MAV_MODE_FLAG_SAFETY_ARMED;
			else heartbeat_msg->base_mode &= ~MAV_MODE_FLAG_SAFETY_ARMED;
			Send(MSP_STATUS, mspPort.inBuf, MSP_DIRECTION_REPLY);
			break;

        default:
           M_ERROR("Unknown MSP command: %i\n", (int)mspPort.cmdMSP);
            break;
    }
	reset_msp_port();
}

void MspDPV1::read_uart() {
    uint8_t buffer[128];
    int bytesRead = read(m_input_fd, buffer, sizeof(buffer)); // Read available bytes
    if (bytesRead > 0) {
        for (int i = 0; i < bytesRead; ++i) {
            processIncomingByte(buffer[i]);
        }
    } 
}

void MspDPV1::init_output_pipe_location(pipe_info_t* info, char* pipe_location, const char* process) {
  std::strcpy(pipe_location, MODAL_PIPE_DEFAULT_BASE_DIR);
  std::strcat(pipe_location, MSP_OUT_PIPE_NAME);
  std::strncpy(info->name, MSP_OUT_PIPE_NAME, 32);
  std::strncpy(info->location, pipe_location, 64);
  std::strncpy(info->type, MSP_OUT_DATA_TYPE, 32);
  std::strncpy(info->server_name, process, 32);
  info->size_bytes = MSP_RECOMMENDED_PIPE_SIZE;
};

int MspDPV1::get_rssi_dbm(){
    float rssi_dbm{UINT8_MAX};
    FILE* fp;
    if((fp = popen("px4-listener input_rc", "r")) == NULL) {
        perror("Error opening pipe from px4 listener");
		return rssi_dbm;
    }
    char buf[PX4_LISTENER_BUFSIZE];
    while(fgets(buf, PX4_LISTENER_BUFSIZE, fp) != NULL) {
		if(strncmp("    rssi_dbm:", buf, strlen("    rssi_dbm:")) == 0){
            sscanf(buf, "    rssi_dbm:%f", &rssi_dbm);
        }
		else if(strncmp("\trssi_dbm:", buf, strlen("\trssi_dbm:")) == 0){
            sscanf(buf, "\trssi_dbm:%f", &rssi_dbm);
        }
    }
    if(std::isnan(rssi_dbm)) rssi_dbm = 255;
	pclose(fp);
	return (int)rssi_dbm;
}

void MspDPV1::process_data() {
	const auto clear_osd_msg = msp_dp_osd::construct_OSD_clear();
	Send(MSP_CMD_DISPLAYPORT, &clear_osd_msg, MSP_DIRECTION_REPLY);
	
	/* MenuManager Disabled for camera server implementation (minimal processing done here in order to not add latency to video streams)
	// Handle already in menu state
	if (heartbeat_msg && !is_armed(heartbeat_msg) && m_menu_manger->menu_is_active()){
		// Populate msp_rc_t struct for us to use
		const auto msp_rc_msg = msp_dp_osd::construct_RC(rc_msg, m_sticks);
		// Update menu as needed, only allow one menu at at time!
		m_menu_manger->handle_menu(m_sticks);
		return;
	} 

	// Check for Menu before doing anything else - If we want the menu then we don't want OSD elements clobbering it 
	if(heartbeat_msg && !is_armed(heartbeat_msg) && m_menu_manger->check_for_menu(rc_msg)){
		// Send RC channel values and populate msp_rc_t struct for us to use
		const auto msp_rc_msg = msp_dp_osd::construct_RC(rc_msg, m_sticks);
		Send(MSP_RC, &msp_rc_msg, MSP_DIRECTION_REPLY);

		// Handle showing initial menu state
		m_menu_manger->handle_menu(m_sticks, true);
		return;
	} 
	*/

	// VEHICLE STATUS / DISARMED / ERROR MESSAGES / FLIGHT MODE
	{
		// RC CHANNELS / RSSI
		if (rc_msg) {
			// Send RC channel values
			const auto msp_rc_msg = msp_dp_osd::construct_RC(rc_msg, m_sticks);
			Send(MSP_RC, &msp_rc_msg, MSP_DIRECTION_REPLY);

			if (m_parameters.rssi_col != -1 && m_parameters.rssi_row != -1){
				char rssi[5];
				snprintf(rssi, sizeof(rssi), "%c%d", SYM_RSSI, get_rssi_dbm());
				uint8_t rssi_output[sizeof(msp_dp_cmd_t) + sizeof(rssi)+1]{0};	
				msp_dp_osd::construct_OSD_write(m_parameters.rssi_col, m_parameters.rssi_row, false, rssi, rssi_output, sizeof(rssi_output));	
				Send(MSP_CMD_DISPLAYPORT, &rssi_output, MSP_DIRECTION_REPLY);
			}
		}

		// Vehicle Status
		if (heartbeat_msg) {

			// Clear the screen

			const auto status_msg = msp_dp_osd::construct_status(heartbeat_msg);
			Send(MSP_STATUS, &status_msg, MSP_DIRECTION_REPLY);

			// DISARMED Message -> BOTTOM-MIDDLE TOP
			if (!(heartbeat_msg->base_mode & MAV_MODE_FLAG_SAFETY_ARMED)
				&& m_parameters.disarmed_row != -1 && m_parameters.disarmed_col != -1) {
					const char* disarmed_msg = "DISARMED";
					uint8_t disarmed_output[sizeof(msp_dp_cmd_t) + sizeof(disarmed_msg)+1]{0};	// size of output buffer is size of OSD display port command struct and the buffer you want shown on OSD
					msp_dp_osd::construct_OSD_write(m_parameters.disarmed_col, m_parameters.disarmed_row, false, disarmed_msg, disarmed_output, sizeof(disarmed_output));	
					Send(MSP_CMD_DISPLAYPORT, &disarmed_output, MSP_DIRECTION_REPLY);
			}

			// Flight Mode -> BOTTOM-MIDDLE BOTTOM
			if (m_parameters.flight_mode_col != -1 && m_parameters.flight_mode_row != -1){
				const auto flight_mode = msp_dp_osd::construct_flight_mode(heartbeat_msg);
				uint8_t flight_mode_output[sizeof(msp_dp_cmd_t) + sizeof(flight_mode)+1]{0};	
				msp_dp_osd::construct_OSD_write(m_parameters.flight_mode_col, m_parameters.flight_mode_row, false, flight_mode, flight_mode_output, sizeof(flight_mode_output));	
				Send(MSP_CMD_DISPLAYPORT, &flight_mode_output, MSP_DIRECTION_REPLY);
			}
			if (attitude_msg ) {

				// STATUS MESSAGE -> BOTTOM-MIDDLE MIDDLE (PX4 error messages)
				// FLIGHT MODE | ARMING | HEADING ....  WILL PRINT PX4 ERROR MESSAGES FOR 30 SECONDS THEN RESET to above format
				if (m_parameters.status_col != -1 && m_parameters.status_row != -1) {
					m_display.set(msp_osd::MessageDisplayType::HEADING, "N");
					const auto display_msg = msp_dp_osd::construct_display_message(heartbeat_msg, 
													attitude_msg, 
													ping_msg, 
													status_text_msg, 
													m_param_osd_log_level, 
													m_display);
					uint8_t display_msg_output[sizeof(msp_dp_cmd_t) + sizeof(display_msg.craft_name)+1]{0};	
					msp_dp_osd::construct_OSD_write(m_parameters.status_col, m_parameters.status_row, false, display_msg.craft_name, display_msg_output, sizeof(display_msg_output));	// display_msg max size (w/o warning) is 15
					Send(MSP_CMD_DISPLAYPORT, &display_msg_output, MSP_DIRECTION_REPLY);
				}
			}
		}
	}

	// VIO quality
	if(m_parameters.vio_col != -1 && m_parameters.vio_row != -1){

		// "VIO" header
		uint8_t vio_header_output[sizeof(msp_dp_cmd_t) + sizeof("VIO")]{0};	
		msp_dp_osd::construct_OSD_write(m_parameters.vio_col, m_parameters.vio_row, false, "VIO", vio_header_output, sizeof("VIO"));
		Send(MSP_CMD_DISPLAYPORT, &vio_header_output, MSP_DIRECTION_REPLY);

		// Place integer value indicating quality value below "VIO"
		std::string vio_quality = m_vio_quality < 0 ? std::to_string(-1) : std::to_string(m_vio_quality /10);
		uint8_t vio_quality_output[sizeof(msp_dp_cmd_t) + vio_quality.size() + 1]{0};	
		msp_dp_osd::construct_OSD_write(m_parameters.vio_col+1, m_parameters.vio_row+1, false, vio_quality.c_str(), vio_quality_output, vio_quality.size());
		Send(MSP_CMD_DISPLAYPORT, &vio_quality_output, MSP_DIRECTION_REPLY);

		// VIO Quality bar
		std::string vio_quality_bar;
		if (m_vio_quality/10 < 1){
			vio_quality_bar = {SYM_PB_START, SYM_PB_EMPTY, SYM_PB_EMPTY, SYM_PB_EMPTY, SYM_PB_CLOSE};	
		} else if (m_vio_quality/10 < 4){
			vio_quality_bar = {SYM_PB_START, SYM_PB_FULL, SYM_PB_EMPTY, SYM_PB_EMPTY, SYM_PB_CLOSE};	
		} else if (m_vio_quality/10 < 7){
			vio_quality_bar = {SYM_PB_START, SYM_PB_FULL, SYM_PB_FULL, SYM_PB_EMPTY, SYM_PB_CLOSE};	
		} else {
			vio_quality_bar = {SYM_PB_START, SYM_PB_FULL, SYM_PB_FULL, SYM_PB_FULL, SYM_PB_CLOSE};	
		}
		
		uint8_t vio_quality_bar_output[sizeof(msp_dp_cmd_t) + vio_quality_bar.size() + 1]{0};	
		msp_dp_osd::construct_OSD_write(m_parameters.vio_col-1, m_parameters.vio_row+2, false, vio_quality_bar.c_str(), vio_quality_bar_output, vio_quality_bar.size());
		Send(MSP_CMD_DISPLAYPORT, &vio_quality_bar_output, MSP_DIRECTION_REPLY);
	}


	// // BATTERY / CURRENT DRAW
	if (battery_msg) {

		// Full battery voltage
		if(m_parameters.battery_col != -1 && m_parameters.battery_row != -1){
			char batt[8];
			float voltage = 0;
			int num_cells = 0;
			for (int i = 0; i < 10; i++) {
				if (battery_msg->voltages[i] == 65535) { // junk val, ignore
					continue;
				} else {
					voltage += battery_msg->voltages[i];
					num_cells += 1;
				}
			}
			voltage *= .001; // scale from mV to V

			snprintf(batt, sizeof(batt), "%c%.2f%c", SYM_BATT_FULL, static_cast<double>(voltage), SYM_VOLT);
			uint8_t battery_output[sizeof(msp_dp_cmd_t) + sizeof(batt)+1]{0};	
			msp_dp_osd::construct_OSD_write(m_parameters.battery_col, m_parameters.battery_row, false, batt, battery_output, sizeof(battery_output));	
			Send(MSP_CMD_DISPLAYPORT, &battery_output, MSP_DIRECTION_REPLY);


			// Per cell battery voltage
			char batt_cell[7];
			snprintf(batt_cell, sizeof(batt_cell), "%c%.2f%c", SYM_BATT_FULL, static_cast<double>(voltage / num_cells), SYM_VOLT);
			uint8_t batt_cell_output[sizeof(msp_dp_cmd_t) + sizeof(batt_cell)+1]{0};	
			msp_dp_osd::construct_OSD_write(m_parameters.cell_battery_col, m_parameters.cell_battery_row, false, batt_cell, batt_cell_output, sizeof(batt_cell_output));	
			Send(MSP_CMD_DISPLAYPORT, &batt_cell_output, MSP_DIRECTION_REPLY);
		}

		// Current draw
		if(m_parameters.current_draw_col != -1 && m_parameters.current_draw_row != -1){
			char current_draw[8];
			snprintf(current_draw, sizeof(current_draw), "%.3f%c", static_cast<double>(battery_msg->current_battery) * static_cast<double>(0.01), SYM_AMP);
			uint8_t current_draw_output[sizeof(msp_dp_cmd_t) + sizeof(current_draw)+1]{0};	
			msp_dp_osd::construct_OSD_write(m_parameters.current_draw_col, m_parameters.current_draw_row, false, current_draw, current_draw_output, sizeof(current_draw_output));	
			Send(MSP_CMD_DISPLAYPORT, &current_draw_output, MSP_DIRECTION_REPLY);
		}
	}

	// GPS LAT/LONG
	if (global_pos_msg) {

		// GPS Longitude 
		if(m_parameters.longitude_col != -1 && m_parameters.longitude_row != -1){
			char longitude[11];
			snprintf(longitude, sizeof(longitude), "%c%.7f", SYM_LON, static_cast<double>(global_pos_msg->lon)*1e-7);
			uint8_t longitude_output[sizeof(msp_dp_cmd_t) + sizeof(longitude)+1]{0};	
			msp_dp_osd::construct_OSD_write(m_parameters.longitude_col, m_parameters.longitude_row, false, longitude, longitude_output, sizeof(longitude_output));	
		}

		// GPS Latitude
		if(m_parameters.latitude_col != -1 && m_parameters.latitude_row != -1){
			char latitude[11];
			snprintf(latitude, sizeof(latitude), "%c%.7f", SYM_LAT, static_cast<double>(global_pos_msg->lat)*1e-7);
			uint8_t latitude_output[sizeof(msp_dp_cmd_t) + sizeof(latitude)+1]{0};	
			msp_dp_osd::construct_OSD_write(m_parameters.latitude_col, m_parameters.latitude_row, false, latitude, latitude_output, sizeof(latitude_output));	
			Send(MSP_CMD_DISPLAYPORT, &latitude_output, MSP_DIRECTION_REPLY);
		}
	}

	// DIR/DIST TO HOME/HEADING ANGLE
	if (global_pos_msg) {
		int16_t distance_to_home{0};
		int16_t bearing_to_home{SYM_ARROW_NORTH};

		// Calculate distance and direction to home
		if (global_pos_msg != NULL && home_position_msg != NULL) {
			float bearing_to_home_f = get_bearing_to_next_waypoint(global_pos_msg->lat,
								global_pos_msg->lon,
								home_position_msg->latitude, home_position_msg->longitude) * (PI / 180); // converts to radians

			if (bearing_to_home < 0) {
				bearing_to_home += 360.0f;
			}

			float distance_to_home_f = get_distance_to_next_waypoint(global_pos_msg->lat,
								global_pos_msg->lon,
								home_position_msg->latitude, home_position_msg->longitude);

			distance_to_home = (int16_t)distance_to_home_f; // meters
			bearing_to_home = msp_dp_osd::get_symbol_from_bearing(bearing_to_home_f);
		} 
	
		// Direction/Distance to home
		if(m_parameters.to_home_col != -1 && m_parameters.to_home_row != -1){
			char to_home[8];
			snprintf(to_home, sizeof(to_home), "%c%c%i%c", bearing_to_home, SYM_HOMEFLAG, distance_to_home, SYM_M);
			uint8_t to_home_output[sizeof(msp_dp_cmd_t) + sizeof(to_home)+1]{0};	
			msp_dp_osd::construct_OSD_write(m_parameters.to_home_col, m_parameters.to_home_row, false, to_home, to_home_output, sizeof(to_home_output));	
			Send(MSP_CMD_DISPLAYPORT, &to_home_output, MSP_DIRECTION_REPLY);
		}

		// Heading Angle
		if(m_parameters.heading_col != -1 && m_parameters.heading_row != -1){
			char heading[5];
			snprintf(heading, sizeof(heading), "%c%i", bearing_to_home, (int16_t)global_pos_msg->yaw);
			uint8_t heading_output[sizeof(msp_dp_cmd_t) + sizeof(heading)+1]{0};	
			msp_dp_osd::construct_OSD_write(m_parameters.heading_col, m_parameters.heading_row, false, heading, heading_output, sizeof(heading_output));	
			Send(MSP_CMD_DISPLAYPORT, &heading_output, MSP_DIRECTION_REPLY);
		}
	}

	// CROSSHAIRS
	if(m_parameters.crosshair_col != -1 && m_parameters.crosshair_row != 1){
		char crosshair[4] = {SYM_AH_CENTER_LINE, SYM_AH_CENTER, SYM_AH_CENTER_LINE_RIGHT, '\0'};
		uint8_t crosshair_output[sizeof(msp_dp_cmd_t) + sizeof(crosshair)]{0};	
		msp_dp_osd::construct_OSD_write(m_parameters.crosshair_col, m_parameters.crosshair_row, false, crosshair, crosshair_output, sizeof(crosshair_output));	
		Send(MSP_CMD_DISPLAYPORT, &crosshair_output, MSP_DIRECTION_REPLY);		
	}
	
	const auto osd_canvas_msg = msp_dp_osd::construct_OSD_canvas((int)column_max[m_parameters.resolution], (int)row_max[m_parameters.resolution]);
	Send(MSP_SET_OSD_CANVAS, &osd_canvas_msg, MSP_DIRECTION_REPLY);
	const auto osd_config_msg = msp_dp_osd::construct_OSD_config(m_parameters.resolution, m_parameters.font_type);
	Send(MSP_CMD_DISPLAYPORT, &osd_config_msg, MSP_DIRECTION_REPLY);

	// DRAW whole screen
	displayportMspCommand_e draw{MSP_DP_DRAW_SCREEN};
	Send(MSP_CMD_DISPLAYPORT, &draw, MSP_DIRECTION_REPLY);
}


struct msp_message_descriptor_t {
	uint8_t message_id;
	bool fixed_size;
	uint8_t message_size;
};

#define MSP_DESCRIPTOR_COUNT 16
const msp_message_descriptor_t msp_message_descriptors[MSP_DESCRIPTOR_COUNT] = {
	{MSP_OSD_CONFIG, true, sizeof(msp_osd_config_t)},
	{MSP_NAME, true, sizeof(msp_name_t)},
	{MSP_ANALOG, true, sizeof(msp_analog_t)},
	{MSP_STATUS, true, sizeof(msp_dp_status_t)},
	{MSP_BATTERY_STATE, true, sizeof(msp_battery_state_t)},
	{MSP_RAW_GPS, true, sizeof(msp_raw_gps_t)},
	{MSP_ATTITUDE, true, sizeof(msp_attitude_t)},
	{MSP_ALTITUDE, true, sizeof(msp_altitude_t)},
	{MSP_COMP_GPS, true, sizeof(msp_comp_gps_t)},
	{MSP_ESC_SENSOR_DATA, true, sizeof(msp_esc_sensor_data_dji_t)},
	{MSP_MOTOR_TELEMETRY, true, sizeof(msp_motor_telemetry_t)},
	{MSP_RC, true, sizeof(msp_rc_t)},
	{MSP_SET_OSD_CANVAS, true, sizeof(msp_dp_canvas_t)},
	{MSP_FC_VARIANT, true, sizeof(msp_fc_variant_t)},
	{MSP_VTX_CONFIG, true, sizeof(msp_dp_vtx_config_t)},
	{MSP_CMD_DISPLAYPORT, false, sizeof(msp_dp_cmd_t)},
};

#define MSP_FRAME_START_SIZE 5
#define MSP_CRC_SIZE 1
bool MspDPV1::Send(const uint8_t message_id, const void *payload, mspDirection_e direction)
{
	int ret{0};
	uint32_t payload_size = 0;

	msp_message_descriptor_t *desc = nullptr;

	for (int i = 0; i < MSP_DESCRIPTOR_COUNT; i++) {
		if (message_id == msp_message_descriptors[i].message_id) {
			desc = (msp_message_descriptor_t *)&msp_message_descriptors[i];
			break;
		}
	}
	
	if (!desc) {
		return false;
	}

	// need to handle different size Displayport commands
	if (!desc->fixed_size) {
		if (desc->message_id ==  MSP_CMD_DISPLAYPORT){
			uint8_t subcmd[1]{0};
			memcpy(subcmd, payload, 1);
			if (subcmd[0] == MSP_DP_DRAW_SCREEN){
				payload_size = 1;
			} else if(subcmd[0] == MSP_DP_WRITE_STRING){	// Case when we write string.. payload size may vary 
				payload_size+=sizeof(msp_dp_cmd_t);
				char dp_payload[sizeof(msp_dp_cmd_t)+MSP_OSD_MAX_STRING_LENGTH];
				memcpy(dp_payload, payload, sizeof(dp_payload));
				// Find length of string in input (may not be whole array)
				for (int i=0;i<MSP_OSD_MAX_STRING_LENGTH;++i){
					if(dp_payload[MSP_OSD_DP_WRITE_PAYLOAD + i] == '\0') break;
					payload_size++;
				}
			} else {
				payload_size = desc->message_size;
			}
		} 
	} else {
		payload_size = desc->message_size;
	}

	uint8_t packet[MSP_FRAME_START_SIZE + payload_size + MSP_CRC_SIZE];
	uint8_t crc;

	packet[0] = MSP_HEADER;
	packet[1] = MSP_START;
	packet[2] = direction ? MSP_CMD : MSP_REPLY;	// HDZero VTX firmware only supports 'replies'...
	packet[3] = payload_size;
	packet[4] = message_id;

	crc = payload_size ^ message_id;

	memcpy(packet + MSP_FRAME_START_SIZE, payload, payload_size);

	for (uint32_t i = 0; i < payload_size; i ++) {
		crc ^= packet[MSP_FRAME_START_SIZE + i];
	}

	packet[MSP_FRAME_START_SIZE + payload_size] = crc;

	int packet_size =  MSP_FRAME_START_SIZE + payload_size + MSP_CRC_SIZE;
	if(m_parameters.output_type & OUTPUT_MPA){
        ret |= pipe_server_write(MSP_OUT_PIPE_CH, &packet, packet_size);
	}
	if(m_parameters.output_type & OUTPUT_UART){
		ret |= write(m_input_fd, packet, packet_size) == packet_size;
	}

	return ret;
}

bool MspDPV1::set_osd_canvas(msp_dp_canvas_t osd_canvas){
	if(osd_canvas.col_max == 29 && osd_canvas.row_max == 15){
		resolution = HD_3016;
	} else if (osd_canvas.col_max == 49 && osd_canvas.row_max == 17){
		resolution = HD_5018;
	} else if (osd_canvas.col_max == 52 && osd_canvas.row_max == 19){
		resolution = HD_5320;
	} else {
		M_ERROR("Invalid OSD Canvas resolution requested: %ux%u. Defaulting to 53x20.\n", osd_canvas.col_max, osd_canvas.row_max);
		resolution = HD_5320;
		return false;
	}
	m_osd_canvas = osd_canvas;
	return true;
}

int MspDPV1::wrap(int x, int low, int high)
{
	const auto range = high - low;

	if (x < low) {
		x += range * ((low - x) / range + 1);
	}

	return low + (x - low) % range;
}


float MspDPV1::get_bearing_to_next_waypoint(double lat_now, double lon_now, double lat_next, double lon_next)
{
	lat_now *= 10e-8;
	lon_now *= 10e-8;
	lat_next *= 10e-8;
	lon_next *= 10e-8;

	const double lat_now_rad = lat_now * (M_PI / 180);
	const double lat_next_rad = lat_next * (M_PI / 180);

	const double cos_lat_next = cos(lat_next_rad);
	const double d_lon = (lon_next - lon_now) * (M_PI / 180);

	/* conscious mix of double and float trig function to maximize speed and efficiency */

	const float y = static_cast<float>(sin(d_lon) * cos_lat_next);
	const float x = static_cast<float>(cos(lat_now_rad) * sin(lat_next_rad) - sin(lat_now_rad) * cos_lat_next * cos(d_lon));

	return wrap(atan2f(y, x), -M_PI, M_PI);
}


float MspDPV1::get_distance_to_next_waypoint(double lat_now, double lon_now, double lat_next, double lon_next)
{
	lat_now *= 10e-8;
	lon_now *= 10e-8;
	lat_next *= 10e-8;
	lon_next *= 10e-8;

	const double lat_now_rad = lat_now * (M_PI / 180);
	const double lat_next_rad = lat_next * (M_PI / 180);

	const double d_lat = lat_next_rad - lat_now_rad;
	const double d_lon = (lon_next) * (M_PI / 180) - (lon_now) * (M_PI / 180);

	const double a = sin(d_lat / 2.0) * sin(d_lat / 2.0) + sin(d_lon / 2.0) * sin(d_lon / 2.0) * cos(lat_now_rad) * cos(
				 lat_next_rad);

	const double c = atan2(sqrt(a), sqrt(1.0 - a));

	return static_cast<float>(CONSTANTS_RADIUS_OF_EARTH * 2.0 * c);
}
