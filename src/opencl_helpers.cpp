/*******************************************************************************
 * Copyright 2025 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with hardware devices provided by
 *    ModalAI® Inc. Reverse engineering or copying this code for use on hardware 
 *    not manufactured by ModalAI is not permitted.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <iostream>
#include <vector>
#include <string>
#include <fstream>

#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.h>
#include <CL/cl.hpp>
#include <CL/cl_ext.h>
//#include <CL/cl_ext_qcom.h>

#define CL_CONTEXT_PERF_HINT_QCOM                   0x40C2
#define CL_PERF_HINT_HIGH_QCOM                      0x40C3
#define CL_PERF_HINT_NORMAL_QCOM                    0x40C4
#define CL_PERF_HINT_LOW_QCOM                       0x40C5


#define CL_CONTEXT_PRIORITY_HINT_QCOM               0x40C9
#define CL_PRIORITY_HINT_NONE_QCOM                   0
#define CL_PRIORITY_HINT_HIGH_QCOM                  0x40CA
#define CL_PRIORITY_HINT_NORMAL_QCOM                0x40CB
#define CL_PRIORITY_HINT_LOW_QCOM                   0x40CC

#define CL_QCOM_BAYER                                       0x414E
#define CL_QCOM_UNORM_MIPI10                                0x4159
#define CL_QCOM_NV12                                        0x4133
#define CL_QCOM_NV12_Y                                      0x4134
#define CL_QCOM_NV12_UV                                     0x4135

#define CL_QCOM_UNSIGNED_MIPI10                             0x415B

#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <inttypes.h>
#include <linux/types.h>

#include <math.h>

#include "misc.h"

cl::Device  gpu_device;
cl::Context gpu_context;
cl::Program program;
cl::Kernel  kernel;
cl::CommandQueue queue;
std::vector<cl::Device> devices;

typedef struct __attribute__ ((packed))
{
    int src_offset_x;
    int src_offset_y;
    int src_width;
    int src_height;
    int src_line_stride;
    int src_plane_stride;
    int dst_width;
    int dst_height;
    int dst_line_stride;    //pixels
    int dst_plane_stride;   //lines
    float gain_r;
    float gain_g;
    float gain_b;
    float gamma;
    float R00;
    float R01;
    float R10;
    float R11;
} TestParams;

typedef struct __attribute__ ((packed))
{
    float gain_r;
    float gain_g;
    float gain_b;
} GainParams;


/*
typedef struct __attribute__ ((packed))
{
    float gain_r;
    float gain_g;
    float gain_b;
    float gamma;
} TestParams;
*/
/*

cl_khr_3d_image_writes cl_img_egl_image cl_khr_byte_addressable_store cl_khr_depth_images cl_khr_egl_event cl_khr_egl_image 
cl_khr_fp16 cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics 
cl_khr_local_int32_extended_atomics cl_khr_image2d_from_buffer cl_khr_mipmap_image cl_khr_srgb_image_writes cl_khr_subgroups 
cl_qcom_create_buffer_from_image cl_qcom_ext_host_ptr cl_qcom_ion_host_ptr cl_qcom_perf_hint cl_qcom_other_image 
cl_qcom_subgroup_shuffle cl_qcom_vector_image_ops cl_qcom_extract_image_plane cl_qcom_protected_context cl_qcom_priority_hint 
cl_qcom_compressed_yuv_image_read cl_qcom_compressed_image cl_qcom_ext_host_ptr_iocoherent cl_qcom_accelerated_image_ops cl_qcom_dot_product8 
cl_qcom_reqd_sub_group_size cl_qcom_recordable_queues cl_qcom_ml_ops

*/

const char *getClErrorString(cl_int error);
int convertToString(std::string filename, std::string& s);

int64_t get_time_1us()
{
    return time_monotonic_ns()/1000;
}

int opencl_init()
{
    try {
        // Get list of OpenCL platforms.
        std::vector<cl::Platform> platform;
        cl::Platform::get(&platform);

        if (platform.empty()) {
            std::cerr << "OpenCL platforms not found." << std::endl;
            return -1;
        }

        // Get first available GPU device as there is only one.
        platform[0].getDevices(CL_DEVICE_TYPE_GPU, &devices);

        cl_context_properties properties[] = {CL_CONTEXT_PERF_HINT_QCOM, CL_PERF_HINT_HIGH_QCOM, CL_CONTEXT_PRIORITY_HINT_QCOM, CL_PRIORITY_HINT_HIGH_QCOM, 0};

        gpu_device  = devices[0];
        gpu_context = cl::Context(devices[0],properties);

        std::cout << gpu_device.getInfo<CL_DEVICE_NAME>() << std::endl;
        std::cout << gpu_device.getInfo<CL_DRIVER_VERSION>() << std::endl;
        std::cout << gpu_device.getInfo<CL_DEVICE_EXTENSIONS>() << std::endl;

        // Create command queue.
        queue = cl::CommandQueue(gpu_context, gpu_device);
    } catch (const cl::Error &err) {
        std::cerr
            << "OpenCL error: "
            << err.what() << "(" << err.err() << ")"
            << std::endl;
        return -1;
    }

    return 0;
}

int opencl_load_kernel_from_file(std::string opencl_kernel_file, std::string kernel_name)
{
    std::string gpu_code_str;
    if (convertToString(opencl_kernel_file, gpu_code_str))
    {
        std::cerr << "Could not read gpu source file " << std::string(opencl_kernel_file) << std::endl;
        return -1;
    }

    cl::Program new_program(gpu_context, cl::Program::Sources(1, std::make_pair(gpu_code_str.c_str(), gpu_code_str.size()) ));
    program = new_program;

    try {
        program.build(devices); //"-cl-fast-relaxed-math "
    } catch (const cl::Error&) {
        std::cerr
        << "OpenCL compilation error" << std::endl
        << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(gpu_device)
        << std::endl;
        return -1;
    }

    cl::Kernel new_kernel(program, kernel_name.c_str());
    kernel = new_kernel;

    std::cout << "Succesfully loaded kernel:  " << kernel_name <<std::endl;

    return 0;
}



int map_ion_host_ptr_to_gpu(cl::Context & cl_ctx, 
                            void * host_ptr, int32_t ion_fd, uint32_t size, 
                            cl::Memory * cl_mem_out_ptr,
                            cl_int * err_code)
{
    cl_mem_ion_host_ptr  cl_mem_ptr;
    memset(&cl_mem_ptr, 0, sizeof(cl_mem_ion_host_ptr));
    cl_mem_ptr.ext_host_ptr.allocation_type   = CL_MEM_ION_HOST_PTR_QCOM;
    cl_mem_ptr.ext_host_ptr.host_cache_policy = CL_MEM_HOST_UNCACHED_QCOM; //CL_MEM_HOST_WRITEBACK_QCOM; //CL_MEM_HOST_UNCACHED_QCOM;
    cl_mem_ptr.ion_filedesc                   = ion_fd;
    cl_mem_ptr.ion_hostptr                    = host_ptr;

    *cl_mem_out_ptr = cl::Buffer(cl_ctx,
                               CL_MEM_USE_HOST_PTR | CL_MEM_EXT_HOST_PTR_QCOM,
                               size , &cl_mem_ptr, err_code);

    return 0;
}


int map_ion_host_ptr_to_gpu_image2d_bayer10(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr,
                                            cl_int * err_code)
{
    cl_mem_ion_host_ptr  cl_mem_ptr;
    memset(&cl_mem_ptr, 0, sizeof(cl_mem_ion_host_ptr));
    cl_mem_ptr.ext_host_ptr.allocation_type   = CL_MEM_ION_HOST_PTR_QCOM;
    cl_mem_ptr.ext_host_ptr.host_cache_policy = CL_MEM_HOST_UNCACHED_QCOM; //CL_MEM_HOST_WRITEBACK_QCOM; //CL_MEM_HOST_UNCACHED_QCOM;
    cl_mem_ptr.ion_filedesc                   = ion_fd;
    cl_mem_ptr.ion_hostptr                    = host_ptr;

    cl::ImageFormat src_format(CL_QCOM_BAYER, CL_QCOM_UNORM_MIPI10);

    size_t img_row_pitch = 0;
    cl_image_format src_format2;
    src_format2.image_channel_order     = CL_QCOM_BAYER;
    src_format2.image_channel_data_type = CL_QCOM_UNORM_MIPI10;
    cl_int err = clGetDeviceImageInfoQCOM(gpu_device(), width, height, &src_format2,
                                          CL_IMAGE_ROW_PITCH, sizeof(img_row_pitch), &img_row_pitch, NULL);
    //printf("bayer10 input w=%d, h=%d, s=%d, image pitch = %lu, err %d\n",width, height, stride, img_row_pitch,err);

    *cl_mem_out_ptr = cl::Image2D(cl_ctx,
                                  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_HOST_PTR_QCOM,
                                  src_format,
                                  width, height, stride,
                                  &cl_mem_ptr, err_code);

    return 0;
}


int map_ion_host_ptr_to_gpu_image2d_rgba8(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr,
                                            cl_int * err_code)
{
    cl_mem_ion_host_ptr  cl_mem_ptr;
    memset(&cl_mem_ptr, 0, sizeof(cl_mem_ion_host_ptr));
    cl_mem_ptr.ext_host_ptr.allocation_type   = CL_MEM_ION_HOST_PTR_QCOM;
    cl_mem_ptr.ext_host_ptr.host_cache_policy = CL_MEM_HOST_UNCACHED_QCOM; //CL_MEM_HOST_WRITEBACK_QCOM; //CL_MEM_HOST_UNCACHED_QCOM;
    cl_mem_ptr.ion_filedesc                   = ion_fd;
    cl_mem_ptr.ion_hostptr                    = host_ptr;

    cl::ImageFormat src_format(CL_RGBA, CL_UNORM_INT8);


    size_t img_row_pitch = 0;
    cl_image_format src_format2;
    src_format2.image_channel_order     = CL_RGBA;
    src_format2.image_channel_data_type = CL_UNORM_INT8;
    cl_int err = clGetDeviceImageInfoQCOM(gpu_device(), width, height, &src_format2,
                                          CL_IMAGE_ROW_PITCH, sizeof(img_row_pitch), &img_row_pitch, NULL);
    //printf("rgba input w=%d, h=%d, s=%d, image pitch = %lu, err %d\n",width, height, stride, img_row_pitch,err);

/*
    size_t img_row_pitch = 0;
    cl_int err = clGetDeviceImageInfoQCOM(m_device, img_desc.image_width, img_desc.image_height, &img_format,
                                          CL_IMAGE_ROW_PITCH, sizeof(img_row_pitch), &img_row_pitch, NULL);
    printf("rgba stride = %d\n",img_row_pitch);
*/

    *cl_mem_out_ptr = cl::Image2D(cl_ctx,
                                  CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_HOST_PTR_QCOM,
                                  src_format,
                                  width, height, stride,
                                  &cl_mem_ptr, err_code);

    return 0;
}

int map_ion_host_ptr_to_gpu_image2d_yuv(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr, cl::Memory * y_plane, cl::Memory * uv_plane,
                                            cl_int * err_code)
{
    cl_mem_ion_host_ptr  cl_mem_ptr;
    memset(&cl_mem_ptr, 0, sizeof(cl_mem_ion_host_ptr));
    
    cl_mem_ptr.ext_host_ptr.allocation_type   = CL_MEM_ION_HOST_PTR_QCOM;
    cl_mem_ptr.ext_host_ptr.host_cache_policy = CL_MEM_HOST_UNCACHED_QCOM; //CL_MEM_HOST_WRITEBACK_QCOM; //CL_MEM_HOST_UNCACHED_QCOM;
    cl_mem_ptr.ion_filedesc                   = ion_fd;
    cl_mem_ptr.ion_hostptr                    = host_ptr;

    cl::ImageFormat image_format(CL_QCOM_NV12, CL_UNORM_INT8);
    
    *cl_mem_out_ptr = cl::Image2D(cl_ctx,
                                  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_HOST_PTR_QCOM,
                                  image_format,
                                  width, height, stride,
                                  &cl_mem_ptr, err_code);
    
    
    cl_image_format dst_y_plane_format;
    dst_y_plane_format.image_channel_order     = CL_QCOM_NV12_Y;
    dst_y_plane_format.image_channel_data_type = CL_UNORM_INT8;

    cl_image_desc dst_y_plane_desc;
    std::memset(&dst_y_plane_desc, 0, sizeof(dst_y_plane_desc));
    dst_y_plane_desc.image_type   = CL_MEM_OBJECT_IMAGE2D;
    dst_y_plane_desc.image_width  = width;
    dst_y_plane_desc.image_height = height;
    dst_y_plane_desc.mem_object   = (*cl_mem_out_ptr)();

    cl_mem dst_y_plane = clCreateImage(
            cl_ctx(),
            CL_MEM_READ_WRITE,
            &dst_y_plane_format,
            &dst_y_plane_desc,
            NULL,
            err_code
    );
    if (*err_code != CL_SUCCESS)
    {
        std::cerr << "Error " << *err_code << " with clCreateImage for dst image y plane." << "\n";
        std::exit(*err_code);
    }
    
    
    cl_image_format dst_uv_plane_format;
    dst_uv_plane_format.image_channel_order     = CL_QCOM_NV12_UV;
    dst_uv_plane_format.image_channel_data_type = CL_UNORM_INT8;

    cl_image_desc dst_uv_plane_desc;
    std::memset(&dst_uv_plane_desc, 0, sizeof(dst_uv_plane_desc));
    dst_uv_plane_desc.image_type   = CL_MEM_OBJECT_IMAGE2D;
    // The image dimensions for the uv-plane derived image must be the same as the parent image, even though the
    // actual dimensions of the uv-plane differ by a factor of 2 in each dimension.
    dst_uv_plane_desc.image_width  = width;
    dst_uv_plane_desc.image_height = height;
    dst_uv_plane_desc.mem_object   = (*cl_mem_out_ptr)();

    cl_mem dst_uv_plane = clCreateImage(
            cl_ctx(),
            CL_MEM_READ_WRITE,
            &dst_uv_plane_format,
            &dst_uv_plane_desc,
            NULL,
            err_code
    );
    if (*err_code != CL_SUCCESS)
    {
        std::cerr << "Error " << *err_code << " with clCreateImage for dst image uv plane." << "\n";
        std::exit(*err_code);
    }
    
    *y_plane = dst_y_plane;
    *uv_plane = dst_uv_plane;

    return 0;
}


int opencl_convert_mipi10_to_yuv_color(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride,
                                   void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride)
{
    cl_int     err_code;
    cl::Memory src_mem_cl;
    cl::Memory dst_mem_cl;
    //cl_int     output_line_stride  = dst_line_stride;  //pixels
    //cl_int     output_plane_stride = dst_plane_stride; //lines
    

    map_ion_host_ptr_to_gpu_image2d_bayer10(gpu_context, src_host_ptr, src_fd, src_size, src_width, src_height, src_line_stride, &src_mem_cl, &err_code);
    map_ion_host_ptr_to_gpu(gpu_context, dst_host_ptr, dst_fd, dst_size, &dst_mem_cl, &err_code);


    cl::Sampler sampler(
            gpu_context,
            CL_FALSE,
            CL_ADDRESS_CLAMP, /*CL_ADDRESS_CLAMP_TO_EDGE,*/
            CL_FILTER_LINEAR,
            &err_code
    );

/*
    cl::Sampler sampler(
            gpu_context,
            CL_FALSE,
            CL_ADDRESS_NONE,
            CL_FILTER_NEAREST,
            &err_code
    );
*/
    //std::cout << "src mem map result: " << std::string(getClErrorString(err_code)) <<std::endl;
    //std::cout << "dst mem map result: " << std::string(getClErrorString(err_code)) <<std::endl;

    const float ped      = 16.0/256.0;  //IMX412 has pedestal value of 64 for 10bit output, so 16.0 for 8 bit
    const float ped_gain = 1.0 / (1.0 - ped);

    TestParams params;
    static int cntr=0;
    cntr++;

    //image rotation for testing
    //params.R00 = cosf(2.0*M_PI * cntr / (2*240.0));
    //params.R01 = -sinf(2.0*M_PI * cntr / (2*240.0));
    //params.R10 = -params.R01;
    //params.R11 = params.R00;
    

    params.R00 = 1.0;
    params.R01 = 0.0;
    params.R10 = 0.0;
    params.R11 = 1.0;
    /*
    params.src_offset_x     = src_width/2;
    params.src_offset_y     = src_height/2;
    params.src_width        = src_width/2;
    params.src_height       = src_height/2;
    */

    params.src_offset_x     = 0;
    params.src_offset_y     = 0;
    params.src_width        = src_width;
    params.src_height       = src_height;

    //hack for 1920x1080 MIPI10 conversion issue, so we request a wider image from camera, but trim it to 1920
    if (params.src_width == 1936)
    {
        params.src_width = 1920;
    }

    params.src_line_stride  = src_line_stride;
    params.src_plane_stride = src_plane_stride;
    params.dst_width        = dst_width;
    params.dst_height       = dst_height;
    params.dst_line_stride  = dst_line_stride;
    params.dst_plane_stride = dst_plane_stride;
    params.gain_r = 1.025; //ped_gain;
    params.gain_g = 1.0; //ped_gain;
    params.gain_b = 2.5; //ped_gain;
    params.gamma  = 2.0;

    GainParams gp;
    gp.gain_r = params.gain_r;
    gp.gain_g = params.gain_g;
    gp.gain_b = params.gain_b;

    float gainsRGB[3] = {gp.gain_r, gp.gain_g, gp.gain_b};


    try
    {
        //printf("starting to set args");
        int argc = 0;
        kernel.setArg(argc++,src_mem_cl);  //printf("loaded arg 0\n");
        kernel.setArg(argc++,sampler);     //printf("loaded arg 1\n");
        kernel.setArg(argc++,dst_mem_cl);  //printf("loaded arg 2\n");


        kernel.setArg(argc++,params.gain_r);
        kernel.setArg(argc++,params.gain_g);
        kernel.setArg(argc++,params.gain_b);
        kernel.setArg(argc++,params.gamma);
        kernel.setArg(argc++,params.src_offset_x);
        kernel.setArg(argc++,params.src_offset_y);
        kernel.setArg(argc++,params.src_width);
        kernel.setArg(argc++,params.src_height);
        kernel.setArg(argc++,params.dst_width);
        kernel.setArg(argc++,params.dst_height);
        kernel.setArg(argc++,params.dst_line_stride);
        kernel.setArg(argc++,params.dst_plane_stride);

        //kernel.setArg(9,params.R00);
        //kernel.setArg(10,params.R01);
        //kernel.setArg(11,params.R10);
        //kernel.setArg(12,params.R11);
        //kernel.setArg(13,gp);
        //kernel.setArg(13,gainsRGB);


        cl::Event syncEvent;
        int64_t kernel_start = get_time_1us();
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(1,src_height), cl::NDRange(1,16), NULL, &syncEvent);
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NDRange(16,16), NULL, &syncEvent);
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NullRange, NULL, &syncEvent);
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NDRange(64,16), NULL, &syncEvent);
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NDRange(32,32), NULL, &syncEvent);
        queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NDRange(64,16), NULL, &syncEvent);
        cl_int errCode = syncEvent.wait();
        if (errCode)
        {
          std::cout << "Enqueue kernel error " << errCode << std::endl;
        }

        int64_t kernel_stop  = get_time_1us();

        //std::cout << "kernel time = " << (kernel_stop-kernel_start) << "us" << std::endl;
    }

    catch (const cl::Error &err) {
        std::cerr
            << "OpenCL error: "
            << err.what() << "(" << err.err() << ")"
            << std::endl;
        //return 1;
    }

    queue.finish();

    clReleaseSampler(sampler());
    clReleaseMemObject(src_mem_cl());
    clReleaseMemObject(dst_mem_cl());

    return 0;
}



int convertToString(std::string filename, std::string& s)
{
  size_t size;
  char*  str;
  std::ifstream f(filename, std::ifstream::in | std::ifstream::binary);
  if(f.is_open())
  {
    size_t fileSize;
    f.seekg(0, std::fstream::end);
    size = fileSize = (size_t)f.tellg();
    f.seekg(0, std::fstream::beg);
    str = new char[size+1];
    if(!str)
    {
      f.close();
      return 0;
    }

    f.read(str, fileSize);
    f.close();
    str[size] = '\0';
    s = std::string(str, str+size);
    delete[] str;
    return 0;
  }
  std::cout<<"Error: failed to open file:"<<filename<<std::endl;
  return -1;
}



const char *getClErrorString(cl_int error)
{
    switch(error){
        // run-time and JIT compiler errors
        case 0: return "CL_SUCCESS";
        case -1: return "CL_DEVICE_NOT_FOUND";
        case -2: return "CL_DEVICE_NOT_AVAILABLE";
        case -3: return "CL_COMPILER_NOT_AVAILABLE";
        case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
        case -5: return "CL_OUT_OF_RESOURCES";
        case -6: return "CL_OUT_OF_HOST_MEMORY";
        case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
        case -8: return "CL_MEM_COPY_OVERLAP";
        case -9: return "CL_IMAGE_FORMAT_MISMATCH";
        case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
        case -11: return "CL_BUILD_PROGRAM_FAILURE";
        case -12: return "CL_MAP_FAILURE";
        case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
        case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
        case -15: return "CL_COMPILE_PROGRAM_FAILURE";
        case -16: return "CL_LINKER_NOT_AVAILABLE";
        case -17: return "CL_LINK_PROGRAM_FAILURE";
        case -18: return "CL_DEVICE_PARTITION_FAILED";
        case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

        // compile-time errors
        case -30: return "CL_INVALID_VALUE";
        case -31: return "CL_INVALID_DEVICE_TYPE";
        case -32: return "CL_INVALID_PLATFORM";
        case -33: return "CL_INVALID_DEVICE";
        case -34: return "CL_INVALID_CONTEXT";
        case -35: return "CL_INVALID_QUEUE_PROPERTIES";
        case -36: return "CL_INVALID_COMMAND_QUEUE";
        case -37: return "CL_INVALID_HOST_PTR";
        case -38: return "CL_INVALID_MEM_OBJECT";
        case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
        case -40: return "CL_INVALID_IMAGE_SIZE";
        case -41: return "CL_INVALID_SAMPLER";
        case -42: return "CL_INVALID_BINARY";
        case -43: return "CL_INVALID_BUILD_OPTIONS";
        case -44: return "CL_INVALID_PROGRAM";
        case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
        case -46: return "CL_INVALID_KERNEL_NAME";
        case -47: return "CL_INVALID_KERNEL_DEFINITION";
        case -48: return "CL_INVALID_KERNEL";
        case -49: return "CL_INVALID_ARG_INDEX";
        case -50: return "CL_INVALID_ARG_VALUE";
        case -51: return "CL_INVALID_ARG_SIZE";
        case -52: return "CL_INVALID_KERNEL_ARGS";
        case -53: return "CL_INVALID_WORK_DIMENSION";
        case -54: return "CL_INVALID_WORK_GROUP_SIZE";
        case -55: return "CL_INVALID_WORK_ITEM_SIZE";
        case -56: return "CL_INVALID_GLOBAL_OFFSET";
        case -57: return "CL_INVALID_EVENT_WAIT_LIST";
        case -58: return "CL_INVALID_EVENT";
        case -59: return "CL_INVALID_OPERATION";
        case -60: return "CL_INVALID_GL_OBJECT";
        case -61: return "CL_INVALID_BUFFER_SIZE";
        case -62: return "CL_INVALID_MIP_LEVEL";
        case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
        case -64: return "CL_INVALID_PROPERTY";
        case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
        case -66: return "CL_INVALID_COMPILER_OPTIONS";
        case -67: return "CL_INVALID_LINKER_OPTIONS";
        case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";

        // extension errors
        case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
        case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
        case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
        case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
        case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
        case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
        default: return "Unknown OpenCL error";
    }
}
