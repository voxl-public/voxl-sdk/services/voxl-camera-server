#pragma once

#include <pthread.h> 
#include <cmath>
#include <memory>
#include <numeric>   
#include <chrono>

// MAVLink
#include <c_library_v2/common/mavlink.h>

// MPA
#include <modal_start_stop.h>
#include <modal_pipe_client.h>
#include <modal_pipe_common.h>
#include <modal_pipe_server.h>

// MSP
#include "msp_dp_defines.h"
#include "MessageDisplay.h"
#include "config_file_msp.h"

// Menu Helper
#include "menu_helper.h"

class MenuManager
{
public:
	// One Menu Manager to rule them all...
	explicit MenuManager(msp_dp_osd_params_t params): m_parameters(params){};
	MenuManager(const MenuManager &other) = delete;
	~MenuManager() = default;

	// Menu Public ops
	bool check_for_menu(std::shared_ptr<mavlink_rc_channels_t> rc_msg);
	void handle_menu(msp_dp_rc_sticks_t sticks, bool new_menu=false);
	bool menu_is_active(){return m_fc_menu || m_vtx_menu || m_camera_menu || m_shortcut_menu;};

private:
	// Menu Private ops
	void handle_fc_menu(bool new_menu=false);
	void handle_vtx_menu(bool new_menu=false);
	void handle_camera_menu(bool new_menu=false);
	void handle_fc_menu_action(bool new_menu=false);
	void handle_vtx_menu_action(bool new_menu=false);
	void handle_camera_menu_action(bool new_menu=false);
	bool save_changes_to_file(menu_type_e menu_type);
	std::string run_command(const std::string& cmd);
	std::string get_sdk_version(const std::string& packageOutput);

	// Timestamp Setters
	void set_menu_exit_ts(){m_latest_menu_exit = std::chrono::steady_clock::now();};
	void set_menu_scroll_ts(){m_latest_menu_scroll = std::chrono::steady_clock::now();};
	void set_menu_select_ts(){m_latest_menu_select = std::chrono::steady_clock::now();};
	void set_menu_req_ts(){m_latest_menu_request = std::chrono::steady_clock::now();};
	void set_menu_exit_time(){
		switch(get_active_menu_type()){
			case FC_MENU:
				m_fc_menu = false;
				break;
			case VTX_MENU:
				m_vtx_menu = false;
				break;
			case CAMERA_MENU:
				m_camera_menu = false;
				break;
			case SHORTCUT_MENU:
				m_shortcut_menu = false;
				break;
			case UNKNOWN_MENU: [[fallthrough]]
			default:
				break;
		}
		m_latest_menu_exit = std::chrono::steady_clock::now();
	}

	// Timestamp Getters
	std::chrono::steady_clock::time_point get_menu_req_ts(){ return m_latest_menu_request;}
	std::chrono::steady_clock::time_point get_menu_exit_ts(){return m_latest_menu_exit;};
	std::chrono::steady_clock::time_point get_menu_scroll_ts(){return m_latest_menu_scroll;};
	std::chrono::steady_clock::time_point get_menu_select_ts(){return m_latest_menu_select;};

	// Menu state/getters
	void get_menu_default_values(menu_type_e menu_type);
	menu_type_e get_active_menu_type(){
		if(m_fc_menu) return FC_MENU;
		if(m_vtx_menu) return VTX_MENU;
		if(m_camera_menu) return CAMERA_MENU;
		if(m_shortcut_menu) return SHORTCUT_MENU;
		return UNKNOWN_MENU;
	}
	int get_menu_size(menu_type_e menu_type){
		switch(menu_type){
			case FC_MENU: return FC_MENU_ROWS; 
			case VTX_MENU: return VTX_MENU_ROWS;
			case CAMERA_MENU: return CAMERA_MENU_ROWS;
			case SHORTCUT_MENU: return 0;
			case UNKNOWN_MENU: [[fallthrough]]
			default: 
				M_ERROR("Unknown menu type! Returning ERROR menu rows\n");				
		}
		return ERROR_MENU_ROWS;
	}
	const char** get_menu_items(menu_type_e menu_type){
		switch(menu_type){
			case FC_MENU: return fc_menu; 
			case VTX_MENU: return vtx_menu;
			case CAMERA_MENU: return camera_menu;
			case UNKNOWN_MENU: [[fallthrough]]
			default:
				// This should never happen, we should never get here... but just in case
				M_ERROR("Can't determine what kind of menu to use! Menu Type Received: (raw)%d --> %s", menu_type, menu_type_as_string(menu_type));
		}
		return error_menu;
	}
	
	// Useful utility structs
	msp_dp_rc_sticks_t	m_sticks{0};
	msp_dp_osd_params_t	m_parameters{0};
	uint8_t fontType{0};
	resolutionType_e resolution{HD_5018};
	uint8_t row_max[4]{15,17,15,19};
	uint8_t column_max[4]{29,49,29,52};

	// Menu control
	int m_menu_row{0};
	int m_freq_idx{0};
	int m_pwr_idx{0};
	int m_mcs_idx{0};
	int m_fec_idx{0};
	int m_mode_idx{0};
	int m_shortcut_idx{0};
	int m_resolution_idx{0};
	int m_bitrate_idx{0};
	int m_fps_idx{0};
	int m_stick_idx{0};

	// Menu default values based on config file, populated at runtime
	int m_default_freq_idx{0};
	int m_default_pwr_idx{0};
	int m_default_mcs_idx{0};
	int m_default_fec_idx{0};
	int m_default_mode_idx{0};
	int m_default_shortcut_idx{0};
	int m_default_resolution_idx{0};
	int m_default_bitrate_idx{0};
	int m_default_fps_idx{0};
	int m_default_stick_idx{0};

	// Variables from camera config 
	int m_camera_width{1280};
	int m_camera_height{720};
	char m_vtx_source_pipe[64];

	// Menu Active or not & values 
	bool m_fc_menu{false};
	bool m_vtx_menu{false};
	bool m_camera_menu{false};
	bool m_shortcut_menu{false};
	char** m_menu_values{nullptr};
	menu_type_e active_menu{UNKNOWN_MENU};

	// Debounce timekeeping
	std::chrono::steady_clock::time_point m_latest_menu_scroll{std::chrono::steady_clock::now()};
	std::chrono::steady_clock::time_point m_latest_menu_select{m_latest_menu_scroll};
	std::chrono::steady_clock::time_point m_latest_menu_exit{m_latest_menu_scroll};
	std::chrono::steady_clock::time_point m_latest_menu_request{m_latest_menu_exit};
};