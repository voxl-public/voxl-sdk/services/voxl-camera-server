/*******************************************************************************
 * Copyright 2025 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with hardware devices provided by
 *    ModalAI® Inc. Reverse engineering or copying this code for use on hardware 
 *    not manufactured by ModalAI is not permitted.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#pragma once

#include <stdlib.h>
#include <stdint.h>
#include <string>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>
#include <modal_pipe_interfaces.h>

#include <rc_math.h>

typedef struct 
{
    int64_t      last_ts_integrated;
    rc_matrix_t  R_since_start;
} ImuContext;


class ImuManager
{
public:
    ImuManager();
    ~ImuManager();

    int init();

    int add_imu_sample(imu_data_t * d);

    int update(ImuContext * ctx, int64_t timestamp_ns);

private:
    int imu_pipe_idx = 2;   //WARNING: hardcoded, needs to be resolved in camera server
    std::string input_pipe_name;
    std::string mpa_client_name;

    rc_timed3_ringbuf_t gyro_buf    = RC_TIMED3_RINGBUF_INITIALIZER;
    rc_matrix_t R_since_start       = RC_MATRIX_INITIALIZER;
    rc_matrix_t R_since_start_filt  = RC_MATRIX_INITIALIZER;
    rc_matrix_t R_last_segment      = RC_MATRIX_INITIALIZER;
    rc_matrix_t R_last_segment_filt = RC_MATRIX_INITIALIZER;

    int64_t last_ts_inserted   = 0;
    int64_t last_ts_integrated = 0;

    const uint32_t gyro_buf_size = 2000;
    bool en_debug = 0;

    uint32_t sample_cntr = 0;

    double bias_gyro[3]      = {0.0, 0.0, 0.0};
    double gyro_vals_filt[3] = {0.0, 0.0, 0.0};
    double accl_vals_filt[3] = {0.0, 0.0, 0.0};
};
