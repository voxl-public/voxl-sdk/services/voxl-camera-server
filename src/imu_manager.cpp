#include "imu_manager.hpp"

#include <math.h>
#include <string>

#define RAD_TO_DEG          (180.0/M_PI)

static int64_t _time_monotonic_ns()
{
    struct timespec ts;
    if(clock_gettime(CLOCK_MONOTONIC, &ts)){
        fprintf(stderr,"ERROR calling clock_gettime\n");
        return -1;
    }
    return (int64_t)ts.tv_sec*1000000000 + (int64_t)ts.tv_nsec;
}


static int64_t _time_realtime_ns()
{
    struct timespec ts;
    if(clock_gettime(CLOCK_REALTIME, &ts)){
        fprintf(stderr,"ERROR calling clock_gettime\n");
        return -1;
    }
    return (int64_t)ts.tv_sec*1000000000 + (int64_t)ts.tv_nsec;
}

// callback to put new imu data into the buffer
static void _helper_cb(__attribute__((unused))int ch, char* data, int bytes, void* context)
{
    ImuManager * m = (ImuManager*)(context);

    // validate that the data makes sense
    int n_packets;
    imu_data_t* d = pipe_validate_imu_data_t(data, bytes, &n_packets);

    // insert each gyro sample to our timed3 buffer
    for(int i=0;i<n_packets;i++){
        /*
        double val[3];
        val[0] = (double)d[i].gyro_rad[0];
        val[1] = (double)d[i].gyro_rad[1];
        val[2] = (double)d[i].gyro_rad[2];
        //printf("[%lu] %.3f %.3f %.3f\n",d[i].timestamp_ns, val[0],val[1],val[2]);
        */
        m->add_imu_sample(&d[i]);
    }
    return;
}

ImuManager::ImuManager()
{
    mpa_client_name = "cam_imu_mgr";

    rc_matrix_identity(&R_since_start, 3);
    rc_matrix_identity(&R_since_start_filt, 3);
    rc_timed3_ringbuf_alloc(&gyro_buf, gyro_buf_size);
}

ImuManager::~ImuManager()
{
    printf("\nIMU Manager closing and exiting\n");

    //TODO: de-allocated matrices / buffers ???

    pipe_client_close_all();
}

int ImuManager::init()
{
    /*
    //voxl2 test
    //+0.007742 -0.003990 -0.006083
    bias_gyro[0] = +0.007742;
    bias_gyro[1] = -0.003990;
    bias_gyro[2] = -0.006083;
    */

    //voxl2 mini test
    //-0.007480 +0.015714 -0.009459
    bias_gyro[0] = -0.007480;
    bias_gyro[1] = +0.015714;
    bias_gyro[2] = -0.009459;

    input_pipe_name = "imu_apps";

    pipe_client_set_simple_helper_cb(imu_pipe_idx, _helper_cb, this);

    int flags = CLIENT_FLAG_EN_SIMPLE_HELPER;

    int ret = pipe_client_open(imu_pipe_idx, input_pipe_name.c_str(), mpa_client_name.c_str(),
                               flags, IMU_RECOMMENDED_READ_BUF_SIZE);

    // check for errors trying to connect to the server pipe
    if(ret<0){
        pipe_print_error(ret);
        return -1;
    }

    return 0;
}

int ImuManager::add_imu_sample(imu_data_t * d)
{
    sample_cntr++;

    double val[3];
    val[0] = (double)d->gyro_rad[0] - bias_gyro[0];
    val[1] = (double)d->gyro_rad[1] - bias_gyro[1];
    val[2] = (double)d->gyro_rad[2] - bias_gyro[2];
    rc_timed3_ringbuf_insert(&gyro_buf, d->timestamp_ns, val);

    //low pass filter to get an average value
    for (int dim=0; dim<3; dim++){
        gyro_vals_filt[dim] = gyro_vals_filt[dim]*0.99 + d->gyro_rad[dim]*0.01;
        accl_vals_filt[dim] = accl_vals_filt[dim]*0.99 + d->accl_ms2[dim]*0.01;

        if (sample_cntr < 1000){
            bias_gyro[dim] = bias_gyro[dim]*0.9 + gyro_vals_filt[dim]*0.1;
        }
    }



    return 0;
    printf("[%lu] gyro: %+.6f %+.6f %+.6f   accel: %+.6f %+.6f %+.6f\n",
        d->timestamp_ns,
        gyro_vals_filt[0],gyro_vals_filt[1],gyro_vals_filt[2],
        accl_vals_filt[0],accl_vals_filt[1],accl_vals_filt[2]);

    return 0;
    printf("[%lu] gyro: %+.6f %+.6f %+.6f   accel: %+.6f %+.6f %+.6f\n",
        d->timestamp_ns,
        d->gyro_rad[0],d->gyro_rad[1],d->gyro_rad[2],
        d->accl_ms2[0],d->accl_ms2[1],d->accl_ms2[2]);


    return 0;

    last_ts_inserted = d->timestamp_ns;

    if (sample_cntr % 33 != 0)
        return 0;

    int64_t t_now   = _time_monotonic_ns();
    int64_t t_end   = last_ts_inserted;
    int64_t t_start = last_ts_integrated;

    // if this is our first pass, set start time as the oldest data we have
    int64_t oldest_ts_in_buf;
    rc_timed3_ringbuf_get_ts_at_pos(&gyro_buf, gyro_buf.items_in_buf-1, &oldest_ts_in_buf);

    if(t_start == 0 || t_start<oldest_ts_in_buf){
        if(en_debug) fprintf(stderr, "setting new start point to oldest data\n");
        t_start = oldest_ts_in_buf;
    }

    // buffer stopped filling, wait to reconnect
    if(t_start>=t_end)
        return 0;


    if(en_debug){
        printf("t_start is %5dms ago, t_end is %5dms ago\n",\
                                                    (int)((t_now-t_start)/1000000),\
                                                    (int)((t_now-t_end)/1000000));
    }

    int64_t t_b4   = _time_monotonic_ns();
    int ret = rc_timed3_ringbuf_integrate_gyro_3d(&gyro_buf, t_start, t_end, &R_last_segment);
    int64_t t_after   = _time_monotonic_ns();
    if(ret){
        fprintf(stderr, "integrate gyro returned %d\n", ret);
        return 0;
    }

    // add last second's rotation to the total
    rc_matrix_right_multiply_inplace(&R_since_start, R_last_segment);

    double tb_all[3], tb_last_s[3];
    rc_rotation_to_tait_bryan(R_since_start, &tb_all[0], &tb_all[1], &tb_all[2]);
    tb_all[0] *= RAD_TO_DEG;
    tb_all[1] *= RAD_TO_DEG;
    tb_all[2] *= RAD_TO_DEG;

    rc_rotation_to_tait_bryan(R_last_segment, &tb_last_s[0], &tb_last_s[1], &tb_last_s[2]);
    tb_last_s[0] *= RAD_TO_DEG;
    tb_last_s[1] *= RAD_TO_DEG;
    tb_last_s[2] *= RAD_TO_DEG;

    if(!en_debug) printf("\r");
    printf("          %6.1f %6.1f %6.1f       %6.1f %6.1f %6.1f   %5d", \
                                tb_all[0],tb_all[1],tb_all[2],\
                                tb_last_s[0],tb_last_s[1],tb_last_s[2],\
                                (int)(t_after - t_b4)/1000);
    if(en_debug) printf("\n");
    fflush(stdout);

    last_ts_integrated = t_end;



    printf("[%lu] gyro: %+.3f %+.3f %+.3f   accel: %+.3f %+.3f %+.3f\n",
        d->timestamp_ns,
        d->gyro_rad[0],d->gyro_rad[1],d->gyro_rad[2],
        d->accl_ms2[0],d->accl_ms2[1],d->accl_ms2[2]);


    return 0;
}

int  ImuManager::update(ImuContext * ctx, int64_t timestamp_ns)
{
    //if first time, initialize rotation matrix to identity
    if (ctx->last_ts_integrated == 0){
        rc_matrix_identity(&ctx->R_since_start, 3);
        ctx->last_ts_integrated = timestamp_ns;
        return 0;
    }

    int64_t t_start = ctx->last_ts_integrated;
    int64_t t_end   = timestamp_ns;


    int64_t oldest_ts_in_buf;
    int64_t newest_ts_in_buf;
    rc_timed3_ringbuf_get_ts_at_pos(&gyro_buf, gyro_buf.items_in_buf-1, &oldest_ts_in_buf);
    rc_timed3_ringbuf_get_ts_at_pos(&gyro_buf, 0, &newest_ts_in_buf);

    if ((timestamp_ns > newest_ts_in_buf) || (timestamp_ns < oldest_ts_in_buf)){
        printf("ERROR: ImuManager: requesting t=%lu, available [%lu-%lu]\n",timestamp_ns,oldest_ts_in_buf,newest_ts_in_buf);
        return -1;
    }

    rc_matrix_t R_delta      = RC_MATRIX_INITIALIZER;

    int64_t t_b4   = _time_monotonic_ns();
    int ret = rc_timed3_ringbuf_integrate_gyro_3d(&gyro_buf, t_start, t_end, &R_delta);
    int64_t t_after  = _time_monotonic_ns();
    if(ret){
        fprintf(stderr, "integrate gyro returned %d\n", ret);
        return 0;
    }

    // add last second's rotation to the total
    rc_matrix_right_multiply_inplace(&ctx->R_since_start, R_delta);

    double tb_all[3];
    rc_rotation_to_tait_bryan(ctx->R_since_start, &tb_all[0], &tb_all[1], &tb_all[2]);
    tb_all[0] *= RAD_TO_DEG;
    tb_all[1] *= RAD_TO_DEG;
    tb_all[2] *= RAD_TO_DEG;

/*
    if(!en_debug) printf("\r");
    printf("          %6.1f %6.1f %6.1f       %5dus", 
                                tb_all[0],tb_all[1],tb_all[2],
                                (int)(t_after - t_b4)/1000);
    if(en_debug) printf("\n");
    fflush(stdout);
*/
    ctx->last_ts_integrated = timestamp_ns;

    return 0;
}
