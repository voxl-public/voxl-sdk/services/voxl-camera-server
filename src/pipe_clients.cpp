/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <thread>
#include <mutex>

#include <c_library_v2/common/mavlink.h>
#include <c_library_v2/development/development.h>
#include <modal_pipe_client.h>
#include <cpu_monitor_interface.h>
#include <modal_journal.h>

#include "pipe_clients.h"
#include "voxl_camera_server.h" // for #define PROCESS_NAME "voxl-camera-server"

#include "MspDPV1.h"
#include <memory>
#include <iostream>
#include <iomanip>

#define MAVLINK_ONBOARD_PIPE_NAME    "mavlink_onboard"
#define CPU_PIPE_NAME    "cpu_monitor"

#define MAVLINK_ONBOARD_CH  0
#define CPU_CH  1

#define MSPDPV1_PIPE_NAME    "msp_osd"
#define MSPDPV1_CH  2

std::mutex mtx;
static uav_state_t _uav_state;
static int standby_active = 0;

// OSD elements
static uint16_t  millivolts = 0;
static double  volts = 0;
static uint8_t battery_percent = 0;
static double  amps = 0;
static uint8_t rssi = 0;
static bool armed = false; 
static std::string heading;
static std::string flight_mode;
static float cpu_temp=0.0f;
static float cpu_load=0.0f;
static std::unique_ptr<MspDPV1> msp_parser = std::make_unique<MspDPV1>(true);

static void update_heading();
static void update_flight_mode(const mavlink_message_t* msg);

// protect multi-byte states such as the attitude struct with this mutex
//static pthread_mutex_t gps_mtx = PTHREAD_MUTEX_INITIALIZER;

static void _mavlink_connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
    M_PRINT("connected to mavlink pipe\n");
}

static void _mavlink_disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
    M_PRINT("disconnected from mavlink pipe\n");
}

static void _mavlink_helper_cb(__attribute__((unused))int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
    //pthread_mutex_lock(&gps_mtx);
    // validate that the data makes sense
    int n_packets;
    mavlink_message_t* msg_array = pipe_validate_mavlink_message_t(data, bytes, &n_packets);
    if(msg_array == NULL){
        return;
    }

    //std::cout << "Got Mavlink Msgs: " << n_packets << std::endl;
    for(int i=0; i<n_packets;i++) {
        //std::cout << "Msg[" << i <<"]=" << msg_array[i].msgid  << std::endl;
        
        mavlink_message_t* msg = &msg_array[i];
        
        switch(msg->msgid){
            case(MAVLINK_MSG_ID_GPS_RAW_INT): {
                // std::cout << "Got GPS RAW INT MAVLINK MSG" << std::endl;
                // fetch integer values from the unpacked mavlink message
                int32_t  lat  = mavlink_msg_gps_raw_int_get_lat(msg);
                int32_t  lon  = mavlink_msg_gps_raw_int_get_lon(msg);
                int32_t  alt  = mavlink_msg_gps_raw_int_get_alt(msg);

                // convert integer values to more useful units
                std::lock_guard<std::mutex> lock(mtx);
                _uav_state.lat_deg = (double)lat/10000000.0;
                _uav_state.lon_deg = (double)lon/10000000.0;
                _uav_state.alt_msl_meters = (double)alt/1000.0;
            }; break;
            case(MAVLINK_MSG_ID_ATTITUDE): {
                // std::cout << "Got ATTITUDE MAVLINK MSG" << std::endl;
                float roll = mavlink_msg_attitude_get_roll(msg)* (180.0/M_PI);
                float pitch = mavlink_msg_attitude_get_pitch(msg)* (180.0/M_PI);
                float yaw = mavlink_msg_attitude_get_yaw(msg)* (180.0/M_PI);
                
                std::lock_guard<std::mutex> lock(mtx);
                _uav_state.pitch_deg = pitch;
                _uav_state.roll_deg = roll;
                _uav_state.yaw_deg = yaw;
                
            }; break;
            case(MAVLINK_MSG_ID_SYS_STATUS): {
                // std::cout << "Got MAVLINK_MSG_ID_SYS_STATUS MAVLINK MSG" << std::endl;
                millivolts   = mavlink_msg_sys_status_get_voltage_battery(msg); //mV to V
                if (millivolts != UINT16_MAX) volts   = ((double) millivolts)/1000.0;
                if (mavlink_msg_sys_status_get_current_battery(msg) != -1) amps = ((double) mavlink_msg_sys_status_get_current_battery(msg))/100.0; // cA to A
            };break;
            case(MAVLINK_MSG_ID_RC_CHANNELS): {
                // std::cout << "Got MAVLINK_MSG_ID_RC_CHANNELS MAVLINK MSG" << std::endl;
                rssi = mavlink_msg_rc_channels_get_rssi(msg);
            }; break;
            case(MAVLINK_MSG_ID_HEARTBEAT): {
                // std::cout << "Got MAVLINK_MSG_ID_HEARTBEAT MAVLINK MSG" << std::endl;
                armed = mavlink_msg_heartbeat_get_base_mode(msg) & MAV_MODE_FLAG_SAFETY_ARMED;
                update_flight_mode(msg);
                update_heading();
            }
        }
        
    }
    //mavlink_message_t* msg = &msg_array[0];


    //pthread_mutex_unlock(&gps_mtx);

    return;
}

static void msp_connect(int ch, void* context) {
    M_PRINT("Connected to MSPDPV1 pipe\n");
}

static void msp_disconnect(int ch, void* context) {
    M_PRINT("Disconnected from MSPDPV1 pipe\n");
}

static void msp_cb(int ch, char* data, int bytes, void* context) {
    for (int i = 0; i < bytes; ++i) {
        msp_parser->processIncomingByte((uint8_t)data[i]);
    }
}

static void _cpu_connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
    M_PRINT("Connected to cpu-monitor\n");
    return;
}

static void _cpu_disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
    M_PRINT("Disconnected from cpu-monitor\n");
    return;
}


// called whenever the simple helper has data for us to process
static void _cpu_helper_cb(__attribute__((unused))int ch, char* raw_data, int bytes, __attribute__((unused)) void* context)
{
    int n_packets;
    cpu_stats_t *data_array = modal_cpu_validate_pipe_data(raw_data, bytes, &n_packets);
    if (data_array == NULL){
        M_DEBUG("Data array is null");
        return;
    }
    // only use most recent packet
    cpu_stats_t data = data_array[n_packets-1];
    cpu_temp = data.cpu_t_max;
    cpu_load = data.total_cpu_load;

    if(data.flags&CPU_STATS_FLAG_STANDBY_ACTIVE){
        if(!standby_active){
            M_DEBUG("Entering standby mode\n");
            standby_active = 1;
        }
    }
    else{
        if(standby_active){
            M_DEBUG("Exiting standby mode\n");
            standby_active = 0;
        }
    }
    M_DEBUG("Value of standby_active is: %i \n", standby_active);
    return;
}




int pipe_clients_init(void)
{
    // Setup necessary pipes for GPS and CPU
    pipe_client_set_connect_cb(MAVLINK_ONBOARD_CH, _mavlink_connect_cb, NULL);
    pipe_client_set_disconnect_cb(MAVLINK_ONBOARD_CH, _mavlink_disconnect_cb, NULL);
    pipe_client_set_simple_helper_cb(MAVLINK_ONBOARD_CH, _mavlink_helper_cb, NULL);
    
    
    int ret = pipe_client_open(MAVLINK_ONBOARD_CH, MAVLINK_ONBOARD_PIPE_NAME, PROCESS_NAME, \
                    EN_PIPE_CLIENT_SIMPLE_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT, \
                    MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);


    pipe_client_set_connect_cb(CPU_CH, _cpu_connect_cb, NULL);
    pipe_client_set_disconnect_cb(CPU_CH, _cpu_disconnect_cb, NULL);
    pipe_client_set_simple_helper_cb(CPU_CH, _cpu_helper_cb, NULL);
    ret |= pipe_client_open(CPU_CH, CPU_PIPE_NAME, PROCESS_NAME, \
                    EN_PIPE_CLIENT_SIMPLE_HELPER, \
                    CPU_STATS_RECOMMENDED_READ_BUF_SIZE);


    pipe_client_set_connect_cb(MSPDPV1_CH, msp_connect, NULL);
    pipe_client_set_disconnect_cb(MSPDPV1_CH, msp_disconnect, NULL);
    pipe_client_set_simple_helper_cb(MSPDPV1_CH, msp_cb, NULL);
    ret |= pipe_client_open(MSPDPV1_CH, MSPDPV1_PIPE_NAME, PROCESS_NAME, EN_PIPE_CLIENT_SIMPLE_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT,
                            MSP_RECOMMENDED_PIPE_SIZE);

    return ret;
}

uav_state_t grab_uav_state(){
    uav_state_t uav_state;
    memset(&uav_state, 0x00, sizeof(uav_state_t));
    
    std::lock_guard<std::mutex> lock(mtx);
    uav_state.alt_agl_meters = _uav_state.alt_agl_meters;
    uav_state.alt_msl_meters = _uav_state.alt_msl_meters;
    uav_state.lat_deg = _uav_state.lat_deg;
    uav_state.lon_deg = _uav_state.lon_deg;
    uav_state.pitch_deg = _uav_state.pitch_deg;
    uav_state.roll_deg = _uav_state.roll_deg;
    uav_state.yaw_deg = _uav_state.yaw_deg;
    
    return uav_state;
}

bool grab_armed(){return armed;}
const char* grab_heading(){return heading.c_str();}
const char* grab_flight_mode(){return flight_mode.c_str();}
uint8_t grab_rssi(){return rssi;}
double grab_battery_amps(){return amps;}
double grab_battery_voltage(){return volts;}
uint8_t grab_battery_voltage_percent(){return battery_percent;}
float grab_cpu_temp(){return cpu_temp;}
float grab_cpu_load(){return cpu_load;};
int grab_cpu_standby_active(void)
{
    return standby_active;
}
std::map<std::pair<uint8_t, uint8_t>, std::string> grab_msp_osd_map(){return msp_parser->getDisplayDataMap();};
msp_dp_canvas_t grab_msp_osd_canvas_dims(){return msp_parser->get_osd_canvas_dims();};
void bf_menu_active(bool status){msp_parser->bf_menu_active(status);};

static void update_flight_mode(const mavlink_message_t* msg){
    // Display Flight Mode
    switch (mavlink_msg_heartbeat_get_custom_mode(msg)) {
    case FLIGHT_MODE_MANUAL:
        flight_mode = "MANUAL"; 
        break;
    case FLIGHT_MODE_ALTCTL:
        flight_mode = "ALTCTL"; 
        break;
    case FLIGHT_MODE_POSCTL:
        flight_mode = "POSCTL"; 
        break;
    case FLIGHT_MODE_AUTO_MISSION:
        flight_mode = "AUTO_MISSION"; 
        break;
    case FLIGHT_MODE_AUTO_LOITER:
        flight_mode = "AUTO_LOITER"; 
        break;
    case FLIGHT_MODE_AUTO_RTL:
        flight_mode = "AUTO_RTL"; 
        break;
    case FLIGHT_MODE_AUTO_TAKEOFF:
        flight_mode = "AUTO_TAKEOFF"; 
        break;
    case FLIGHT_MODE_AUTO_LAND:
        flight_mode = "AUTO_LAND"; 
        break;
    case FLIGHT_MODE_AUTO_FOLLOW_TARGET:
        flight_mode = "AUTO_FOLLOW_TARGET";
        break;
    case FLIGHT_MODE_AUTO_PRECLAND:
        flight_mode = "AUTO_PRECLAND"; 
        break;
    case FLIGHT_MODE_AUTO_VTOL_TAKEOFF:
        flight_mode = "AUTO_VTOL_TAKEOFF"; 
        break;
    case FLIGHT_MODE_ACRO:
        flight_mode = "ACRO"; 
        break;
    case FLIGHT_MODE_OFFBOARD:
        flight_mode = "OFFBOARD"; 
        break;
    case FLIGHT_MODE_STABELIZED:
        flight_mode = "STAB"; 
        break;
    default:
        flight_mode = "???";
        break; 
    }
}

static void update_heading(){
    // Display direction
    // Need to test and verify these calculations to cardinal directions more
    double yaw = grab_uav_state().yaw_deg; 
    if (yaw<0)yaw+=360.0;
    if (yaw <= 22.5f) {
        heading = "N";
    } else if (yaw <= 67.5f) {
        heading = "NE";
    } else if (yaw <= 112.5f) {
        heading = "E";
    } else if (yaw <= 157.5f) {
        heading = "SE";
    } else if (yaw <= 202.5f) {
        heading = "S";
    } else if (yaw <= 247.5f) {
        heading = "SW";
    } else if (yaw <= 292.5f) {
        heading = "W";
    } else if (yaw <= 337.5f) {
        heading = "NW";
    } else if (yaw <= 360.0f) {
        heading = "N";
    }
}

int pipe_clients_close(void)
{
    pipe_client_close(MAVLINK_ONBOARD_CH);
    pipe_client_close(CPU_CH);
    return 0;
}

