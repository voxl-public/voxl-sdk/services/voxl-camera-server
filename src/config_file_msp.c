#include <stdio.h>
#include <modal_json.h>

#include "config_file_msp.h"

#define FILE_HEADER "\
/**\n\
 * voxl-osd Configuration File\n\
 */\n"

int print_config_file(msp_dp_osd_params_t params)
{
	printf("=================================================================");
	printf("\n");
	printf("Parameters as loaded from config file:\n");
	printf("rssi_col: %d\n", params.rssi_col);
	printf("rssi_row: %d\n", params.rssi_row);
	printf("current_draw_col: %d\n", params.current_draw_col);
	printf("current_draw_row: %d\n", params.current_draw_row);
	printf("battery_col: %d\n", params.battery_col);
	printf("battery_row: %d\n", params.battery_row);
	printf("cell_battery_col: %d\n", params.cell_battery_col);
	printf("cell_battery_row: %d\n", params.cell_battery_row);
	printf("disarmed_col: %d\n", params.disarmed_col);
	printf("disarmed_row: %d\n", params.disarmed_row);
	printf("status_col: %d\n", params.status_col);
	printf("status_row: %d\n", params.status_row);
	printf("flight_mode_col: %d\n", params.flight_mode_col);
	printf("flight_mode_row: %d\n", params.flight_mode_row);
	printf("latitude_col: %d\n", params.latitude_col);
	printf("latitude_row: %d\n", params.latitude_row);
	printf("longitude_col: %d\n", params.longitude_col);
	printf("longitude_row: %d\n", params.longitude_row);
	printf("to_home_col: %d\n", params.to_home_col);
	printf("to_home_row: %d\n", params.to_home_row);
	printf("crosshair_col: %d\n", params.crosshair_col);
	printf("crosshair_row: %d\n", params.crosshair_row);
	printf("heading_col: %d\n", params.heading_col);
	printf("heading_row: %d\n", params.heading_row);
	printf("vio_col: %d\n", params.vio_col);
	printf("vio_row: %d\n", params.vio_row);
	printf("shortcut: %s\n", shortcut_type_as_string(params.shortcut));
	printf("input protocol: %s\n", input_protocol_as_string(params.input_protocol));
	printf("input type: %s\n", input_type_as_string(params.input_type));
	printf("input port: %s\n", params.input_port);
	printf("output type: %s\n", output_type_as_string(params.output_type));
	printf("output port: %s\n", params.output_port);
	printf("=================================================================");
	printf("\n");

	return 0;
}


int load_config_file(msp_dp_osd_params_t* params, bool print)
{
	// check if the file exists and make a new one if not
	int ret = json_make_empty_file_with_header_if_missing(CONF_FILE, FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Created new json file: %s\n", CONF_FILE);

	// read the data in
	cJSON* parent = json_read_file(CONF_FILE);
	if(parent==NULL) return -1;

	json_fetch_int_with_default(parent, "rssi_col", &params->rssi_col, 0);
	json_fetch_int_with_default(parent, "rssi_row", &params->rssi_row, 0);
	json_fetch_int_with_default(parent, "current_draw_col", &params->current_draw_col, 40);
	json_fetch_int_with_default(parent, "current_draw_row", &params->current_draw_row, 1);
	json_fetch_int_with_default(parent, "battery_col", &params->battery_col, 0);
	json_fetch_int_with_default(parent, "battery_row", &params->battery_row, 16);
	json_fetch_int_with_default(parent, "cell_battery_col", &params->cell_battery_col, 0);
	json_fetch_int_with_default(parent, "cell_battery_row", &params->cell_battery_row, 17);
	json_fetch_int_with_default(parent, "disarmed_col", &params->disarmed_col, 20);
	json_fetch_int_with_default(parent, "disarmed_row", &params->disarmed_row, 13);
	json_fetch_int_with_default(parent, "status_col", &params->status_col, 19);
	json_fetch_int_with_default(parent, "status_row", &params->status_row, 15);
	json_fetch_int_with_default(parent, "flight_mode_col", &params->flight_mode_col, 21);
	json_fetch_int_with_default(parent, "flight_mode_row", &params->flight_mode_row, 17);
	json_fetch_int_with_default(parent, "latitude_col", &params->latitude_col, 39);
	json_fetch_int_with_default(parent, "latitude_row", &params->latitude_row, 15);
	json_fetch_int_with_default(parent, "longitude_col", &params->longitude_col, 39);
	json_fetch_int_with_default(parent, "longitude_row", &params->longitude_row, 16);
	json_fetch_int_with_default(parent, "to_home_col", &params->to_home_col, 43);
	json_fetch_int_with_default(parent, "to_home_row", &params->to_home_row, 14);
	json_fetch_int_with_default(parent, "crosshair_col", &params->crosshair_col, 24);
	json_fetch_int_with_default(parent, "crosshair_row", &params->crosshair_row, 8);
	json_fetch_int_with_default(parent, "heading_col", &params->heading_col, 24);
	json_fetch_int_with_default(parent, "heading_row", &params->heading_row, 1);
	json_fetch_int_with_default(parent, "vio_col", &params->vio_col, 1);
	json_fetch_int_with_default(parent, "vio_row", &params->vio_row, 1);
	json_fetch_int_with_default(parent, "resolution", &params->resolution, -1);
	json_fetch_int_with_default(parent, "font_type", (int*)&params->font_type, -1);
	json_fetch_int_with_default(parent, "input_protocol", (int*)&params->input_protocol, PROTO_MAVLINK);
	json_fetch_int_with_default(parent, "input_type", (int*)&params->input_type, INPUT_MPA);
	json_fetch_string_with_default(parent, "input_port", params->input_port, MAX_PORT_NAME_LEN, "");
	json_fetch_int_with_default(parent, "output_type", (int*)&params->output_type, OUTPUT_NONE);
	json_fetch_string_with_default(parent, "output_port", params->output_port, MAX_PORT_NAME_LEN, "");
	// json_fetch_enum_with_default(parent, "shortcut", (int*)&params->shortcut, shortcut_type_strings, OPT_MAX, OPT_A);

	// check if we got any errors in that process
	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse data in %s\n", CONF_FILE);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		// printf("The JSON config file data was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(CONF_FILE, parent, FILE_HEADER);
	}
	cJSON_Delete(parent);

	if(print) print_config_file(*params);
	return 0;
}
