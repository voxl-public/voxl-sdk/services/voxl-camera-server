/*******************************************************************************
 * Copyright 2025 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with hardware devices provided by
 *    ModalAI® Inc. Reverse engineering or copying this code for use on hardware 
 *    not manufactured by ModalAI is not permitted.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#pragma once

#define STRINGIFY(x) #x
#define OCL_KERNEL(name, k) std::string const name##_str = STRINGIFY(k)

//RGB to YUV
#define RGBA2Y(rgb) ( 0.299*rgb.x + 0.587*rgb.y + 0.114*rgb.z + 0.0625)
#define RGBA2U(rgb) (-0.147*rgb.x - 0.289*rgb.y + 0.436*rgb.z + 0.5)
#define RGBA2V(rgb) ( 0.615*rgb.x - 0.515*rgb.y - 0.100*rgb.z + 0.5)




OCL_KERNEL(debayer_to_yuv,
__kernel void debayer_to_yuv( __read_only  image2d_t           bayer_image,
                                                 sampler_t           sampler,
                                    __write_only __global  uchar *   image_out,
                                    float gain_r, float gain_g, float gain_b, float gamma,
                                    float src_offset_x, float src_offset_y,
                                    float src_width, float src_height, int src_rggb, 
                                    int dst_width, int dst_height,
                                    int dst_line_stride, int dst_plane_stride)
{
    const int    wid_x          = get_global_id(0);
    const int    wid_y          = get_global_id(1);
    const float2 coord          = (float2)(wid_x, wid_y);
    const float2 src_offset     = (float2)(src_offset_x/2,src_offset_y/2);

    const float zoomx           = ((float)src_width) / dst_width;
    const float zoomy           = ((float)src_height) / dst_height;
    const float2 zoom           = (float2)(zoomx,zoomy);

    float2 coords[4];
    coords[0] = src_offset + (coord)*zoom;
    coords[1] = src_offset + (coord + (float2)(0.5, 0.0)) * zoom;
    coords[2] = src_offset + (coord + (float2)(0.0, 0.5)) * zoom;
    coords[3] = src_offset + (coord + (float2)(0.5, 0.5)) * zoom;

    //compensate for subtraction of pedestal
    const float ped      = 16.0/256.0;          //IMX412 has pedestal value of 64 for 10bit output, so 16.0 for 8 bit
    const float ped_gain = 1.0 / (1.0 - ped);   //after subtracting the pedestal, this gain will be used to scale the range back to 0 .. 1.0
    
    gain_r *= ped_gain;
    gain_g *= ped_gain;
    gain_b *= ped_gain;

    //sample the 10- or 12-bit RGGB image with interpolation to get 4 RGGB pixels
    //coordinates for reading 10- or 12-bit RGGB are half the width and height
    //underlying logic assumes the 4 pixels in bayer image make up a single 4-color pixel
    
    //convert 4 bayer RGGB pixels to 4 RGB pixels and apply white balance gains
    float3 yuv_pixels[4];

    const float inv_gamma = 1.0 / gamma;

    //no gamma correction
    for (int i=0; i<4; i++)
    {
        float4 bayer_pixel = read_imagef(bayer_image, sampler, coords[i]);
        //float p[3] = {bayer_pixel.w, bayer_pixel.x, bayer_pixel.w};
        float pix_r = bayer_pixel.x;
        float pix_b = bayer_pixel.w;
        
        //this if without else seems to be adding almost zero overhead (if-else significantly slows things down)
        if (src_rggb == 0){
            pix_r = bayer_pixel.w;
            pix_b = bayer_pixel.x;
        }

        float3 rgb_pixel;
        //rgb_pixel = clamp((float3)(gain_r*(p[src_rggb]-ped), gain_g*0.5f * (bayer_pixel.y + bayer_pixel.z-ped-ped), gain_b*(p[src_rggb+1]-ped)), 0.0, 1.0);
        //rgb_pixel = clamp((float3)(gain_r*(bayer_pixel.x-ped), gain_g*0.5f * (bayer_pixel.y + bayer_pixel.z-ped-ped), gain_b*(bayer_pixel.w-ped)), 0.0, 1.0);
        rgb_pixel = native_powr(clamp((float3)(gain_r*(pix_r-ped), gain_g*0.5f * (bayer_pixel.y + bayer_pixel.z-ped-ped), gain_b*(pix_b-ped)), 0.0, 1.0),inv_gamma);

        yuv_pixels[i] = (float3)(RGBA2Y(rgb_pixel), RGBA2U(rgb_pixel), RGBA2V(rgb_pixel));
        yuv_pixels[i] *= 255.0;  //convert to 0-255 range
    }

    const int line_stride     = dst_line_stride;
    const int uv_plane_offset = dst_plane_stride * line_stride;

    //calculate the coordinates of the 4 Y pixels in output image
    int write_coord0 = wid_y*line_stride*2 + wid_x *2;
    //int write_coord1 = wid_y*line_stride*2 + wid_x *2 +1;
    int write_coord2 = (wid_y*2+1)*line_stride + wid_x *2;
    //int write_coord3 = (wid_y*2+1)*line_stride + wid_x *2 +1;

    //calculate the coordinates of the U and V pixels
    int write_coord_u = uv_plane_offset + wid_y*line_stride + wid_x *2;
    //int write_coord_v = write_coord_u + 1;
    
    //calculate U and V values, which should be average of the 4 pixels
    float u_av = (yuv_pixels[0].y + yuv_pixels[1].y + yuv_pixels[2].y + yuv_pixels[3].y)*0.25;
    float v_av = (yuv_pixels[0].z + yuv_pixels[1].z + yuv_pixels[2].z + yuv_pixels[3].z)*0.25;
    
    //clamp and write the data to output image
    //writing uchar2 is a bit faster than individual bytes
    uchar2 yy0 = convert_uchar2_sat((float2)(yuv_pixels[0].x, yuv_pixels[1].x));
    uchar2 yy1 = convert_uchar2_sat((float2)(yuv_pixels[2].x, yuv_pixels[3].x));
    uchar2 uv  = convert_uchar2_sat((float2)(u_av, v_av));

    uchar2 * yy0_ptr = (uchar2 *)&(image_out[write_coord0]);
    uchar2 * yy1_ptr = (uchar2 *)&(image_out[write_coord2]);
    uchar2 * uv_ptr  = (uchar2 *)&(image_out[write_coord_u]);

    *yy0_ptr = yy0;
    *yy1_ptr = yy1;
    *uv_ptr = uv;
});





OCL_KERNEL(blur_image,
__kernel void blur_image( __read_only            image2d_t            src_image,
                                                 sampler_t            sampler,
                          __write_only __global  uchar *              dst_image,
                          __read_only            qcom_weight_image_t  weight_image,
                                                 int                  dst_line_stride)
{
    int    wid_x          = get_global_id(0);
    int    wid_y          = get_global_id(1);
    float2 src_coord      = (float2)(wid_x, wid_y);

    float val             = qcom_convolve_imagef(src_image, sampler, src_coord, weight_image).x;
    //val                   = wid_x / 255.0;

    int dst_idx           = wid_y * dst_line_stride + wid_x;
    dst_image[dst_idx]    = clamp((float)(val*255.0), 0.0, 255.0);
});

OCL_KERNEL(bayer_downscale_blur_to_yuv_color,
__kernel void bayer_downscale_blur_to_yuv_color(__read_only  image2d_t           bayer_image,
                                                 sampler_t           sampler,
                                                __write_only __global  uchar *   image_out,
                                                int src_offset_x, int src_offset_y,
                                                int src_width, int src_height, int src_rggb, 
                                                int dst_width, int dst_height,
                                                int dst_line_stride, int dst_plane_stride, const qcom_box_size_t box_size)
{
    const int    wid_x          = get_global_id(0);
    const int    wid_y          = get_global_id(1);
    const float2 coord          = (float2)(wid_x, wid_y);
    const float2 src_offset     = (float2)(src_offset_x/2,src_offset_y/2);

    const float zoomx  = ((float)src_width) / dst_width;
    const float zoomy  = ((float)src_height) / dst_height;
    const float2 zoom  = (float2)(zoomx,zoomy);

    float2 coords[4];
    coords[0] = src_offset + (coord)*zoom;
    coords[1] = src_offset + (coord + (float2)(0.5, 0.0)) * zoom;
    coords[2] = src_offset + (coord + (float2)(0.0, 0.5)) * zoom;
    coords[3] = src_offset + (coord + (float2)(0.5, 0.5)) * zoom;

    float4 bayer_pixels[4];
    float3 rgb_pixels[4];
    float3 yuv_pixels[4];
    for (int i=0; i<4; i++) {
        bayer_pixels[i] = qcom_box_filter_imagef(bayer_image, sampler, coords[i], box_size);
        //bayer_pixels[i] = read_imagef(bayer_image, sampler, coords[i]);
        rgb_pixels[i]   = (float3)(bayer_pixels[i].x, 0.5*(bayer_pixels[i].y + bayer_pixels[i].z), bayer_pixels[i].w);   //convert bayer to RGB
        yuv_pixels[i]   = (float3)(RGBA2Y(rgb_pixels[i]), RGBA2U(rgb_pixels[i]), RGBA2V(rgb_pixels[i]));                 //convert RGB to YUV
    }

    const int line_stride     = dst_line_stride;
    const int uv_plane_offset = dst_plane_stride * line_stride;

    //calculate the coordinates of the 4 Y pixels in output image
    int write_coord_y0 = wid_y*line_stride*2 + wid_x *2;
    int write_coord_y2 = (wid_y*2+1)*line_stride + wid_x *2;

    //calculate the coordinates of the U and V pixels
    int write_coord_uv = uv_plane_offset + wid_y*line_stride + wid_x *2;   //is this correct??
    //int write_coord_v = write_coord_u + 1;
    
    //calculate U and V values, which should be average of the 4 pixels
    float u_av = (yuv_pixels[0].y + yuv_pixels[1].y + yuv_pixels[2].y + yuv_pixels[3].y)*0.25;
    float v_av = (yuv_pixels[0].z + yuv_pixels[1].z + yuv_pixels[2].z + yuv_pixels[3].z)*0.25;
    
    //clamp and write the data to output image
    //writing uchar2 is a bit faster than individual bytes
    uchar2 yy0 = (uchar2)(clamp(yuv_pixels[0].x * 255.0, 0.0, 255.0),clamp(yuv_pixels[1].x * 255.0, 0.0, 255.0));
    uchar2 yy1 = (uchar2)(clamp(yuv_pixels[2].x * 255.0, 0.0, 255.0),clamp(yuv_pixels[3].x * 255.0, 0.0, 255.0));
    uchar2 uv  = (uchar2)(clamp(u_av * 255.0, 0.0, 255.0), clamp(v_av * 255.0, 0.0, 255.0));

    uchar2 * yy0_ptr = (uchar2 *)&(image_out[write_coord_y0]);
    uchar2 * yy1_ptr = (uchar2 *)&(image_out[write_coord_y2]);
    uchar2 * uv_ptr  = (uchar2 *)&(image_out[write_coord_uv]);

    *yy0_ptr = yy0;
    *yy1_ptr = yy1;
    //*uv_ptr = uv;
}

__kernel void raw8_downscale_blur_to_raw8(__read_only  image2d_t           bayer_image,
                                                 sampler_t           sampler,
                                                __write_only __global  uchar *   image_out,
                                                int src_offset_x, int src_offset_y,
                                                int src_width, int src_height, int src_rggb, 
                                                int dst_width, int dst_height,
                                                int dst_line_stride, int dst_plane_stride, const qcom_box_size_t box_size)
{
    const int    wid_x          = get_global_id(0);
    const int    wid_y          = get_global_id(1);
    const float2 coord          = (float2)(wid_x*2, wid_y*2);
    const float2 src_offset     = (float2)(src_offset_x,src_offset_y);

    const float zoomx  = ((float)src_width) / dst_width;
    const float zoomy  = ((float)src_height) / dst_height;
    const float2 zoom  = (float2)(zoomx,zoomy);

    float2 coords[4];
    coords[0] = src_offset + (coord)*zoom;
    coords[1] = src_offset + (coord + (float2)(1.0, 0.0)) * zoom;
    coords[2] = src_offset + (coord + (float2)(0.0, 1.0)) * zoom;
    coords[3] = src_offset + (coord + (float2)(1.0, 1.0)) * zoom;

    float out_pixels[4];

    for (int i=0; i<4; i++) {
        out_pixels[i] = qcom_box_filter_imagef(bayer_image, sampler, coords[i], box_size).x * 255.0;
    }

    const int line_stride     = dst_line_stride;
    const int uv_plane_offset = dst_plane_stride * line_stride;

    //calculate the coordinates of the 4 Y pixels in output image
    int write_coord_y0 = wid_y*line_stride*2 + wid_x*2;
    int write_coord_y2 = (wid_y*2+1)*line_stride + wid_x*2;

    //clamp and write the data to output image
    //writing uchar2 is a bit faster than individual bytes
    //uchar2 yy0 = (uchar2)(clamp(yuv_pixels[0].x * 255.0, 0.0, 255.0),clamp(yuv_pixels[1].x * 255.0, 0.0, 255.0));
    //uchar2 yy1 = (uchar2)(clamp(yuv_pixels[2].x * 255.0, 0.0, 255.0),clamp(yuv_pixels[3].x * 255.0, 0.0, 255.0));
    uchar2 yy0 = (uchar2)(out_pixels[0],out_pixels[1]);
    uchar2 yy1 = (uchar2)(out_pixels[2],out_pixels[3]);

    uchar2 * yy0_ptr = (uchar2 *)&(image_out[write_coord_y0]);
    uchar2 * yy1_ptr = (uchar2 *)&(image_out[write_coord_y2]);

    *yy0_ptr = yy0;
    *yy1_ptr = yy1;

/*
    int write_coord_uv = uv_plane_offset + wid_y*line_stride + wid_x*2;
    //uchar2 uv  = (uchar2)(clamp(u_av * 255.0, 0.0, 255.0), clamp(v_av * 255.0, 0.0, 255.0));
    uchar2 uv  = (uchar2)(127,127);
    uchar2 * uv_ptr  = (uchar2 *)&(image_out[write_coord_uv]);
    *uv_ptr = uv;
*/
}

);


OCL_KERNEL(bayer_to_yuv_color_norm,

__kernel void bayer_to_yuv_color_norm_old( __read_only  image2d_t           bayer_image,
                                                 sampler_t           sampler,
                                    __write_only __global  uchar *   image_out,
                                    float gain_r, float gain_g, float gain_b, float gamma,
                                    int src_offset_x, src_offset_y,
                                    int src_width, int src_height, int src_rggb, 
                                    int dst_width, int dst_height,
                                    int dst_line_stride, int dst_plane_stride,
                                    __read_only  image2d_t scratch_image)  //mean   /__write_only __global  uchar * scratch_image
{
    const int    wid_x          = get_global_id(0);
    const int    wid_y          = get_global_id(1);
    const float2 coord          = (float2)(wid_x, wid_y);
    const float2 src_offset     = (float2)(src_offset_x/2,src_offset_y/2);

    const float zoomx  = ((float)src_width) / dst_width;
    const float zoomy  = ((float)src_height) / dst_height;
    const float2 zoom  = (float2)(zoomx,zoomy);

    float2 coords[4];
    coords[0] = src_offset + (coord)*zoom;
    coords[1] = src_offset + (coord + (float2)(0.5, 0.0)) * zoom;
    coords[2] = src_offset + (coord + (float2)(0.0, 0.5)) * zoom;
    coords[3] = src_offset + (coord + (float2)(0.5, 0.5)) * zoom;

    //FIXME: how to deal with offset if any??
    //use the same scratch coordinate for 4 output pixels (interpolate coordinate)
    float2 scratch_coord = (coord + (float2)(0.25, 0.25)) * 2 / 16;
    //float2 scratch_coord = (coord00/zoom*2) / 16;

/*
    float2 scratch_coord = coord00 / 16;
    int scratch_width  = dst_width / 16;
    int scratch_height = dst_height / 16;
    int scratch_idx    = (wid_y*2/16) * scratch_width + (wid_x*2/16);
    uchar scratch_val  = scratch_image[scratch_idx];
    float scratch_valf = (float)(scratch_val) / 255.0 * 1.5;
*/
    
    float4 scratch_valf4 = read_imagef(scratch_image, sampler, scratch_coord);
    float scratch_valf   = scratch_valf4.x;
    //float scratch_valf = clamp(scratch_valf4.x * 2.0,0.0,1.0);
    uchar scratch_val  = (uchar)(scratch_valf*255.0);

    

    //compensate for subtraction of pedestal
    const float ped      = 0*16.0/256.0;          //IMX412 has pedestal value of 64 for 10bit output, so 16.0 for 8 bit
    const float ped_gain = 1.0 / (1.0 - ped);   //after subtracting the pedestal, this gain will be used to scale the range back to 0 .. 1.0
    
    gain_r *= ped_gain;
    gain_g *= ped_gain;
    gain_b *= ped_gain;

    //sample the 10-bit or 12-bit RGGB image with interpolation to get 4 RGGB pixels
    //coordinates for reading 10-bit RGGB are half the width and height
    //underlying logic assumes the 4 pixels in bayer image make up a single 4-color pixel

    const float3 MIN_RGB = (float3)(0.0, 0.0, 0.0);
    const float3 MAX_RGB = (float3)(1.0, 1.0, 1.0);

    //const float3 gamma_vec = (float3)(1.0/gamma, 1.0/gamma, 1.0/gamma);
    
    //convert 4 bayer RGGB pixels to 4 RGB pixels, subtract pedestal and apply white balance gains
    float4 bayer_pixels[4];
    float3 rgb_pixels[4];
    float3 yuv_pixels[4];

    for (int i=0; i<4; i++)
    {
        bayer_pixels[i] = read_imagef(bayer_image, sampler, coords[i]);

        float pix_r = bayer_pixels[i].x;
        float pix_b = bayer_pixels[i].w;
        
        //this if without else seems to be adding almost zero overhead (if-else significantly slows things down)
        if (src_rggb == 0){
            pix_r = bayer_pixels[i].w;
            pix_b = bayer_pixels[i].x;
        }

        
        rgb_pixels[i] = clamp((float3)(gain_r*(pix_r-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z-ped-ped), gain_b*(pix_b-ped)), MIN_RGB, MAX_RGB);
        //rgb_pixels[i] = clamp((float3)(gain_r*(bayer_pixels[i].x-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z-ped-ped), gain_b*(bayer_pixels[i].w-ped)), MIN_RGB, MAX_RGB);

        //rgb_pixels[i] = native_powr(clamp((float3)(gain_r*(pix_r-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z), (pix_b-ped)), MIN_RGB, MAX_RGB), gamma_vec);

        //convert RGB to YUV
        yuv_pixels[i] = (float3)(RGBA2Y(rgb_pixels[i]), RGBA2U(rgb_pixels[i]), RGBA2V(rgb_pixels[i]));
        yuv_pixels[i].x /= (2.0*scratch_valf);
    }


    const int line_stride     = dst_line_stride;
    const int uv_plane_offset = dst_plane_stride * line_stride;

    //calculate the coordinates of the 4 Y pixels in output image
    int write_coord_y0 = wid_y*line_stride*2 + wid_x *2;
    int write_coord_y2 = (wid_y*2+1)*line_stride + wid_x *2;

    //clamp and write the data to output image
    //writing uchar2 is a bit faster than individual bytes
    uchar2 yy0 = (uchar2)(clamp(yuv_pixels[0].x * 255.0, 0.0, 255.0),clamp(yuv_pixels[1].x * 255.0, 0.0, 255.0));
    uchar2 yy1 = (uchar2)(clamp(yuv_pixels[2].x * 255.0, 0.0, 255.0),clamp(yuv_pixels[3].x * 255.0, 0.0, 255.0));
    
    uchar2 * yy0_ptr = (uchar2 *)&(image_out[write_coord_y0]);
    uchar2 * yy1_ptr = (uchar2 *)&(image_out[write_coord_y2]);
    
    //hack to see the scratch image
    //yy0 = (uchar2)(scratch_val,scratch_val);
    //yy1 = (uchar2)(scratch_val,scratch_val);
    
    *yy0_ptr = yy0;
    *yy1_ptr = yy1;
    

/*
    //calculate the coordinates of the U and V pixels
    int write_coord_uv = uv_plane_offset + wid_y*line_stride + wid_x *2;

    //calculate U and V values, which should be average of the 4 pixels
    float u_av = (yuv_pixels[0].y + yuv_pixels[1].y + yuv_pixels[2].y + yuv_pixels[3].y)*0.25;
    float v_av = (yuv_pixels[0].z + yuv_pixels[1].z + yuv_pixels[2].z + yuv_pixels[3].z)*0.25;
    uchar2 uv  = (uchar2)(clamp(u_av * 255.0, 0.0, 255.0), clamp(v_av * 255.0, 0.0, 255.0));
    uchar2 * uv_ptr  = (uchar2 *)&(image_out[write_coord_uv]);

    *uv_ptr = uv;
*/
}

__kernel void bayer_to_yuv_color_norm( __read_only  image2d_t           bayer_image,
                                                 sampler_t           sampler,
                                    __write_only __global  uchar *   image_out,
                                    float gain_r, float gain_g, float gain_b, float gamma,
                                    int src_offset_x, src_offset_y,
                                    int src_width, int src_height, int src_rggb, 
                                    int dst_width, int dst_height,
                                    int dst_line_stride, int dst_plane_stride,
                                    __read_only  image2d_t scratch_image)  //mean   /__write_only __global  uchar * scratch_image
{
    const int    wid_x          = get_global_id(0);
    const int    wid_y          = get_global_id(1);
    const float2 coord          = (float2)(wid_x, wid_y);
    const float2 src_offset     = (float2)(src_offset_x/2,src_offset_y/2);

    const float zoomx  = ((float)src_width) / dst_width;
    const float zoomy  = ((float)src_height) / dst_height;
    const float2 zoom  = (float2)(zoomx,zoomy);

    float2 coords[4];
    coords[0] = src_offset + (coord)*zoom;
    coords[1] = src_offset + (coord + (float2)(0.5, 0.0)) * zoom;
    coords[2] = src_offset + (coord + (float2)(0.0, 0.5)) * zoom;
    coords[3] = src_offset + (coord + (float2)(0.5, 0.5)) * zoom;

    //FIXME: how to deal with offset if any??
    //use the same scratch coordinate for 4 output pixels (interpolate coordinate)
    float2 scratch_coord = (coord + (float2)(0.25, 0.25)) * 2 / 16;
    //float2 scratch_coord = (coord00/zoom*2) / 16;

/*
    float2 scratch_coord = coord00 / 16;
    int scratch_width  = dst_width / 16;
    int scratch_height = dst_height / 16;
    int scratch_idx    = (wid_y*2/16) * scratch_width + (wid_x*2/16);
    uchar scratch_val  = scratch_image[scratch_idx];
    float scratch_valf = (float)(scratch_val) / 255.0 * 1.5;
*/
    
    float scratch_valf = read_imagef(scratch_image, sampler, scratch_coord).x;
    uchar scratch_val  = (uchar)(scratch_valf*255.0);

    

    //compensate for subtraction of pedestal
    const float ped      = 0*16.0/256.0;          //IMX412 has pedestal value of 64 for 10bit output, so 16.0 for 8 bit
    const float ped_gain = 1.0 / (1.0 - ped);   //after subtracting the pedestal, this gain will be used to scale the range back to 0 .. 1.0
    
    gain_r *= ped_gain;
    gain_g *= ped_gain;
    gain_b *= ped_gain;

    //sample the 10-bit or 12-bit RGGB image with interpolation to get 4 RGGB pixels
    //coordinates for reading 10-bit RGGB are half the width and height
    //underlying logic assumes the 4 pixels in bayer image make up a single 4-color pixel
    
    //convert 4 bayer RGGB pixels to 4 RGB pixels, subtract pedestal and apply white balance gains
    float4 bayer_pixels[4];
    float3 rgb_pixels[4];
    float3 yuv_pixels[4];

    for (int i=0; i<4; i++)
    {
        bayer_pixels[i] = read_imagef(bayer_image, sampler, coords[i]);

        float pix_r = bayer_pixels[i].x;
        float pix_b = bayer_pixels[i].w;
        
        //this if without else seems to be adding almost zero overhead (if-else significantly slows things down)
        if (src_rggb == 0){
            pix_r = bayer_pixels[i].w;
            pix_b = bayer_pixels[i].x;
        }

        
        rgb_pixels[i] = clamp((float3)(gain_r*(pix_r-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z-ped-ped), gain_b*(pix_b-ped)), 0.0, 1.0);
        //rgb_pixels[i] = clamp((float3)(gain_r*(bayer_pixels[i].x-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z-ped-ped), gain_b*(bayer_pixels[i].w-ped)), 0.0, 1.0);

        //rgb_pixels[i] = native_powr(clamp((float3)(gain_r*(pix_r-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z), (pix_b-ped)), MIN_RGB, MAX_RGB), gamma_vec);

        //convert RGB to YUV
        yuv_pixels[i] = (float3)(RGBA2Y(rgb_pixels[i]), RGBA2U(rgb_pixels[i]), RGBA2V(rgb_pixels[i]));
        //yuv_pixels[i].x /= (2.0*scratch_valf);
        yuv_pixels[i].x = clamp(yuv_pixels[i].x/(2.0*scratch_valf), 0.0, 1.0) * 255.0;  //clamp and convert to 0-255
    }

    const int line_stride     = dst_line_stride;
    const int uv_plane_offset = dst_plane_stride * line_stride;

    //calculate the coordinates of the 4 Y pixels in output image
    int write_coord_y0 = wid_y*line_stride*2 + wid_x *2;
    int write_coord_y2 = (wid_y*2+1)*line_stride + wid_x *2;

    //clamp and write the data to output image
    //writing uchar2 is a bit faster than individual bytes
    uchar2 yy0 = (uchar2)(yuv_pixels[0].x,yuv_pixels[1].x);
    uchar2 yy1 = (uchar2)(yuv_pixels[2].x,yuv_pixels[3].x);
    
    uchar2 * yy0_ptr = (uchar2 *)&(image_out[write_coord_y0]);
    uchar2 * yy1_ptr = (uchar2 *)&(image_out[write_coord_y2]);
    
    //hack to see the scratch image
    //yy0 = (uchar2)(scratch_val,scratch_val);
    //yy1 = (uchar2)(scratch_val,scratch_val);
    
    *yy0_ptr = yy0;
    *yy1_ptr = yy1;
    

/*
    //calculate the coordinates of the U and V pixels
    int write_coord_uv = uv_plane_offset + wid_y*line_stride + wid_x *2;

    //calculate U and V values, which should be average of the 4 pixels
    float u_av = (yuv_pixels[0].y + yuv_pixels[1].y + yuv_pixels[2].y + yuv_pixels[3].y)*0.25;
    float v_av = (yuv_pixels[0].z + yuv_pixels[1].z + yuv_pixels[2].z + yuv_pixels[3].z)*0.25;
    uchar2 uv  = (uchar2)(clamp(u_av * 255.0, 0.0, 255.0), clamp(v_av * 255.0, 0.0, 255.0));
    uchar2 * uv_ptr  = (uchar2 *)&(image_out[write_coord_uv]);

    *uv_ptr = uv;
*/
}


__kernel void raw10_12_to_mono8_norm( __read_only  image2d_t           bayer_image,
                                                 sampler_t           sampler,
                                    __write_only __global  uchar *   image_out,
                                    float gain_r, float gain_g, float gain_b, float gamma,
                                    int src_offset_x, src_offset_y,
                                    int src_width, int src_height, int src_rggb, 
                                    int dst_width, int dst_height,
                                    int dst_line_stride, int dst_plane_stride,
                                    __read_only  image2d_t scratch_image)  //mean   /__write_only __global  uchar * scratch_image
{
    const int    wid_x          = get_global_id(0);
    const int    wid_y          = get_global_id(1);
    const float2 coord          = (float2)(wid_x, wid_y);
    const float2 src_offset     = (float2)(src_offset_x/2,src_offset_y/2);

    const float zoomx  = ((float)src_width) / dst_width;
    const float zoomy  = ((float)src_height) / dst_height;
    const float2 zoom  = (float2)(zoomx,zoomy);

    float2 coords[4];
    coords[0] = src_offset + (coord)*zoom;
    coords[1] = src_offset + (coord + (float2)(0.5, 0.0)) * zoom;
    coords[2] = src_offset + (coord + (float2)(0.0, 0.5)) * zoom;
    coords[3] = src_offset + (coord + (float2)(0.5, 0.5)) * zoom;

    //FIXME: how to deal with offset if any??
    //use the same scratch coordinate for 4 output pixels (interpolate coordinate)
    float2 scratch_coord = (coord + (float2)(0.25, 0.25)) * 2 / 16;
    //float2 scratch_coord = (coord00/zoom*2) / 16;
    
    float scratch_valf = read_imagef(scratch_image, sampler, scratch_coord).x;

    //compensate for subtraction of pedestal
    const float ped      = 0*16.0/256.0;          //IMX412 has pedestal value of 64 for 10bit output, so 16.0 for 8 bit
    const float ped_gain = 1.0 / (1.0 - ped);   //after subtracting the pedestal, this gain will be used to scale the range back to 0 .. 1.0
    
    gain_r *= ped_gain;
    gain_g *= ped_gain;
    gain_b *= ped_gain;

    //sample the 10-bit or 12-bit RGGB image with interpolation to get 4 RGGB pixels
    //coordinates for reading 10-bit RGGB are half the width and height
    //underlying logic assumes the 4 pixels in bayer image make up a single 4-color pixel
    
    float pixels[4];

    for (int i=0; i<4; i++)
    {
        float pix = read_imagef(bayer_image, sampler, coords[i]).x;
        pixels[i] = clamp(pix/(2.0*scratch_valf), 0.0, 1.0) * 255.0;  //clamp and convert to 0-255
    }

    const int line_stride     = dst_line_stride;
    const int uv_plane_offset = dst_plane_stride * line_stride;

    //calculate the coordinates of the 4 Y pixels in output image
    int write_coord_y0 = wid_y*line_stride*2 + wid_x *2;
    int write_coord_y2 = (wid_y*2+1)*line_stride + wid_x *2;

    //clamp and write the data to output image
    //writing uchar2 is a bit faster than individual bytes
    uchar2 yy0 = (uchar2)(pixels[0],pixels[1]);
    uchar2 yy1 = (uchar2)(pixels[2],pixels[3]);
    
    uchar2 * yy0_ptr = (uchar2 *)&(image_out[write_coord_y0]);
    uchar2 * yy1_ptr = (uchar2 *)&(image_out[write_coord_y2]);
    
    //hack to see the scratch image
    //uchar scratch_val  = (uchar)(scratch_valf*255.0);
    //yy0 = (uchar2)(scratch_val,scratch_val);
    //yy1 = (uchar2)(scratch_val,scratch_val);
    
    *yy0_ptr = yy0;
    *yy1_ptr = yy1;
}



__kernel void raw8_to_raw8_norm( __read_only  image2d_t           bayer_image,
                                                 sampler_t           sampler,
                                    __write_only __global  uchar *   image_out,
                                    float gain_r, float gain_g, float gain_b, float gamma,
                                    int src_offset_x, src_offset_y,
                                    int src_width, int src_height, int src_rggb, 
                                    int dst_width, int dst_height,
                                    int dst_line_stride, int dst_plane_stride,
                                    __read_only  image2d_t scratch_image)  //mean   /__write_only __global  uchar * scratch_image
{
    const int    wid_x          = get_global_id(0);
    const int    wid_y          = get_global_id(1);
    const float2 coord          = (float2)(wid_x*2, wid_y*2);
    const float2 src_offset     = (float2)(src_offset_x,src_offset_y);

    const float zoomx  = ((float)src_width) / dst_width;
    const float zoomy  = ((float)src_height) / dst_height;
    const float2 zoom  = (float2)(zoomx,zoomy);

    float2 coords[4];
    coords[0] = src_offset + (coord)*zoom;
    coords[1] = src_offset + (coord + (float2)(1.0, 0.0)) * zoom;
    coords[2] = src_offset + (coord + (float2)(0.0, 1.0)) * zoom;
    coords[3] = src_offset + (coord + (float2)(1.0, 1.0)) * zoom;

    //FIXME: how to deal with offset if any??
    //use the same scratch coordinate for 4 output pixels (interpolate coordinate)
    float2 scratch_coord = (coord + (float2)(0.5, 0.5)) / 16;

    float scratch_valf = read_imagef(scratch_image, sampler, scratch_coord).x;
        
    float pixels[4];

    for (int i=0; i<4; i++)
    {
        pixels[i]  = read_imagef(bayer_image, sampler, coords[i]).x;
        pixels[i] /= (2.0*scratch_valf);
        pixels[i]  = clamp(pixels[i]*255.0, 0.0, 255.0);
    }

    const int line_stride     = dst_line_stride;
    const int uv_plane_offset = dst_plane_stride * line_stride;

    //calculate the coordinates of the 4 Y pixels in output image
    int write_coord_y0 = wid_y*line_stride*2 + wid_x*2;
    int write_coord_y2 = (wid_y*2+1)*line_stride + wid_x*2;

    //clamp and write the data to output image
    //writing uchar2 is a bit faster than individual bytes
    uchar2 yy0 = (uchar2)(pixels[0],pixels[1]);
    uchar2 yy1 = (uchar2)(pixels[2],pixels[3]);
    
    uchar2 * yy0_ptr = (uchar2 *)&(image_out[write_coord_y0]);
    uchar2 * yy1_ptr = (uchar2 *)&(image_out[write_coord_y2]);
    
    #if 0
    //hack to see the scratch image
    uchar scratch_val  = (uchar)(scratch_valf*255.0);
    yy0 = (uchar2)(scratch_val,scratch_val);
    yy1 = (uchar2)(scratch_val,scratch_val);
    #endif
    
    *yy0_ptr = yy0;
    *yy1_ptr = yy1;
}

);


OCL_KERNEL(ar0144_lsc,

float ar0144_get_lsc_gain(float2 coord, int src_width, int src_height)
{
    float x = coord.x - src_width / 2;
    float y = coord.y - src_height / 2;
    float r = native_sqrt(x*x + y*y);
    //float g = 1.066e-09*(r*r*r*r) -1.042e-06*(r*r*r) - 0.0001265*(r*r) - 0.03029*r + 220.0;
    float g = 1.1e-09*(r*r*r*r) -1.042e-06*(r*r*r) - 0.0001265*(r*r) - 0.03029*r + 220.0;
    //float g = 1.0e-09*(r*r*r*r) -1.042e-06*(r*r*r) - 0.00013*(r*r) - 0.03029*r + 220.0;
    float o = (220.0f / g);

    o = clamp(o,0.0,8.0);
    return o;
}

__kernel void ar0144_12bit_lsc( __read_only  __global uchar * image_in,
                                             __global uchar * image_out,
                                             __global uchar * image_out_no_lsc,
                                            int src_width, int src_height, int src_stride,
                                            int dst_stride)
{
    const uint input_chunk_size  = 3;
    const uint output_chunk_size = 2;

    uint image_column      = get_global_id(0)*output_chunk_size;
    uint image_row         = get_global_id(1);
    uint row_offset        = image_row * src_stride;
    uint chunk_idx         = image_column / output_chunk_size;
    uint row_offset_dst    = image_row * dst_stride;
    uint chunk_src_offset  = (row_offset + chunk_idx * input_chunk_size);
    uint write_offset      = row_offset_dst + chunk_idx*output_chunk_size;
    uint write_offset_raw8 = image_row * src_width + chunk_idx*output_chunk_size;

/*
    //no LSC
    uchar2 ab    = (uchar2)(image_in[chunk_src_offset], image_in[chunk_src_offset+1]);
    uchar2 * out = &(image_out[write_offset]);
    *out         = ab;
*/

/*
    //8-bit LSC
    float2 ab      = (float2)(image_in[chunk_src_offset], image_in[chunk_src_offset+1]);
    float lsc_gain = ar0144_get_lsc_gain((float2)(image_column,image_row), src_width, src_height);
    ab *= lsc_gain;
    ab  = clamp(ab, (float2)(0,0), (float2)(255.0,255.0));

    uchar2 * out = &(image_out[write_offset]);
    *out = (uchar2)(ab.x, ab.y);

*/

    float2 ab      = (float2)(image_in[chunk_src_offset]*16, image_in[chunk_src_offset+1]*16);  //MSB of two pixels, multiply by 16
    uchar c        = image_in[chunk_src_offset+2]; //4 LSB of each pixel
    
    ab.x += c & 0x0F; //add 4 LSB to each pixel
    ab.y += c >> 4;
    ab   /= 16.0;

    float2 ab_no_lsc = ab;
    
    //float lsc_gain_a = ar0144_get_lsc_gain((float2)(image_column,image_row), src_width, src_height);
    //float lsc_gain_b = ar0144_get_lsc_gain((float2)(image_column+1,image_row), src_width, src_height);


    //HACK use the same gain for both pixels (interpolate)
    float lsc_gain = ar0144_get_lsc_gain((float2)(image_column+0.5,image_row), src_width, src_height);

    ab *=lsc_gain;
    ab  = clamp(ab, 0.0, 255.0);

    uchar2 * out = &(image_out[write_offset]);
    *out = convert_uchar2(ab);

    uchar2 * out_no_lsc = &(image_out_no_lsc[write_offset_raw8]);
    *out_no_lsc = convert_uchar2(ab_no_lsc);


/*
    uchar a = image_in[chunk_src_offset];
    uchar b = image_in[chunk_src_offset+1];
    uchar2 * out = &(image_out[write_offset]);
    *out = (uchar2)(a,b);
*/

/*
    uchar a = image_in[chunk_src_offset];
    uchar b = image_in[chunk_src_offset+1];
    
    image_out[write_offset]   = a;
    image_out[write_offset+1] = b;
*/
}

__kernel void ar0144_10bit_lsc( __read_only  __global uchar * image_in,
                                             __global uchar * image_out,
                                             __global uchar * image_out_no_lsc,
                                            int src_width, int src_height, int src_stride,
                                            int dst_stride)
{
    const uint input_chunk_size  = 5;
    const uint output_chunk_size = 4;

    uint image_column      = get_global_id(0)*output_chunk_size;
    uint image_row         = get_global_id(1);
    uint row_offset        = image_row * src_stride;
    uint chunk_idx         = image_column / output_chunk_size;
    uint row_offset_dst    = image_row * dst_stride;
    uint chunk_src_offset  = (row_offset + chunk_idx * input_chunk_size);
    uint write_offset      = row_offset_dst + chunk_idx*output_chunk_size;
    uint write_offset_raw8 = image_row * src_width + chunk_idx*output_chunk_size;

    float4 abcd      = (float4)(image_in[chunk_src_offset], 
                                image_in[chunk_src_offset+1],
                                image_in[chunk_src_offset+2],
                                image_in[chunk_src_offset+3]) * 4.0;

    uchar e        = image_in[chunk_src_offset+4]; //2 LSB of each pixel

    abcd.s0 += (e & 0x03); e >>=2;
    abcd.s1 += (e & 0x03); e >>=2;
    abcd.s2 += (e & 0x03); e >>=2;
    abcd.s3 += (e & 0x03);

    abcd /= 4.0;

    float4 abcd_no_lsc = abcd;

    //HACK use the same gain for two pixels (interpolate)
    float lsc_gain_01 = ar0144_get_lsc_gain((float2)(image_column+0.5,image_row), src_width, src_height);
    float lsc_gain_23 = ar0144_get_lsc_gain((float2)(image_column+1.5,image_row), src_width, src_height);

    abcd.s01 *= lsc_gain_01;
    abcd.s23 *= lsc_gain_23;

    abcd  = clamp(abcd, 0.0, 255.0);

    uchar4 * out        = &(image_out[write_offset]);
    *out                = convert_uchar4(abcd);

    uchar4 * out_no_lsc = &(image_out_no_lsc[write_offset_raw8]);
    *out_no_lsc         = convert_uchar4(abcd_no_lsc);
}

__kernel void ar0144_8bit_lsc( __read_only  __global uchar * image_in,
                                             __global uchar * image_out,
                                             __global uchar * image_out_no_lsc,
                                            int src_width, int src_height, int src_stride,
                                            int dst_stride)
{
    const uint input_chunk_size  = 1;
    const uint output_chunk_size = 1;

    uint image_column      = get_global_id(0)*output_chunk_size;
    uint image_row         = get_global_id(1);
    uint row_offset        = image_row * src_stride;
    uint chunk_idx         = image_column / output_chunk_size;
    uint row_offset_dst    = image_row * dst_stride;
    uint chunk_src_offset  = (row_offset + chunk_idx * input_chunk_size);
    uint write_offset      = row_offset_dst + chunk_idx*output_chunk_size;
    uint write_offset_raw8 = image_row * src_width + chunk_idx*output_chunk_size;

    float a = (float)image_in[chunk_src_offset];

    //HACK use the same gain for two pixels (interpolate)
    float lsc_gain = ar0144_get_lsc_gain((float2)(image_column,image_row), src_width, src_height);

    a *= lsc_gain;
    a  = clamp(a, 0.0, 255.0);

    uchar * out = &(image_out[write_offset]);
    *out        = convert_uchar(a);
}

);

OCL_KERNEL(transform,

//RGB to YUV
#define RGB2Y(rgb) ( 0.299*rgb.x + 0.587*rgb.y + 0.114*rgb.z + 0.0625)
#define RGB2U(rgb) (-0.147*rgb.x - 0.289*rgb.y + 0.436*rgb.z + 0.5)
#define RGB2V(rgb) ( 0.615*rgb.x - 0.515*rgb.y - 0.100*rgb.z + 0.5)

typedef struct __attribute__ ((packed))
{
    float H[9];
} EisParams;


float2 Rot3D(float2 xy, float * H){
    float x = xy.x;
    float y = xy.y;
    //const __constant float * H = p->H;

    float zz =  H[6]*x + H[7]*y + H[8];
    float xx = (H[0]*x + H[1]*y + H[2]) / zz;
    float yy = (H[3]*x + H[4]*y + H[5]) / zz;

    return (float2)(xx,yy);
}

float2 distort_coord(float2 coord, int src_width, int src_height){
    const float fx_new = 800;
    const float fy_new = fx_new;
    const float cx_new = src_width * 0.25;
    const float cy_new = src_height * 0.25;

    float xwu = (coord.x - cx_new) / fx_new;
    float ywu = (coord.y - cy_new) / fy_new;

    float r     = native_sqrt(xwu*xwu + ywu*ywu);
    float theta = atan2(r, 1.0);
    //float theta = atan2pi(r, 1.0);

    float D[4]; // = {0.0, 0.0, 0.0, 0.0};
    D[0] = D[1] = D[2] = D[3] = 0.0;

    // Perform distortion (forward map)
    float theta2  = native_powr(theta,2);
    float theta4  = theta2*theta2;
    float theta_d = theta*(1 + D[0]*theta2        + D[1]*theta4 +
                               D[2]*theta4*theta2 + D[3]*theta4*theta4);

    //float theta_d = theta;


    // now find the world-frame location when distorted
    float xwd = (theta_d/r)*xwu;
    float ywd = (theta_d/r)*ywu;

    // convert the distorted world coordinate to pixel coordinate
    // in the original distorted image
    //float xd = (fx * xwd) + cx;
    //float yd = (fy * ywd) + cy;

    float xd = (fx_new*1.3 * xwd) + cx_new;
    float yd = (fy_new*1.3 * ywd) + cy_new;
    //float xd = (fx_new*4.0 * xwd) + cx_new;
    //float yd = (fy_new*4.0 * ywd) + cy_new;

    return (float2)(xd,yd);
}

__kernel void transform( __read_only  image2d_t           bayer_image,
                                                 sampler_t           sampler,
                                    __write_only __global  uchar *   image_out,
                                    float gain_r, float gain_g, float gain_b, float gamma,
                                    int src_offset_x, src_offset_y,
                                    int src_width, int src_height, int src_rggb, 
                                    int dst_width, int dst_height,
                                    int dst_line_stride, int dst_plane_stride,
                                    float H00, float H01, float H02,
                                    float H10, float H11, float H12,
                                    float H20, float H21, float H22)
{
    const int    wid_x          = get_global_id(0);
    const int    wid_y          = get_global_id(1);
    const float2 coord          = (float2)(wid_x, wid_y);   //+0.5
    const float2 src_offset     = (float2)(src_offset_x/2,src_offset_y/2);

    const float zoomx  = ((float)src_width) / dst_width;
    const float zoomy  = ((float)src_height) / dst_height;
    const float2 zoom  = (float2)(zoomx,zoomy);

    float2 coords[4];

    coords[0] = (src_offset + (coord)                      * zoom);
    coords[1] = (src_offset + (coord + (float2)(0.5, 0.0)) * zoom);
    coords[2] = (src_offset + (coord + (float2)(0.0, 0.5)) * zoom);
    coords[3] = (src_offset + (coord + (float2)(0.5, 0.5)) * zoom);
/*
    for (int i=0; i<4; i++)
    {
        coords[i] = Rot3D(coords[i]*2.0, &H[0]) / 2.0;
    }
*/
    for (int i=0; i<4; i++)
    {   
        float x = coords[i].x * 2.0;
        float y = coords[i].y * 2.0;
        
        float zz =  H20*x + H21*y + H22;
        float xx = (H00*x + H01*y + H02) / zz;
        float yy = (H10*x + H11*y + H12) / zz;

        coords[i].x = xx * 0.5;
        coords[i].y = yy * 0.5;

        coords[i] = distort_coord(coords[i], src_width, src_height);
    }

    for (int i=0; i<4; i++)
    {
        //coords[i] = distort_coord(coords[i], src_width, src_height);
        /*
        const float fx_new = 800;
        const float fy_new = fx_new;
        const float cx_new = src_width / 4;
        const float cy_new = src_height / 4;

        float xwu = (coords[i].x - cx_new) / fx_new;
        float ywu = (coords[i].y - cy_new) / fy_new;

        float r     = native_sqrt(xwu*xwu + ywu*ywu);
        //float theta = atan2(r, 1);
        float theta = atan2pi(r, 1);

        //float D[4]; // = {0.0, 0.0, 0.0, 0.0};
        //D[0] = D[1] = D[2] = D[3] = 0.0;

        // Perform distortion (forward map)
        //float theta_d = theta*(1 + D[0]*native_powr(theta,2) + D[1]*native_powr(theta,4) +
        //                    D[2]*native_powr(theta,6) + D[3]*native_powr(theta,8));

        float theta_d = theta;


        // now find the world-frame location when distorted
        float xwd = (theta_d/r)*xwu;
        float ywd = (theta_d/r)*ywu;

        // convert the distorted world coordinate to pixel coordinate
        // in the original distorted image
        //float xd = (fx * xwd) + cx;
        //float yd = (fy * ywd) + cy;

        //float xd = (fx_new*1.3 * xwd) + cx_new;
        //float yd = (fy_new*1.3 * ywd) + cy_new;
        float xd = (fx_new*4.0 * xwd) + cx_new;
        float yd = (fy_new*4.0 * ywd) + cy_new;

        coords[i].x = xd;
        coords[i].y = yd;
        */
    }


    //compensate for subtraction of pedestal
    const float ped      = 16.0/256.0;          //IMX412 has pedestal value of 64 for 10bit output, so 16.0 for 8 bit
    const float ped_gain = 1.0 / (1.0 - ped);   //after subtracting the pedestal, this gain will be used to scale the range back to 0 .. 1.0
    
    gain_r *= ped_gain;
    gain_g *= ped_gain;
    gain_b *= ped_gain;
    
    //convert 4 bayer RGGB pixels to 4 RGB pixels and apply white balance gains
    float4 bayer_pixels[4];
    float3 rgb_pixels[4];
    float3 yuv_pixels[4];

    for (int i=0; i<4; i++)
    {
        bayer_pixels[i] = read_imagef(bayer_image, sampler, coords[i]);
        rgb_pixels[i]   = clamp((float3)(gain_r*(bayer_pixels[i].x-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z-ped-ped), gain_b*(bayer_pixels[i].w-ped)), 0.0, 1.0);
        yuv_pixels[i]   = (float3)(RGB2Y(rgb_pixels[i]), RGB2U(rgb_pixels[i]), RGB2V(rgb_pixels[i]));
    }

    const int line_stride     = dst_line_stride;
    const int uv_plane_offset = dst_plane_stride * line_stride;

    //calculate the coordinates of the 4 Y pixels in output image
    int write_coord0 = wid_y*line_stride*2 + wid_x *2;
    //int write_coord1 = wid_y*line_stride*2 + wid_x *2 +1;
    int write_coord2 = (wid_y*2+1)*line_stride + wid_x *2;
    //int write_coord3 = (wid_y*2+1)*line_stride + wid_x *2 +1;

    //calculate the coordinates of the U and V pixels
    int write_coord_u = uv_plane_offset + wid_y*line_stride + wid_x *2;
    //int write_coord_v = write_coord_u + 1;
    
    //calculate U and V values, which should be average of the 4 pixels
    float u_av = (yuv_pixels[0].y + yuv_pixels[1].y + yuv_pixels[2].y + yuv_pixels[3].y)*0.25;
    float v_av = (yuv_pixels[0].z + yuv_pixels[1].z + yuv_pixels[2].z + yuv_pixels[3].z)*0.25;
    
    //clamp and write the data to output image
    //writing uchar2 is a bit faster than individual bytes
    uchar2 yy0 = (uchar2)(clamp(yuv_pixels[0].x * 255.0, 0.0, 255.0),clamp(yuv_pixels[1].x * 255.0, 0.0, 255.0));
    uchar2 yy1 = (uchar2)(clamp(yuv_pixels[2].x * 255.0, 0.0, 255.0),clamp(yuv_pixels[3].x * 255.0, 0.0, 255.0));
    uchar2 uv  = (uchar2)(clamp(u_av * 255.0, 0.0, 255.0), clamp(v_av * 255.0, 0.0, 255.0));

    uchar2 * yy0_ptr = (uchar2 *)&(image_out[write_coord0]);
    uchar2 * yy1_ptr = (uchar2 *)&(image_out[write_coord2]);
    uchar2 * uv_ptr  = (uchar2 *)&(image_out[write_coord_u]);

    *yy0_ptr = yy0;
    *yy1_ptr = yy1;
    *uv_ptr = uv;
}
);
