#include <iostream>
#include <syslog.h>
#include <sys/types.h>
#include <fcntl.h> 
#include <unistd.h>
#include <termios.h>
#include <string.h>
#include <math.h>
#include <array>
#include <cstring>

// MPA
#include <modal_pipe_interfaces.h>

#include "mavlink_to_msp_dp.h"
#include "msp_dp_defines.h"
#include "config_file_msp.h"
#include "MenuManager.h"


std::string MenuManager::run_command(const std::string& cmd) {
    std::array<char, 128> buffer;
    std::string result;
    FILE* pipe = popen(cmd.c_str(), "r");
    if (!pipe) {
        M_ERROR("popen() failed!\n");
        return "";
    }
    while (fgets(buffer.data(), buffer.size(), pipe) != nullptr) {
        result += buffer.data();
    }
    pclose(pipe);
    return result;
}

std::string MenuManager::get_sdk_version(const std::string& packageOutput) {
    std::size_t tabPos = packageOutput.find('\t');
    if (tabPos != std::string::npos) {
        return packageOutput.substr(tabPos + 1);
    }
    return "";
}

bool MenuManager::save_changes_to_file(menu_type_e menu_type){
	int ret{0}, len{0}, temp_disable_transmission{0};
	FILE* fd{nullptr};
	char camera_source_pipe[64]{}, cmd_string[256]{};
	cJSON* item{nullptr}, *parent{nullptr}, *cameras_json{nullptr};
	const char* restart_cmd = "systemctl restart voxl-camera-server";
	switch(menu_type){
		case FC_MENU:

			return true;
		case VTX_MENU:
			parent = open_json(VTX_CONFIG_PATH);
			if(parent==NULL) {
				M_ERROR("Parent JSON is NULL\n");
				return false;
			}
			cJSON_ReplaceItemInObject(parent, "frequency", cJSON_CreateNumber(atoi(frequencies[m_freq_idx])));
			cJSON_ReplaceItemInObject(parent, "power", cJSON_CreateNumber(atoi(power_level[m_pwr_idx])));
			cJSON_ReplaceItemInObject(parent, "fec_percent", cJSON_CreateNumber(atoi(fec_percent[m_fec_idx])));
			cJSON_ReplaceItemInObject(parent, "mcs", cJSON_CreateNumber(atoi(mcs_indices[m_mcs_idx])));
			json_write_to_file(VTX_CONFIG_PATH, parent);
			cJSON_Delete(parent);

			parent = open_json(CONF_FILE);
			if(parent==NULL) {
				M_ERROR("Parent JSON is NULL\n");
				return false;
			}
			cJSON_ReplaceItemInObject(parent, "shortcut", cJSON_CreateString(vtx_shortcut[m_shortcut_idx]));
			m_parameters.shortcut = m_shortcut_idx == OPT_B ? OPT_B : OPT_A;	// handle case where it is neither (should never happen) 
			json_write_to_file(CONF_FILE, parent);
			cJSON_Delete(parent);
			return true;
		case CAMERA_MENU:
			parent = open_json(CAMERA_CONFIG_PATH);
			if(parent==NULL) {
				M_ERROR("Parent JSON is NULL\n");
				return false;
			}
			len = 0;
			cameras_json = json_fetch_array_of_objects(parent, "cameras", &len);
			for(int i=0; i<len; i++){
				item = cJSON_GetArrayItem(cameras_json, i);

				if(item==NULL){
					M_ERROR("failed to fetch item %d from json array\n", i);
					cJSON_Delete(parent);
					return false;
				}

				if(json_fetch_string_with_default(item, "name", camera_source_pipe, 63, "hires")){
					M_ERROR("Reading config file: camera name not specified for %s\n", camera_source_pipe);
				}
				
				if (!strncmp(camera_source_pipe, m_vtx_source_pipe, strlen(camera_source_pipe))){
					// we found the camera VTX is using!
					M_DEBUG("Found matching camera! %s\n", camera_source_pipe);
					break;
				}
			}
			// M_DEBUG("Setting:\n\tWidth: %d\n\tHeight: %d\n\tBitrate: %s\n\tFPS: %s\n", m_camera_width, m_camera_height, bitrates[m_bitrate_idx], fps[m_fps_idx]);
			cJSON_ReplaceItemInObject(item, "small_video_width", cJSON_CreateNumber(m_camera_width));
			cJSON_ReplaceItemInObject(item, "small_video_height", cJSON_CreateNumber(m_camera_height));
			cJSON_ReplaceItemInObject(item, "small_venc_mbps", cJSON_CreateNumber(atof(bitrates[m_bitrate_idx])));
			cJSON_ReplaceItemInObject(item, "fps", cJSON_CreateNumber(atoi(fps[m_fps_idx])));
			json_write_to_file(CAMERA_CONFIG_PATH, parent);
			cJSON_Delete(parent);
			std::system(restart_cmd);
			return true;
		case SHORTCUT_MENU:
			parent = open_json(VTX_CONFIG_PATH);
			if(parent==NULL) {
				M_ERROR("Parent JSON is NULL\n");
				return false;
			}
			cJSON_ReplaceItemInObject(parent, "disable_transmissions", cJSON_CreateFalse());
			json_write_to_file(VTX_CONFIG_PATH, parent);
			cJSON_Delete(parent);
			return true;	 // nothing to save
		case UNKNOWN_MENU: [[fallthrough]]
		default:
			M_ERROR("Cannot determine which menu to save values for.\n");
			break;
	}
	return false;
}

void MenuManager::get_menu_default_values(menu_type_e menu_type){
	float bitrate{0};
	FILE* fd{nullptr};
	char resolution[64]{}, camera_source_pipe[64]{};
	cJSON* item{nullptr}, *parent{nullptr}, *cameras_json{nullptr};
	int frequency{-1}, pwr{-1}, fec{-1}, mcs{-1}, len{0}, width{0}, height{0}, current_fps{0};
	switch(menu_type){
		case FC_MENU:
			m_default_stick_idx = 0;
			return;
		case VTX_MENU:
			parent = open_json(VTX_CONFIG_PATH);
			if(parent==NULL) {
				M_ERROR("Parent JSON is NULL\n");
				return;
			}

			json_fetch_int_with_default(parent, "frequency", &frequency, -1);
			json_fetch_int_with_default(parent, "power", &pwr, -1);
			json_fetch_int_with_default(parent, "fec_percent", &fec, -1);
			json_fetch_int_with_default(parent, "mcs", &mcs, -1);
			for(int freq=0;freq<NUM_FREQ;++freq){
				if(atoi(frequencies[freq]) == frequency){
					m_default_freq_idx = freq;
					break;
				}
			}
			for(int pwr_level=0;pwr_level<NUM_PWR_LEVEL;++pwr_level){
				if(atoi(power_level[pwr_level]) == pwr) {
					m_default_pwr_idx = pwr_level;
					break;
				}
			}
			for(int mcs_index=0;mcs_index<NUM_MCS;++mcs_index){
				if(atoi(mcs_indices[mcs_index]) == mcs){
					m_default_mcs_idx = mcs_index;
					break;
				}
			}
			for(int fec_perc=0;fec_perc<NUM_FEC_PERCENT;++fec_perc){
				if(atoi(fec_percent[fec_perc]) == fec) {
					m_default_fec_idx = fec_perc;
					break;
				}
			}
			m_default_shortcut_idx = m_parameters.shortcut == OPT_B ? OPT_B : OPT_A;
			if(frequency==-1){
				M_ERROR("Failed to read frequency value from config file, defaulting to lowest\n");
				m_default_freq_idx = 0;
			}
			if(pwr==-1){
				M_ERROR("Failed to read power value from config file, defaulting to lowest\n");
				m_default_pwr_idx = 0;
			}
			if(mcs==-1){
				M_ERROR("Failed to read MCS Index value from config file, defaulting to MCS 1\n");
				m_default_fec_idx = 1;
			}
			if(fec==-1){
				M_ERROR("Failed to read FEC percent value from config file, defaulting to lowest\n");
				m_default_fec_idx = 0;
			}
			cJSON_Delete(parent);
			return;
		case CAMERA_MENU:
			// Get VTX input pipe name so we can known which camera config to update below 
			parent = open_json(VTX_CONFIG_PATH);
			if(parent==NULL) {
				M_ERROR("Parent JSON is NULL\n");
				return;
			}
			json_fetch_string_with_default(parent, "source", m_vtx_source_pipe, 63, "hires_small_encoded");
			cJSON_Delete(parent);
			
			// Now get the config data for the camera we are using for VTX 
			parent = open_json(CAMERA_CONFIG_PATH);
			if(parent==NULL) {
				M_ERROR("Parent JSON is NULL\n");
				return;
			}
			len = 0;
			cameras_json = json_fetch_array_of_objects(parent, "cameras", &len);
			for(int i=0; i<len; i++){
				item = cJSON_GetArrayItem(cameras_json, i);

				if(item==NULL){
					M_ERROR("failed to fetch item %d from json array\n", i);
					cJSON_Delete(parent);
					return;
				}

				if(json_fetch_string_with_default(item, "name", camera_source_pipe, 63, "hires")){
					M_ERROR("Reading config file: camera name not specified for %s\n", camera_source_pipe);
				}
				
				if (!strncmp(camera_source_pipe, m_vtx_source_pipe, strlen(camera_source_pipe))){
					// we found the camera VTX is using!
					break;
				}
			}
			json_fetch_int_with_default(item, "fps", &current_fps, -1);
			json_fetch_int_with_default(item, "small_video_width", &width, -1);
			json_fetch_int_with_default(item, "small_video_height", &height, -1);
			json_fetch_float_with_default(item, "small_venc_mbps", &bitrate, -1.0);
			snprintf(resolution, sizeof(resolution), "%dx%d", width, height);
			// M_DEBUG("Got camera config:\n\tResolution: %s\n\tWidth: %d\n\tHeight: %d\n\tBitrate: %.1f\n\tFPS: %d\n", resolution, width, height, bitrate, current_fps);
			for(int res=0;res<NUM_RESOLUTIONS;++res){
				if (!strncmp(resolution, resolutions[res], strlen(resolution))){
					m_default_resolution_idx = res;
					m_camera_height = height;
					m_camera_width = width;
					break;
				}
			}
			for(int mbps=0;mbps<NUM_BITRATE;++mbps){
				if((int)(atof(bitrates[mbps])*10) == (int)(bitrate*10)) {
					m_default_bitrate_idx = mbps;
					break;
				}
			}
			for(int rate=0;rate<NUM_FPS;++rate){
				if(atoi(fps[rate]) == current_fps) {
					m_default_fps_idx = rate;
					break;
				}
			}
			if(bitrate<0.0){
				M_ERROR("Failed to read bitrate value from config file, defaulting to lowest\n");
				m_default_bitrate_idx = 0;
			}
			if(width == -1 || height == -1){
				M_ERROR("Failed to read resolution value from config file, defaulting to 1280x720\n");
				m_default_resolution_idx = NUM_RESOLUTIONS-1;
			}
			if(current_fps==-1){
				M_ERROR("Failed to read fps value from config file, defaulting to 60\n");
				m_default_fps_idx = NUM_FPS-1;
			}
			cJSON_Delete(parent);
			return;
		default:
			return;
	}
}

bool MenuManager::check_for_menu(std::shared_ptr<mavlink_rc_channels_t> rc_msg){
	if(!rc_msg) return false;
	if(open_fc_menu(rc_msg->chan1_raw,rc_msg->chan2_raw,rc_msg->chan4_raw,rc_msg->chan3_raw) && menu_debounce(m_latest_menu_request, m_latest_menu_exit)){
		m_latest_menu_request = std::chrono::steady_clock::now();
		m_fc_menu = true;
		return true;
	}

	if(open_vtx_menu(rc_msg->chan1_raw,rc_msg->chan2_raw,rc_msg->chan4_raw,rc_msg->chan3_raw) && menu_debounce(m_latest_menu_request, m_latest_menu_exit)){
		m_latest_menu_request = std::chrono::steady_clock::now();
		m_vtx_menu = true;
		return true;
	}

	if(open_camera_menu(rc_msg->chan1_raw,rc_msg->chan2_raw,rc_msg->chan4_raw,rc_msg->chan3_raw) && menu_debounce(m_latest_menu_request, m_latest_menu_exit)){
		m_latest_menu_request = std::chrono::steady_clock::now()+std::chrono::milliseconds(500);	// Don't wanna start scrolling through menu items right away on accident
		m_latest_menu_select = std::chrono::steady_clock::now();
		m_camera_menu = true;
		return true;
	}

	if(enter_shortcut(rc_msg->chan1_raw, rc_msg->chan2_raw, rc_msg->chan4_raw, rc_msg->chan3_raw, m_parameters.shortcut) && menu_debounce(m_latest_menu_request, m_latest_menu_exit)){
		m_latest_menu_request = std::chrono::steady_clock::now();	
		m_latest_menu_select = std::chrono::steady_clock::now();
		m_shortcut_menu = true;
		cJSON* parent = open_json(VTX_CONFIG_PATH);
		if(parent==NULL) {
			M_ERROR("Parent JSON is NULL\n");
			return false;
		}
		cJSON_ReplaceItemInObject(parent, "disable_transmissions", cJSON_CreateTrue());
		json_write_to_file(VTX_CONFIG_PATH, parent);
		cJSON_Delete(parent);
		return true;
	}
	return false;
}

void MenuManager::handle_fc_menu_action(bool new_menu){
	// Do nothing if no action on an already created menu
	if(!new_menu && stick_is_middle(m_sticks.yaw))return;
	if(!m_menu_values) m_menu_values = (char**)FC_MENU_DEFAULTS;
	if(new_menu) {
		m_menu_values[FC_STICK_ASSIGNMENTS] = (char*)stick_layouts[m_default_stick_idx];
		m_stick_idx = m_default_stick_idx;
		m_latest_menu_select = std::chrono::steady_clock::now();
		return;
	}
	switch(m_menu_row){
		case FC_STICK_ASSIGNMENTS:
			if(stick_is_high(m_sticks.yaw)){
				m_stick_idx = m_stick_idx+1 >= NUM_STICK_LAYOUTS ? 0 : m_stick_idx+1;
				m_menu_values[FC_STICK_ASSIGNMENTS] = (char*)stick_layouts[m_stick_idx];
			} else if(stick_is_low(m_sticks.yaw)){
				m_stick_idx = m_stick_idx-1 < 0 ? NUM_STICK_LAYOUTS-1 : m_stick_idx-1;
				m_menu_values[FC_STICK_ASSIGNMENTS] = (char*)stick_layouts[m_stick_idx];
			}
			m_latest_menu_select = std::chrono::steady_clock::now();
			break;
		default:
			break;
	}
}

void MenuManager::handle_vtx_menu_action(bool new_menu){
	// Do nothing if no action on an already created menu
	if(!new_menu && stick_is_middle(m_sticks.yaw))return;
	if(!m_menu_values) m_menu_values = (char**)VTX_MENU_DEFAULTS;
	if(new_menu) {
		m_menu_values[VTX_FREQ]  = (char*)frequencies[m_default_freq_idx];
		m_menu_values[VTX_POWER] = (char*)power_level[m_default_pwr_idx];
		m_menu_values[VTX_MODE]  = (char*)vtx_mode[m_default_mode_idx];
		m_menu_values[VTX_MCS]   = (char*)mcs_indices[m_default_mcs_idx];
		m_menu_values[VTX_FEC]   = (char*)fec_percent[m_default_fec_idx];
		m_menu_values[VTX_SHORTCUTS]   = (char*)vtx_shortcut[m_default_shortcut_idx];
		m_freq_idx = m_default_freq_idx;
		m_pwr_idx  = m_default_pwr_idx;
		m_mode_idx = m_default_mode_idx;
		m_mcs_idx  = m_default_mcs_idx;
		m_fec_idx  = m_default_fec_idx;
		m_shortcut_idx  = m_default_shortcut_idx;
		m_latest_menu_select = std::chrono::steady_clock::now();
		return;
	}
    switch(m_menu_row){
		case VTX_FREQ:
			if(stick_is_high(m_sticks.yaw)){
				m_freq_idx = m_freq_idx+1 >= NUM_FREQ ? 0 : m_freq_idx+1;
				m_menu_values[VTX_FREQ] = (char*)frequencies[m_freq_idx];
			} else if(stick_is_low(m_sticks.yaw)) {
				m_freq_idx = m_freq_idx-1 < 0 ? NUM_FREQ-1 : m_freq_idx-1;
				m_menu_values[VTX_FREQ] = (char*)frequencies[m_freq_idx];
			}
			m_latest_menu_select = std::chrono::steady_clock::now();
			break;
		case VTX_POWER: 
			if(stick_is_high(m_sticks.yaw)){
				m_pwr_idx = m_pwr_idx+1 >= NUM_PWR_LEVEL ? 0 : m_pwr_idx+1;
				m_menu_values[VTX_POWER] = (char*)power_level[m_pwr_idx];
			} else if(stick_is_low(m_sticks.yaw)) {
				m_pwr_idx = m_pwr_idx-1 < 0 ? NUM_PWR_LEVEL-1 : m_pwr_idx-1;
				m_menu_values[VTX_POWER] = (char*)power_level[m_pwr_idx];
			}
			m_latest_menu_select = std::chrono::steady_clock::now();
			break;
		case VTX_MODE: 
			if(stick_is_high(m_sticks.yaw)) {
				m_mode_idx = m_mode_idx+1 >= NUM_MODES ? 0 : m_mode_idx+1;
				m_menu_values[VTX_MODE] = (char*)vtx_mode[m_mode_idx];
			} else if(stick_is_low(m_sticks.yaw)){
				m_mode_idx = m_mode_idx-1 < 0 ? NUM_MODES-1 : m_mode_idx-1;
				m_menu_values[VTX_MODE] = (char*)vtx_mode[m_mode_idx];
			}
			m_latest_menu_select = std::chrono::steady_clock::now();
			break;
		case VTX_MCS: 
			if(stick_is_high(m_sticks.yaw)) {
				m_mcs_idx = m_mcs_idx+1 >= NUM_MCS ? 0 : m_mcs_idx+1;
				m_menu_values[VTX_MCS] = (char*)mcs_indices[m_mcs_idx];
			} else if(stick_is_low(m_sticks.yaw)){
				m_mcs_idx = m_mcs_idx-1 < 0 ? NUM_MCS-1 : m_mcs_idx-1;
				m_menu_values[VTX_MCS] = (char*)mcs_indices[m_mcs_idx];
			}
			m_latest_menu_select = std::chrono::steady_clock::now();
			break;
		case VTX_FEC: 
			if(stick_is_high(m_sticks.yaw)) {
				m_fec_idx = m_fec_idx+1 >= NUM_FEC_PERCENT ? 0 : m_fec_idx+1;
				m_menu_values[VTX_FEC] = (char*)fec_percent[m_fec_idx];
			} else if(stick_is_low(m_sticks.yaw)){
				m_fec_idx = m_fec_idx-1 < 0 ? NUM_FEC_PERCENT-1 : m_fec_idx-1;
				m_menu_values[VTX_FEC] = (char*)fec_percent[m_fec_idx];
			}
			m_latest_menu_select = std::chrono::steady_clock::now();
			break;
		case VTX_SHORTCUTS: 
			if(stick_is_high(m_sticks.yaw)) {
				m_shortcut_idx = m_shortcut_idx+1 >= NUM_SHORTCUTS ? 0 : m_shortcut_idx+1;
				m_menu_values[VTX_SHORTCUTS] = (char*)vtx_shortcut[m_shortcut_idx];
			} else if(stick_is_low(m_sticks.yaw)){
				m_shortcut_idx = m_shortcut_idx-1 < 0 ? NUM_SHORTCUTS-1 : m_shortcut_idx-1;
				m_menu_values[VTX_SHORTCUTS] = (char*)vtx_shortcut[m_shortcut_idx];
			}
			m_latest_menu_select = std::chrono::steady_clock::now();
			break;
		default:
			break;
    }
}

void MenuManager::handle_camera_menu_action(bool new_menu){
	// Do nothing if no action on an already created menu
	if(!new_menu && stick_is_middle(m_sticks.yaw))return;
	if(!m_menu_values) m_menu_values = (char**)CAMERA_MENU_DEFAULTS;
	if(new_menu) {
		m_menu_values[CAMERA_RESOLUTION] = (char*)resolutions[m_default_resolution_idx];
		m_menu_values[CAMERA_BITRATE] = (char*)bitrates[m_default_bitrate_idx];
		m_menu_values[CAMERA_FPS] = (char*)fps[m_default_fps_idx];
		m_resolution_idx = m_default_resolution_idx;
		m_bitrate_idx = m_default_bitrate_idx;
		m_fps_idx = m_default_fps_idx;
		m_latest_menu_select = std::chrono::steady_clock::now();
		return;
	}
    switch(m_menu_row){
		case CAMERA_RESOLUTION:
			if(stick_is_high(m_sticks.yaw)){
				m_resolution_idx = m_resolution_idx+1 >= NUM_RESOLUTIONS ? 0 : m_resolution_idx+1;
				m_menu_values[CAMERA_RESOLUTION] = (char*)resolutions[m_resolution_idx];
			} else if(stick_is_low(m_sticks.yaw)){
				m_resolution_idx = m_resolution_idx-1 < 0 ? NUM_RESOLUTIONS-1 : m_resolution_idx-1;
				m_menu_values[CAMERA_RESOLUTION] = (char*)resolutions[m_resolution_idx];
			}
			m_camera_height = atoi(resolution_height[m_resolution_idx]);
			m_camera_width  = atoi(resolution_width[m_resolution_idx]);
			m_latest_menu_select = std::chrono::steady_clock::now();
			break;
		case CAMERA_BITRATE:
			if(stick_is_high(m_sticks.yaw)){
				m_bitrate_idx = m_bitrate_idx+1 >= NUM_BITRATE ? 0 : m_bitrate_idx+1;
				m_menu_values[CAMERA_BITRATE] = (char*)bitrates[m_bitrate_idx];
			} else if(stick_is_low(m_sticks.yaw)) {
				m_bitrate_idx = m_bitrate_idx-1 < 0 ? NUM_BITRATE-1 : m_bitrate_idx-1;
				m_menu_values[CAMERA_BITRATE] = (char*)bitrates[m_bitrate_idx];
			}
			m_latest_menu_select = std::chrono::steady_clock::now();
			break;
    	case CAMERA_FPS: 
			if(stick_is_high(m_sticks.yaw)){
				m_fps_idx = m_fps_idx+1 >= NUM_FPS ? 0 : m_fps_idx+1;
				m_menu_values[CAMERA_FPS] = (char*)fps[m_fps_idx];
			} else if(stick_is_low(m_sticks.yaw)) {
				m_fps_idx = m_fps_idx-1 < 0 ? NUM_FPS-1 : m_fps_idx-1;
				m_menu_values[CAMERA_FPS] = (char*)fps[m_fps_idx];
			}
			m_latest_menu_select = std::chrono::steady_clock::now();
			break;
		default:
			break;
    }
}

void MenuManager::handle_menu(msp_dp_rc_sticks_t sticks, bool new_menu){
	// Check which menu is active
	if(new_menu){
		active_menu = get_active_menu_type(); 
		// Get current values for menu items based on config file 
		if (active_menu != SHORTCUT_MENU) get_menu_default_values(active_menu);
	}
	// Store current RC stick values and other meta data for use later 
	m_sticks = sticks;
	int n_rows = get_menu_size(active_menu);
	std::chrono::steady_clock::time_point latest_menu_request{get_menu_req_ts()};

	// 0mW mode 
	if(m_shortcut_menu){
		// Check if we want to exit 0mW mode
		if(stick_debounce(latest_menu_request) && exit_shortcut(m_sticks.roll, m_sticks.pitch, m_sticks.yaw, m_sticks.throttle, m_parameters.shortcut)){
			save_changes_to_file(active_menu);
			set_menu_exit_time();
			m_latest_menu_request = std::chrono::steady_clock::now()+std::chrono::seconds(1);
		}
		return;	// In 0mW mode don't do anything beyond enter and exit 
	} 

	// We want to exit and/or save changes
	if(stick_is_high(m_sticks.yaw) && stick_debounce(latest_menu_request) && exit_menu(m_sticks, m_menu_row, active_menu)){
		M_DEBUG("Exiting %s Menu\n", menu_type_as_string(active_menu));
		if(save_changes(m_sticks.yaw, m_menu_row, active_menu)){
			M_DEBUG("Saving changes in %s Menu\n", menu_type_as_string(active_menu));
			save_changes_to_file(active_menu);
		}
		set_menu_exit_time();
		return;
	}

	// We want to navigate the menu, determine which one and grab the menu items
	const char** menu_items = get_menu_items(active_menu);

	// Determine where we are in the menu
	if(new_menu){
		m_menu_row = 0;
		m_menu_values = nullptr;
	} else if (stick_is_high(m_sticks.throttle) && menu_debounce(m_latest_menu_scroll, latest_menu_request)){
		m_menu_row--;
		if(m_menu_row<0) m_menu_row=n_rows-1;	
		m_latest_menu_scroll = std::chrono::steady_clock::now();
	} else if (stick_is_low(m_sticks.throttle) && menu_debounce(m_latest_menu_scroll, latest_menu_request)){
		m_menu_row++;
		if(m_menu_row>n_rows-1) m_menu_row=0;	
		m_latest_menu_scroll = std::chrono::steady_clock::now();
	}

	// Get value of menu fields (current freq, power, resolution, bitrate, etc)
	if(menu_debounce(m_latest_menu_select, latest_menu_request) || !m_menu_values){
		switch(get_active_menu_type()){
			case FC_MENU:
				handle_fc_menu_action(new_menu);
				break;
			case VTX_MENU:
				handle_vtx_menu_action(new_menu);
				break;
			case CAMERA_MENU:
				handle_camera_menu_action(new_menu);
				break;
			case UNKNOWN_MENU: [[fallthrough]]
			default:
				M_ERROR("ACTIVE MENU TYPE UNKNOWN!\n");
		}
	}

	// Empty menu field values... should never happen!!
	if(!m_menu_values){
		M_ERROR("ERROR - Menu items is empty!! MENUS: FC: %d\tVTX: %d\tCAMERA: %d\n", m_fc_menu, m_vtx_menu, m_camera_menu);
		return;
	}

	// Code below will be replaced by UART or MPA writes instead of printing to the terminal
	// Send String messages that will represent the menu 
	// M_DEBUG("------ %s MENU ------\n", menu_type_as_string(active_menu));
	for(int menu_item=0;menu_item<n_rows;++menu_item){
		if(menu_item == m_menu_row){
			char current_menu_item[MSP_OSD_MAX_STRING_LENGTH];
			snprintf(current_menu_item, sizeof(current_menu_item),"%c%s", '>', menu_items[menu_item]);
			// if(menu_item < n_rows-2) M_DEBUG("%s: %s\n",current_menu_item, m_menu_values[menu_item]);
			// else M_DEBUG("%s\n",current_menu_item);
		} else if (menu_item < n_rows-2){
			// M_DEBUG(" %s: %s\n",menu_items[menu_item], m_menu_values[menu_item]);
		} else {
			// M_DEBUG(" %s\n",menu_items[menu_item]);
		}
	}
	if(active_menu == FC_MENU){
		std::string version = get_sdk_version(run_command("dpkg-query -W voxl-suite"));
		// Output the version
		if (version.empty()) version = "???";
		// M_DEBUG("---------------------\n");
		// M_DEBUG("SDK: %s", version.c_str());
	}
	// M_DEBUG("\n");
}
