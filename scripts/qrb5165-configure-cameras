#!/bin/bash
################################################################################
# Copyright 2023 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

set -e

# options set by arguments
CAM_CONFIG_ID="-1"
ENABLE_CAMERA_SERVER=1
CUSTOM_CONFIG_FILE_PATH="/data/modalai/custom_camera_config.txt"
CAM_LIB_DIR="/usr/lib/camera/"
PLATFORM_CONFIG=$(cat /sys/module/voxl_platform_mod/parameters/config)
CONFIG_DIR="/usr/share/modalai/voxl-camera-server/standard_configs/platform_config_$PLATFORM_CONFIG/"
CONFIG_FILE="/etc/modalai/voxl-camera-server.conf"
DRYRUN=false

. /home/root/.profile.d/modal_bash_formatting.sh


RESET_ALL="\e[0m"
RED="\e[91m"
YLW="\e[33m"
GRN="\e[32m"
SET_BOLD="\e[1m"

PRINT_ERROR (){
	echo -e "${RED}[ERROR] $@${RESET_ALL}"
}

PRINT_GREEEN_LINE (){
	echo -e "${GRN}${SET_BOLD}------------------------------------------------------------------${RESET_ALL}"
}


print_usage () {
	echo ""
	echo "This tool moves the correct camera driver binaries into place"
	echo "and creates a new voxl-camera-server.conf config file with defaults"
	echo "for all cameras in the desired config"
	echo ""
	echo "Usage:"
	echo "voxl-configure-cameras <id>          - set up a pre-defined config"
	echo "voxl-configure-cameras custom        - read from custom config file"
	echo "voxl-configure-cameras <id> disable  - configure but leave disabled"
	echo "voxl-configure-cameras help          - show this help text"
	echo ""
	echo "Available camera config IDs are as follows:"
	echo ""
	qrb5165-print-camera-configs
	echo -e " C - use user-defined custom camera config in"
	echo -e "     (/data/modalai/custom_camera_config.txt)"
	echo ""
	echo "If using a custom camera config file, make a new one with:"
	echo "cp /usr/share/modalai/voxl-camera-server/custom_camera_config.txt /data/modalai/"
	echo "Instructions can be found in that file."
	echo ""
	exit 0
}

## set most parameters which don't have quotes in json
set_param () {
	if [ "$#" != "2" ]; then
		echo "set_param expected 2 args"
		exit 1
	fi

	# remove quotes if they exist
	var=$1
	var="${var%\"}"
	var="${var#\"}"
	val=$2
	val="${val%\"}"
	val="${val#\"}"

	sed -E -i "/\"$var\":/c\	\"$var\":	$val," ${CONFIG_FILE}
}

## set string parameters which need quotes in json
set_param_string () {
	if [ "$#" != "2" ]; then
		echo "set_param_string expected 2 args"
		exit 1
	fi
	var=$1
	var="${var%\"}"
	var="${var#\"}"
	sed -E -i "/\"$var\":/c\	\"$var\":	\"$2\"," ${CONFIG_FILE}
}

parse_opts(){

	while [[ $# -gt 0 ]]; do

		## convert argument to lower case for robustness
		arg=$(echo "$1" | tr '[:upper:]' '[:lower:]')

		## parse arguments
		case ${arg} in
			"h"|"-h"|"help"|"--help")
				print_usage
				exit 0
				;;
			"disable")
				if [[ "$CAM_CONFIG_ID" == "-1" ]]; then
					CAM_CONFIG_ID=0
				fi
				ENABLE_CAMERA_SERVER=0
				;;
			"sentinel_v1") ## DEPRECATED OPTION
				CAM_CONFIG_ID=11
				ENABLE_CAMERA_SERVER=1
				;;
			"starling_v2") ## DEPRECATED OPTION
				CAM_CONFIG_ID=6
				ENABLE_CAMERA_SERVER=1
				;;
			"fpv_revB") ## DEPRECATED OPTION
				CAM_CONFIG_ID=15
				ENABLE_CAMERA_SERVER=1
				;;
			"--custom"|"custom"|"Custom"|"c"|"C")
				CAM_CONFIG_ID="custom"
				;;
			"--dry"|"--dry-run"|"dry"|"dry-run"|"dryrun")
				DRYRUN=true
				;;
			*)
				## all other arguments are either config id numbers or invalid
				if [[ "$arg" =~ ^[0-9]+$ ]]; then
					CAM_CONFIG_ID=$arg
				else
					echo "invalid option $arg"
					print_usage
					exit 1
				fi
		esac
		shift
	done
}


add_custom_sensor(){
	port=$1
	connector=$2
	sensor=$3
	name=$4
	combo_mode=$5
	name2=$6
	rotate=$7

	if $DRYRUN; then
		echo ""
		echo port:       ${port}
		echo connector:  ${connector}
		echo sensor:     ${sensor}
		echo name:       ${name}
		echo combo_mode: ${combo_mode}
		echo name2:      ${name2}
		echo rotate:     ${rotate}
	fi

	## empty slot, nothing to do
	if [[ $sensor == "" ]] || [[ $sensor == "none" ]]; then
		return
	fi

	## Sanity checks
	if [[ "$name" == "" ]]; then
		echo "ERROR parsing custom cam file"
		echo "empty name for $sensor on port $1"
		exit 1
	fi

	## pick the right driver to copy over
	case $sensor in

	"pmd-tof")
		BIN_LIST+=("/usr/share/modalai/chi-cdk/irs1645/com.qti.sensormodule.irs1645_${port}.bin")
		;;
	"pmd-tof-liow2")
		BIN_LIST+=("/usr/share/modalai/chi-cdk/irs2975c/com.qti.sensormodule.irs2975c_${port}.bin")
		;;
	"ov7251")
		BIN_LIST+=("/usr/share/modalai/chi-cdk/ov7251/com.qti.sensormodule.ov7251_${port}.bin")
		;;
	"ov7251-combo")
		if [ $port == "0" ]; then
			BIN_LIST+=("/usr/share/modalai/chi-cdk/ov7251-combo/com.qti.sensormodule.ov7251_combo_0.bin")
			BIN_LIST+=("/usr/share/modalai/chi-cdk/ov7251-combo/com.qti.sensormodule.ov7251_combo_1.bin")
		elif [ $port == "4" ]; then
			BIN_LIST+=("/usr/share/modalai/chi-cdk/ov7251-combo/com.qti.sensormodule.ov7251_combo_4.bin")
			BIN_LIST+=("/usr/share/modalai/chi-cdk/ov7251-combo/com.qti.sensormodule.ov7251_combo_5.bin")
		else
			echo "ERROR combo ov7251 only works on J6 lower and J8 lower"
			exit 1
		fi
		;;
	"ov9782")
		BIN_LIST+=("/usr/share/modalai/chi-cdk/ov9782/com.qti.sensormodule.ov9782_${port}.bin")
		;;
	"ov9782-combo")
		if [ $port == "0" ]; then
			BIN_LIST+=("/usr/share/modalai/chi-cdk/ov9782-combo/com.qti.sensormodule.ov9782_combo_0.bin")
			BIN_LIST+=("/usr/share/modalai/chi-cdk/ov9782-combo/com.qti.sensormodule.ov9782_combo_1.bin")
		elif [ $port == "4" ]; then
			BIN_LIST+=("/usr/share/modalai/chi-cdk/ov9782-combo/com.qti.sensormodule.ov9782_combo_4.bin")
			BIN_LIST+=("/usr/share/modalai/chi-cdk/ov9782-combo/com.qti.sensormodule.ov9782_combo_5.bin")
		else
			echo "ERROR combo ov9782 only works on J6 lower and J8 lower"
			exit 1
		fi
		;;

	"ar0144")
		BIN_LIST+=("/usr/share/modalai/chi-cdk/ar0144/com.qti.sensormodule.ar0144_${port}.bin")
		;;

	"ar0144-fsin")
		# at time of writing only port 2 is available, usually we used these sensors in combo mode instead
		BIN_LIST+=("/usr/share/modalai/chi-cdk/ar0144-fsin/com.qti.sensormodule.ar0144_fsin_${port}.bin")
		sensor="ar0144"
		;;

	"ar0144-fsin-combo")
		BIN_LIST+=("/usr/share/modalai/chi-cdk/ar0144-combo/com.qti.sensormodule.ar0144_combo_${port}.bin")
		sensor="ar0144"
		;;

	"ar0144-12bit")
		BIN_LIST+=("/usr/share/modalai/chi-cdk/ar0144-12bit/com.qti.sensormodule.ar0144_12bit_${port}.bin")
		;;

	"ar0144-12bit-fsin")
		# at time of writing only port 2 is available, usually we used these sensors in combo mode instead
		BIN_LIST+=("/usr/share/modalai/chi-cdk/ar0144-12bit-fsin/com.qti.sensormodule.ar0144_12bit_fsin_${port}.bin")
		sensor="ar0144-12bit"
		;;

	"ar0144-12bit-fsin-combo")
		BIN_LIST+=("/usr/share/modalai/chi-cdk/ar0144-12bit-combo/com.qti.sensormodule.ar0144_12bit_combo_${port}.bin")
		sensor="ar0144-12bit"
		;;


	## IMX sensors use a different driver to rotate since they go through the ISP
	"imx214")
		if $rotate; then
			BIN_LIST+=("/usr/share/modalai/chi-cdk/imx214-flip/com.qti.sensormodule.imx214_flip_${port}.bin")
		else
			BIN_LIST+=("/usr/share/modalai/chi-cdk/imx214/com.qti.sensormodule.imx214_${port}.bin")
		fi
		rotate=false # turn off the flag
		;;

	"imx412")
		if $rotate; then
			BIN_LIST+=("/usr/share/modalai/chi-cdk/imx412-flip/com.qti.sensormodule.imx412_flip_${port}.bin")
		else
			BIN_LIST+=("/usr/share/modalai/chi-cdk/imx412/com.qti.sensormodule.imx412_${port}.bin")
		fi
		rotate=false # turn off the flag
		;;

	"imx412-fpv")
		BIN_LIST+=("/usr/share/modalai/chi-cdk/imx412-fpv/com.qti.sensormodule.imx412_fpv_${port}.bin")
		if $rotate; then
			echo "ERROR parsing custom cam file"
			echo "no flip driver for imx412-fpv yet (coming soon!!)"
			exit 1
		fi
		sensor="imx412" ## remove fpx suffix before passing config to camera-server-config-helper
		rotate=false # turn off the flag
		;;

	"imx412-fpv-misp")
		BIN_LIST+=("/usr/share/modalai/chi-cdk/imx412-fpv-misp/com.qti.sensormodule.imx412_fpv_misp_${port}.bin")
		if $rotate; then
			echo "ERROR parsing custom cam file"
			echo "no flip driver for imx412-fpv-misp yet (coming soon!!)"
			exit 1
		fi
		sensor="imx412-fpv-misp" 
		rotate=false # turn off the flag
		;;

	"imx664" | "imx664-fpv")
		BIN_LIST+=("/usr/share/modalai/chi-cdk/imx664/com.qti.sensormodule.imx664_${port}.bin")
		;;

	"imx678")
		if $rotate; then
			BIN_LIST+=("/usr/share/modalai/chi-cdk/imx678-flip/com.qti.sensormodule.imx678_flip_${port}.bin")
		else
			BIN_LIST+=("/usr/share/modalai/chi-cdk/imx678/com.qti.sensormodule.imx678_${port}.bin")
		fi
		rotate=false # turn off the flag
		;;
	"boson")
		BIN_LIST+=("/usr/share/modalai/chi-cdk/boson/com.qti.sensormodule.boson_${port}.bin")
		;;
	*)
		echo "ERROR, unknown sensor $sensor"
		exit 1
	esac

	# check the driver file exists
	if [ ! -f ${BIN_LIST[${#BIN_LIST[@]}-1]} ]; then
		echo "ERROR parsing custom cam file"
		echo "no driver for sensor $sensor on connector $connector (port ${port})"
		exit 1
	fi


	## now construct the token string to send to camera server config helper
	OPTIONS=""
	case $rotate in
		"true"|"both")
			OPTIONS=":rotate"
			;;
		"first-only"|"left-only"|"first_only"|"left_only")
			OPTIONS=":rotate-first-only"
			;;
		"second-only"|"right-only"|"second_only"|"right_only")
			OPTIONS=":rotate-second-only"
			;;
		"false"|"")
			;;
		*)
			echo ""
			echo "ROTATE option must be one of \"true\" \"first-only\" \"second-only\" or \"false\""
			exit 1
	esac

	# very simple for mono sensors
	if ! [[ "$sensor" == *"combo"* ]]; then
		CAM_LIST+=("${name}:${sensor}:${ID_COUNTER}${OPTIONS}:")
		ID_COUNTER=$((ID_COUNTER+1))

	# combo sensors are a bit more fussy to handle the stereo configuration
	else
		prefix=${sensor%-*}
		prefix=${prefix%-*}
		prefix=${prefix%-*}
		case $combo_mode in
		"left-right")
			CAM_LIST+=("${name}:${prefix}:${ID_COUNTER}:$((ID_COUNTER+1))${OPTIONS}:")
			ID_COUNTER=$((ID_COUNTER+2))
			;;
		"right-left")
			CAM_LIST+=("${name}:${prefix}:$((ID_COUNTER+1)):${ID_COUNTER}${OPTIONS}:")
			ID_COUNTER=$((ID_COUNTER+2))
			;;
		"independent")
			if [[ "$name2" == "" ]]; then
				echo "ERROR parsing custom cam file"
				echo "missing NAME2 field for independent combo mode"
				exit 1
			fi
			if [[ "$name2" == "$name" ]]; then
				echo "ERROR parsing custom cam file"
				echo "NAME and NAME2 fields must be unique"
				exit 1
			fi
			CAM_LIST+=("${name}:${prefix}:${ID_COUNTER}${OPTIONS}:")
			CAM_LIST+=("${name2}:${prefix}:$((ID_COUNTER+1))${OPTIONS}:")
			ID_COUNTER=$((ID_COUNTER+2))
			;;
		"single")
			CAM_LIST+=("${name}:${prefix}:${ID_COUNTER}${OPTIONS}:")
			ID_COUNTER=$((ID_COUNTER+1))
			;;
		"")
			echo "ERROR parsing custom cam file"
			echo "combo mode sensor requires COMBO_MODE field"
			echo "e.g. J6_LOWER_COMBO_MODE=\"left-right\""
			exit 1
			;;
		*)
			echo "ERROR parsing custom cam file"
			echo "COMBO_MODE must be one of \"left-right\" \"right-left\" \"independent\" or \"single\""
			exit 1
		esac
	fi

}


parse_custom_file(){
	if ! [ -f $CUSTOM_CONFIG_FILE_PATH ]; then
		echo "WARNING, user requested to load their custom camera configuration file"
		echo "but $CUSTOM_CONFIG_FILE_PATH is missing"
		echo "we are making a new empty one now, please populate it with your config"
		echo "and try running voxl-configure-cameras C again"
		echo "vi /usr/share/modalai/voxl-camera-server/custom_camera_config.txt"
		cp /usr/share/modalai/voxl-camera-server/custom_camera_config.txt /data/modalai/
		exit 0
	fi
	parse_file "$CUSTOM_CONFIG_FILE_PATH"

	if [ $ID_COUNTER == "0" ]; then
		echo ""
		echo "WARNING, no cameras specified in file, please populate it with"
		echo "your config and try running voxl-configure-cameras C again."
		echo ""
		echo "vi /data/modalai/custom_camera_config.txt"
		echo ""
		echo "disabling camera server for now"
		rm -rf /usr/lib/camera/com.qti.sensormodule*
		rm -f /etc/modalai/voxl-camera-server
		systemctl disable voxl-camera-server
		## exit 0 here so voxl-configure-mpa doesn't report a failure when setting
		## up a board for the first time when a user hasn't had a chance to set up
		## their custom config file yet
		exit 0
	fi
}

parse_file(){

	if ! [ -f "$1" ]; then
		echo "ERROR, user requested to load camera config file $1"
		echo "but it is missing"
		exit -1
	fi

	source "$1"

	add_custom_sensor "0" "J6 lower" "$J6_LOWER_SENSOR" "$J6_LOWER_NAME" "$J6_LOWER_COMBO_MODE" "$J6_LOWER_NAME2" "$J6_LOWER_ROTATE"
	add_custom_sensor "1" "J6 upper" "$J6_UPPER_SENSOR" "$J6_UPPER_NAME" "$J6_UPPER_COMBO_MODE" "$J6_UPPER_NAME2" "$J6_UPPER_ROTATE"
	add_custom_sensor "2" "J7 lower" "$J7_LOWER_SENSOR" "$J7_LOWER_NAME" "$J7_LOWER_COMBO_MODE" "$J7_LOWER_NAME2" "$J7_LOWER_ROTATE"
	add_custom_sensor "3" "J7 upper" "$J7_UPPER_SENSOR" "$J7_UPPER_NAME" "$J7_UPPER_COMBO_MODE" "$J7_UPPER_NAME2" "$J7_UPPER_ROTATE"
	add_custom_sensor "4" "J8 lower" "$J8_LOWER_SENSOR" "$J8_LOWER_NAME" "$J8_LOWER_COMBO_MODE" "$J8_LOWER_NAME2" "$J8_LOWER_ROTATE"
	add_custom_sensor "5" "J8 upper" "$J8_UPPER_SENSOR" "$J8_UPPER_NAME" "$J8_UPPER_COMBO_MODE" "$J8_UPPER_NAME2" "$J8_UPPER_ROTATE"
	add_custom_sensor "6" "J6 lower combo" "$J6_LOWER_COMBO1_SENSOR" "$J6_LOWER_COMBO1_NAME" "" "" "$J6_LOWER_COMBO1_ROTATE"


}


################################################################################
## actual start of execution
################################################################################

parse_opts $@

if [ $# -eq 0 ]; then
	## no options given, ask the user
	echo ""
	echo "Available camera config IDs are as follows:"
	echo ""
	qrb5165-print-camera-configs
	echo -e " C - use user-defined custom camera config in"
	echo -e "     (/data/modalai/custom_camera_config.txt)"
	echo -e " q - Quit The Wizard"
fi

if [[ "$CAM_CONFIG_ID" == "-1" ]]; then
	# read and validate camera config
	CAM_VALID=false
	while ! $CAM_VALID; do

		echo ""
		read -p "selection: " CAM_CONFIG_ID

		if [[ $CAM_CONFIG_ID == "c" ]] || [[ $CAM_CONFIG_ID == "C" ]] || [[ $CAM_CONFIG_ID == "custom" ]]; then
			echo "Selecting custom camera config"
			CAM_CONFIG_ID="custom"
			CAM_VALID=true
		elif [[ $CAM_CONFIG_ID =~ ^[0-9]+$ ]]; then
			echo "Selected numerical camera config"
			CAM_VALID=true
		elif [[ $CAM_CONFIG_ID == "q" ]] || [[ $CAM_CONFIG_ID == "Q" ]]; then
			echo "quitting"
			exit 0;
		else
			echo "invalid entry, please try again"
		fi
	done
fi

################################################################################
## now actually configure everything based on the variables set above
################################################################################
echo "Camera Configuration: $CAM_CONFIG_ID"


if [ $CAM_CONFIG_ID == "0" ]; then
	echo "stopping and disabling voxl-camera-server"
	systemctl disable voxl-camera-server
	systemctl stop voxl-camera-server
	exit 0
fi

if [ ! -f /usr/share/modalai/chi-cdk/ov7251/com.qti.sensormodule.ov7251_2.bin ]; then
	PRINT_ERROR "Your system image is too old to support the new camera driver structure"
	PRINT_ERROR "Please update to a system image newer than May 14 2023"
	exit 1
fi

CAM_LIST=()
BIN_LIST=()
ID_COUNTER=0


## load custom file or a numbered file from the config directory
if [ $CAM_CONFIG_ID == "custom" ]; then
	parse_custom_file
else
	NUM=$(printf "%02d" "$CAM_CONFIG_ID")
	N_FILES=$(find "$CONFIG_DIR" -maxdepth 1 -type f -name "${NUM}*" | wc -l)
	if [ $N_FILES == "0" ]; then
		echo "invalid option"
		echo "Please provide a valid camera configuration-id number"
		echo ""
		echo "available camera configurations are as follows:"
		qrb5165-print-camera-configs
		exit -1
	elif [ $N_FILES == "1" ]; then
		FILE=$(ls $CONFIG_DIR$NUM*)
	else
		echo "ERROR found multiple configurations with ID number $NUM"
		exit -1
	fi

	parse_file "$FILE"
fi



## print what we came up with
echo ""
echo "camera server config helper list:"
for f in ${CAM_LIST[@]}; do
	echo $f
done
echo ""
echo "driver binary list:"
	for f in ${BIN_LIST[@]}; do
	echo $f
done


## quit now in dryrun mode
if $DRYRUN; then

	echo ""
	echo "exiting dry run without doing anything"
	exit 0
fi


echo ""
echo "calling camera-server-config-helper ${CAM_LIST[@]}"
camera-server-config-helper ${CAM_LIST[@]}


# gpio trigger stuff
if [ ! "$FSYNC_GPIO" == "" ]; then
	echo "setting gpio pin $FSYNC_GPIO for fsync"
	set_param fsync_gpio "$FSYNC_GPIO"
fi

if [ ! "$FSYNC_EN" == "" ] && [ $FSYNC_EN == true ]; then
	echo "enabling fsync pin"
	set_param fsync_en "true"
else
	echo "leaving fsync pin off"
fi



echo "copying required camera drivers in place"
rm -rf /usr/lib/camera/com.qti.sensormodule*
for f in ${BIN_LIST[@]}; do
	echo $f
	cp $f ${CAM_LIB_DIR}
done



# make sure changes got written to disk!!!
sync

## Enable voxl-camera-server
if [ $ENABLE_CAMERA_SERVER == "1" ]; then
	echo "enabling voxl-camera-server"
	systemctl enable voxl-camera-server
	echo ""
	PRINT_GREEEN_LINE
	echo -e "${GRN}Done Configuring Cameras${RESET_ALL}"
	echo -e "${GRN}Please power cycle VOXL to load new drivers${RESET_ALL}"
	echo -e "${GRN}camera server will start on next reboot${RESET_ALL}"
	PRINT_GREEEN_LINE
else
	echo "disabling voxl-camera-server"
	systemctl disable voxl-camera-server
	echo ""
	PRINT_GREEEN_LINE
	echo -e "${GRN}Done Configuring Cameras${RESET_ALL}"
	echo -e "${GRN}voxl-camera-server has been left disabled as requested${RESET_ALL}"
	echo -e "${GRN}Please power cycle VOXL to load new drivers${RESET_ALL}"
	PRINT_GREEEN_LINE
fi



exit 0
