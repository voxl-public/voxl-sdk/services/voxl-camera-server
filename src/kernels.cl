/*******************************************************************************
 * Copyright 2025 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with hardware devices provided by
 *    ModalAI® Inc. Reverse engineering or copying this code for use on hardware 
 *    not manufactured by ModalAI is not permitted.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
 
//RGB to YUV
#define RGBA2Y(rgb) ( 0.299*rgb.x + 0.587*rgb.y + 0.114*rgb.z + 0.0625)
#define RGBA2U(rgb) (-0.147*rgb.x - 0.289*rgb.y + 0.436*rgb.z + 0.5)
#define RGBA2V(rgb) ( 0.615*rgb.x - 0.515*rgb.y - 0.100*rgb.z + 0.5)

//RGB to YCbCr
//#define RGBA2Y(rgba) ( 0.2990*rgba.x + 0.5870*rgba.y + 0.11400*rgba.z + 0.0625)
//#define RGBA2U(rgba) (-0.1687*rgba.x - 0.3313*rgba.y + 0.50000*rgba.z + 0.5000)
//#define RGBA2V(rgba) ( 0.5000*rgba.x - 0.4187*rgba.y - 0.08131*rgba.z + 0.5000)

/*
typedef struct __attribute__ ((packed))
{
    int src_width;
    int src_height;
    int src_line_stride;
    int src_plane_stride;
    int dst_width;
    int dst_height;
    int dst_line_stride;    //pixels
    int dst_plane_stride;   //lines
    float gain_r;
    float gain_g;
    float gain_b;
    float gamma;
    float R00;
    float R01;
    float R10;
    float R11;
} TestParams;
*/

typedef struct __attribute__ ((packed))
{
    float R00;
    float R01;
    float R10;
    float R11;
    int w;
    int h;
} RotParams;

typedef struct __attribute__ ((packed))
{
    float gain_r;
    float gain_g;
    float gain_b;
} GainParams;

/*
static inline float2 ROT2D(float2 xy, RotParams p)
{
    const float w4 = p.w/4;
    const float h4 = p.h/4;
    const float zoom = 0.5 + 0.5*(p.R00 + 1.0)/2.0;

    float2 out = (float2)(((xy.x-w4) * p.R00 + (xy.y-h4) * p.R01)*zoom + w4, ((xy.x-w4) * p.R10 + (xy.y-h4) * p.R11)*zoom + h4);
    return out;
}
*/


float2 ROT2D(float2 xy, float R00, float R01, float R10, float R11)
{
    const float w4 = 1280/4;
    const float h4 = 720/4;
    const float zoom = 0.5 + 0.5*(R00 + 1.0)/2.0;

    float2 out = (float2)(((xy.x-w4) * R00 + (xy.y-h4) * R01)*zoom + w4, ((xy.x-w4) * R10 + (xy.y-h4) * R11)*zoom + h4);
    //float2 out = (float2)(((xy.x-w4)) + w4, ((xy.y-h4)) + h4);
    return out;
}


//#define ROT2D(xy, R00, R01, R10, R11) ((float2)(((xy.x-1280/4) * R00 + (xy.y-720/4) * R01) + 1280/4, ((xy.x-1280/4) * R10 + (xy.y-720/4) * R11) + 720/4))
//#define ROT2D(xy) ((float2)(((xy.x-1280.0/4.0)) + 1280.0/4.0, ((xy.y-720.0/4.0)) + 720.0/4.0))
//#define ROT2D(xy) ((float2)(xy.x-1.0, xy.y-1.0))
//#define ROT2D(xy) (xy + (float2)(0.5, 0.5))

#define CLAMPF_0255(X) clamp((x), 0.0f, 255.0f)

float2 distort_coord(float2 coord, int src_width, int src_height)
{
    const float fx_new = 800;
    const float fy_new = fx_new;
    const float cx_new = src_width / 4;
    const float cy_new = src_height / 4;

    float xwu = (coord.x - cx_new) / fx_new;
    float ywu = (coord.y - cy_new) / fy_new;

    float r     = native_sqrt(xwu*xwu + ywu*ywu);
    //float theta = atan2(r, 1);
    float theta = atan2pi(r, 1);

    float D[4] = {0.0, 0.0, 0.0, 0.0};

    // Perform distortion (forward map)
    float theta_d = theta*(1 + D[0]*native_powr(theta,2) + D[1]*native_powr(theta,4) +
                        D[2]*native_powr(theta,6) + D[3]*native_powr(theta,8));


    // now find the world-frame location when distorted
    float xwd = (theta_d/r)*xwu;
    float ywd = (theta_d/r)*ywu;

    // convert the distorted world coordinate to pixel coordinate
    // in the original distorted image
    //float xd = (fx * xwd) + cx;
    //float yd = (fy * ywd) + cy;

    //float xd = (fx_new*1.3 * xwd) + cx_new;
    //float yd = (fy_new*1.3 * ywd) + cy_new;
    float xd = (fx_new*4.0 * xwd) + cx_new;
    float yd = (fy_new*4.0 * ywd) + cy_new;


    return (float2)(xd,yd);
}

float get_lsc_gain(float2 coord, int src_width, int src_height)
{
    float x = coord.x - src_width / 2;
    float y = coord.y - src_height / 2;
    float r = native_sqrt(x*x + y*y);
    //float g = 1.066e-09*(r*r*r*r) -1.042e-06*(r*r*r) - 0.0001265*(r*r) - 0.03029*r + 220.0;
    float g = 1.1e-09*(r*r*r*r) -1.042e-06*(r*r*r) - 0.0001265*(r*r) - 0.03029*r + 220.0;
    //float g = 1.0e-09*(r*r*r*r) -1.042e-06*(r*r*r) - 0.00013*(r*r) - 0.03029*r + 220.0;
    float o = (220.0f / g);

    o = clamp(o,0.0,8.0);
    return o;
}


//12-bit images uses 3 bytes to store 2 pixels
//load 24 bytes and save 16 bytes
__kernel void convert_mono_12_to_8_bit_( __read_only  __global uchar * image_in,
                                        __write_only __global uchar * image_out,
                                        int src_width, int src_height, int src_stride,
                                        int dst_stride)
{
    const uint input_chunk_size  = 24;
    const uint output_chunk_size = 16;

    int image_column = get_global_id(0)*output_chunk_size;
    int image_row    = get_global_id(1);

    const uint row_offset  = image_row * src_stride;
    const uint chunk_idx   = image_column / output_chunk_size;

    const uint row_offset_dst  = image_row * dst_stride;

    //load 24 bytes in two chunks (3x 8 bytes)
    uint chunk_src_offset8 = (row_offset + chunk_idx * input_chunk_size)/8;
    uchar8 v8a             = vload8(chunk_src_offset8,   image_in);
    uchar8 v8b             = vload8(chunk_src_offset8+1, image_in);
    uchar8 v8c             = vload8(chunk_src_offset8+2, image_in);

    uchar16 v16;
    v16.s0123   = v8a.s0134;
    v16.s4567   = (uchar4)(v8a.s67, v8b.s12);
    v16.s89ab   = (uchar4)(v8b.s457, v8c.s0);
    v16.scdef   = v8c.s2356;

    uint write_offset16 = row_offset_dst / output_chunk_size + chunk_idx;
    vstore16(v16,write_offset16,image_out);
}

__kernel void convert_mono_12_to_8_bit( __read_only  __global uchar * image_in,
                                        __write_only __global uchar * image_out,
                                        int src_width, int src_height, int src_stride,
                                        int dst_stride)
{
    const uint input_chunk_size  = 3;
    const uint output_chunk_size = 2;

    uint image_column     = get_global_id(0)*output_chunk_size;
    uint image_row        = get_global_id(1);
    uint row_offset       = image_row * src_stride;
    uint chunk_idx        = image_column / output_chunk_size;
    uint row_offset_dst   = image_row * dst_stride;
    uint chunk_src_offset = (row_offset + chunk_idx * input_chunk_size);
    uint write_offset     = row_offset_dst + chunk_idx*output_chunk_size;

/*
    //no LSC
    uchar2 ab    = (uchar2)(image_in[chunk_src_offset], image_in[chunk_src_offset+1]);
    uchar2 * out = &(image_out[write_offset]);
    *out         = ab;
*/

/*
    //8-bit LSC
    float2 ab      = (float2)(image_in[chunk_src_offset], image_in[chunk_src_offset+1]);
    float lsc_gain = get_lsc_gain((float2)(image_column,image_row), src_width, src_height);
    ab *= lsc_gain;
    ab  = clamp(ab, (float2)(0,0), (float2)(255.0,255.0));

    uchar2 * out = &(image_out[write_offset]);
    *out = (uchar2)(ab.x, ab.y);

*/

    float2 ab      = (float2)(image_in[chunk_src_offset]*16, image_in[chunk_src_offset+1]*16);  //MSB of two pixels, multiply by 16
    uchar c        = image_in[chunk_src_offset+2]; //4 LSB of each pixel
    
    ab.x += c & 0x0F; //add 4 LSB to each pixel
    ab.y += c / 16;

    //HACK use the same gain for both pixels (interpolate)
    float lsc_gain_a = get_lsc_gain((float2)(image_column+0.5,image_row+0.5), src_width, src_height);
    //float lsc_gain_b = get_lsc_gain((float2)(image_column+1,image_row), src_width, src_height);

    //float2 lsc_gain = (float2)(lsc_gain_a, lsc_gain_b);
    float2 lsc_gain = (float2)(lsc_gain_a, lsc_gain_a);

    //disable LSC
    lsc_gain = (float2)(1.0, 1.0);

    ab = (ab*lsc_gain)/16.0;
    ab = clamp(ab, (float2)(0,0), (float2)(255.0,255.0));


    uchar2 * out = &(image_out[write_offset]);
    *out = (uchar2)(ab.x, ab.y);


/*
    uchar a = image_in[chunk_src_offset];
    uchar b = image_in[chunk_src_offset+1];
    uchar2 * out = &(image_out[write_offset]);
    *out = (uchar2)(a,b);
*/

/*
    uchar a = image_in[chunk_src_offset];
    uchar b = image_in[chunk_src_offset+1];
    
    image_out[write_offset]   = a;
    image_out[write_offset+1] = b;
*/
}

__kernel void convert_mono_10_to_8_bit( __read_only  __global uchar * image_in,
                                        __write_only __global uchar * image_out,
                                        int src_width, int src_height, int src_stride,
                                        int dst_stride)
{
  const uint input_chunk_size  = 20;
  const uint output_chunk_size = 16;

  int image_column      = get_global_id(0)*output_chunk_size;
  int image_row         = get_global_id(1);

  uint row_offset       = image_row * src_stride;
  uint chunk_idx        = image_column / output_chunk_size;

  uint row_offset_dst   = image_row * dst_stride;
  uint chunk_src_offset = (row_offset + chunk_idx * input_chunk_size)/4;
  //uint write_offset     = row_offset_dst + chunk_idx*output_chunk_size;
  uint write_offset     = row_offset_dst/output_chunk_size + chunk_idx;

  uchar4  v0  = vload4(chunk_src_offset+0,image_in);
  uchar4  v1  = vload4(chunk_src_offset+1,image_in);
  uchar4  v2  = vload4(chunk_src_offset+2,image_in);
  uchar4  v3  = vload4(chunk_src_offset+3,image_in);
  uchar4  v4  = vload4(chunk_src_offset+4,image_in);

  uchar16 v16;
  v16.s0123 = v0.s0123;
  v16.s4567 = (uchar4)(v1.s123, v2.s0);
  v16.s89ab = (uchar4)(v2.s23,v3.s01);
  v16.scdef = (uchar4)(v3.s3, v4.s012);
  
  vstore16(v16,write_offset,image_out);
}



__kernel void bayer_to_yuv_color( __read_only  image2d_t           bayer_image,
                                                 sampler_t           sampler,
                                    __write_only __global  uchar *   image_out,
                                    float gain_r, float gain_g, float gain_b, float gamma,
                                    int src_offset_x, src_offset_y,
                                    int src_width, int src_height, int src_rggb, 
                                    int dst_width, int dst_height,
                                    int dst_line_stride, int dst_plane_stride)
{
    const int    wid_x          = get_global_id(0);
    const int    wid_y          = get_global_id(1);
    const float2 coord          = (float2)(wid_x, wid_y);   //+0.5
    const float2 src_offset     = (float2)(src_offset_x/2,src_offset_y/2);

    const float zoomx           = ((float)src_width) / dst_width;
    const float zoomy           = ((float)src_height) / dst_height;
    const float2 zoom           = (float2)(zoomx,zoomy);
    float2 coord00 = src_offset + (coord)*zoom;
    float2 coord01 = src_offset + (coord + (float2)(0.5, 0.0)) * zoom;
    float2 coord10 = src_offset + (coord + (float2)(0.0, 0.5)) * zoom;
    float2 coord11 = src_offset + (coord + (float2)(0.5, 0.5)) * zoom;

    float4 bayer_pixels[] = {
        read_imagef(bayer_image, sampler, coord00),
        read_imagef(bayer_image, sampler, coord01),
        read_imagef(bayer_image, sampler, coord10),
        read_imagef(bayer_image, sampler, coord11)
    };

    //compensate for subtraction of pedestal
    const float ped      = 16.0/256.0;          //IMX412 has pedestal value of 64 for 10bit output, so 16.0 for 8 bit
    const float ped_gain = 1.0 / (1.0 - ped);   //after subtracting the pedestal, this gain will be used to scale the range back to 0 .. 1.0
    
    gain_r *= ped_gain;
    gain_g *= ped_gain;
    gain_b *= ped_gain;

    //sample the 10- or 12-bit RGGB image with interpolation to get 4 RGGB pixels
    //coordinates for reading 10- or 12-bit RGGB are half the width and height
    //underlying logic assumes the 4 pixels in bayer image make up a single 4-color pixel
    
    //convert 4 bayer RGGB pixels to 4 RGB pixels and apply white balance gains
    float3 yuv_pixels[4];

    const float inv_gamma = 1.0 / gamma;

    //no gamma correction
    for (int i=0; i<4; i++)
    {
        //float p[3] = {bayer_pixels[i].w, bayer_pixels[i].x, bayer_pixels[i].w};
        float pix_r = bayer_pixels[i].x;
        float pix_b = bayer_pixels[i].w;
        
        //this if without else seems to be adding almost zero overhead (if-else significantly slows things down)
        if (src_rggb == 0){
            pix_r = bayer_pixels[i].w;
            pix_b = bayer_pixels[i].x;
        }

        float3 rgb_pixel;
        //rgb_pixel = clamp((float3)(gain_r*(p[src_rggb]-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z-ped-ped), gain_b*(p[src_rggb+1]-ped)), 0.0, 1.0);
        //rgb_pixel = clamp((float3)(gain_r*(bayer_pixels[i].x-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z-ped-ped), gain_b*(bayer_pixels[i].w-ped)), 0.0, 1.0);
        rgb_pixel = native_powr(clamp((float3)(gain_r*(pix_r-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z-ped-ped), gain_b*(pix_b-ped)), 0.0, 1.0),inv_gamma);

        yuv_pixels[i] = (float3)(RGBA2Y(rgb_pixel), RGBA2U(rgb_pixel), RGBA2V(rgb_pixel));
        yuv_pixels[i] *= 255.0;  //convert to 0-255 range
    }

    const int line_stride     = dst_line_stride;
    const int uv_plane_offset = dst_plane_stride * line_stride;

    //calculate the coordinates of the 4 Y pixels in output image
    int write_coord0 = wid_y*line_stride*2 + wid_x *2;
    //int write_coord1 = wid_y*line_stride*2 + wid_x *2 +1;
    int write_coord2 = (wid_y*2+1)*line_stride + wid_x *2;
    //int write_coord3 = (wid_y*2+1)*line_stride + wid_x *2 +1;

    //calculate the coordinates of the U and V pixels
    int write_coord_u = uv_plane_offset + wid_y*line_stride + wid_x *2;
    //int write_coord_v = write_coord_u + 1;
    
    //calculate U and V values, which should be average of the 4 pixels
    float u_av = (yuv_pixels[0].y + yuv_pixels[1].y + yuv_pixels[2].y + yuv_pixels[3].y)*0.25;
    float v_av = (yuv_pixels[0].z + yuv_pixels[1].z + yuv_pixels[2].z + yuv_pixels[3].z)*0.25;
    
    //clamp and write the data to output image
    //writing uchar2 is a bit faster than individual bytes
    uchar2 yy0 = convert_uchar2_sat((float2)(yuv_pixels[0].x, yuv_pixels[1].x));
    uchar2 yy1 = convert_uchar2_sat((float2)(yuv_pixels[2].x, yuv_pixels[3].x));
    uchar2 uv  = convert_uchar2_sat((float2)(u_av, v_av));

    uchar2 * yy0_ptr = (uchar2 *)&(image_out[write_coord0]);
    uchar2 * yy1_ptr = (uchar2 *)&(image_out[write_coord2]);
    uchar2 * uv_ptr  = (uchar2 *)&(image_out[write_coord_u]);

    *yy0_ptr = yy0;
    *yy1_ptr = yy1;
    *uv_ptr = uv;
}





__kernel void bayer_to_yuv_color_old( __read_only  image2d_t           bayer_image,
                                                 sampler_t           sampler,
                                    __write_only __global  uchar *   image_out,
                                    float gain_r, float gain_g, float gain_b, float gamma,
                                    int src_offset_x, src_offset_y,
                                    int src_width, int src_height, int src_rggb, 
                                    int dst_width, int dst_height,
                                    int dst_line_stride, int dst_plane_stride)
{
    const int    wid_x          = get_global_id(0);
    const int    wid_y          = get_global_id(1);
    const float2 coord          = (float2)(wid_x, wid_y);   //+0.5
    const float2 src_offset     = (float2)(src_offset_x/2,src_offset_y/2);

    const float zoomx  = ((float)src_width) / dst_width;
    const float zoomy  = ((float)src_height) / dst_height;
    const float2 zoom  = (float2)(zoomx,zoomy);
    float2 coord00 = src_offset + (coord)*zoom;
    float2 coord01 = src_offset + (coord + (float2)(0.5, 0.0)) * zoom;
    float2 coord10 = src_offset + (coord + (float2)(0.0, 0.5)) * zoom;
    float2 coord11 = src_offset + (coord + (float2)(0.5, 0.5)) * zoom;

/*
    coord00 = distort_coord(coord00,src_width,src_height);
    coord01 = distort_coord(coord01,src_width,src_height);
    coord10 = distort_coord(coord10,src_width,src_height);
    coord11 = distort_coord(coord11,src_width,src_height);
*/

    float4 bayer_pixels[] = {
        read_imagef(bayer_image, sampler, coord00),
        read_imagef(bayer_image, sampler, coord01),
        read_imagef(bayer_image, sampler, coord10),
        read_imagef(bayer_image, sampler, coord11)
    };

    //compensate for subtraction of pedestal
    const float ped      = 16.0/256.0;          //IMX412 has pedestal value of 64 for 10bit output, so 16.0 for 8 bit
    const float ped_gain = 1.0 / (1.0 - ped);   //after subtracting the pedestal, this gain will be used to scale the range back to 0 .. 1.0
    
    gain_r *= ped_gain;
    gain_g *= ped_gain;
    gain_b *= ped_gain;

    //AR0144 LSC test
    if (0){
        float lsc_gain = get_lsc_gain(coord * 2.0, src_width, src_height);
        gain_r *= lsc_gain;
        gain_g *= lsc_gain;
        gain_b *= lsc_gain;
    }

    //gp.gain_r *= ped_gain;
    //gp.gain_g *= ped_gain;
    //gp.gain_b *= ped_gain;

    //const float gain_r = _gain_r;
    //const float gain_g = _gain_g;
    //const float gain_b = _gain_b;

    //sample the 10-bit RGGB image with interpolation to get 4 RGGB pixels
    //coordinates for reading 10-bit RGGB are half the width and height
    //underlying logic assumes the 4 pixels in bayer image make up a single 4-color pixel

    
    //const float3 gain_vec = (float3)(gain_r, 0.5*gain_g, gain_b);  //using gain vec is a bit slower

    //subtract black level pedestal
    /*
    //now the pedestal will be subtracted during gain application
    const float4 pedf4   = (float4)(ped, ped, ped,ped);
    bayer_pixels[0] -= pedf4;
    bayer_pixels[1] -= pedf4;
    bayer_pixels[2] -= pedf4;
    bayer_pixels[3] -= pedf4;
    */

    const float3 MIN_RGB = (float3)(0.0, 0.0, 0.0);
    const float3 MAX_RGB = (float3)(1.0, 1.0, 1.0);

    const float3 gamma_vec = (float3)(1.0/gamma, 1.0/gamma, 1.0/gamma);
    
    //convert 4 bayer RGGB pixels to 4 RGB pixels and apply white balance gains
    float3 rgb_pixels[4];

    if (1) //(gamma < 1.05)
    {
        //no gamma correction
        /*
        rgb_pixels[0] = clamp((float3)(gain_r*bayer_pixels[0].x, gain_g*0.5f * (bayer_pixels[0].y + bayer_pixels[0].z), gain_b*bayer_pixels[0].w), MIN_RGB, MAX_RGB);
        rgb_pixels[1] = clamp((float3)(gain_r*bayer_pixels[1].x, gain_g*0.5f * (bayer_pixels[1].y + bayer_pixels[1].z), gain_b*bayer_pixels[1].w), MIN_RGB, MAX_RGB);
        rgb_pixels[2] = clamp((float3)(gain_r*bayer_pixels[2].x, gain_g*0.5f * (bayer_pixels[2].y + bayer_pixels[2].z), gain_b*bayer_pixels[2].w), MIN_RGB, MAX_RGB);
        rgb_pixels[3] = clamp((float3)(gain_r*bayer_pixels[3].x, gain_g*0.5f * (bayer_pixels[3].y + bayer_pixels[3].z), gain_b*bayer_pixels[3].w), MIN_RGB, MAX_RGB);
        */

        for (int i=0; i<4; i++)
        {
            if (1) //(src_rggb)
            {
                float pix_r = bayer_pixels[i].x;
                float pix_b = bayer_pixels[i].w;
                
                //this if without else seems to be adding almost zero overhead (if-else significantly slows things down)
                if (src_rggb == 0){
                    pix_r = bayer_pixels[i].w;
                    pix_b = bayer_pixels[i].x;
                }

                
                rgb_pixels[i] = clamp((float3)(gain_r*(pix_r-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z-ped-ped), gain_b*(pix_b-ped)), MIN_RGB, MAX_RGB);
                //rgb_pixels[i] = clamp((float3)(gain_r*(bayer_pixels[i].x-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z-ped-ped), gain_b*(bayer_pixels[i].w-ped)), MIN_RGB, MAX_RGB);
            }
            else
            {
                rgb_pixels[i] = clamp((float3)(gain_r*(bayer_pixels[i].w-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z-ped-ped), gain_b*(bayer_pixels[i].x-ped)), MIN_RGB, MAX_RGB);
            }
            
        }
        
        /*
        if (1) //src_rggb)
        {
            rgb_pixels[0] = clamp((float3)(gain_r*(bayer_pixels[0].x-ped), gain_g*0.5f * (bayer_pixels[0].y + bayer_pixels[0].z-ped-ped), gain_b*(bayer_pixels[0].w-ped)), MIN_RGB, MAX_RGB);
            rgb_pixels[1] = clamp((float3)(gain_r*(bayer_pixels[1].x-ped), gain_g*0.5f * (bayer_pixels[1].y + bayer_pixels[1].z-ped-ped), gain_b*(bayer_pixels[1].w-ped)), MIN_RGB, MAX_RGB);
            rgb_pixels[2] = clamp((float3)(gain_r*(bayer_pixels[2].x-ped), gain_g*0.5f * (bayer_pixels[2].y + bayer_pixels[2].z-ped-ped), gain_b*(bayer_pixels[2].w-ped)), MIN_RGB, MAX_RGB);
            rgb_pixels[3] = clamp((float3)(gain_r*(bayer_pixels[3].x-ped), gain_g*0.5f * (bayer_pixels[3].y + bayer_pixels[3].z-ped-ped), gain_b*(bayer_pixels[3].w-ped)), MIN_RGB, MAX_RGB);
        }
        else
        {
            rgb_pixels[0] = clamp((float3)(gain_r*(bayer_pixels[0].w-ped), gain_g*0.5f * (bayer_pixels[0].y + bayer_pixels[0].z-ped-ped), gain_b*(bayer_pixels[0].x-ped)), MIN_RGB, MAX_RGB);
            rgb_pixels[1] = clamp((float3)(gain_r*(bayer_pixels[1].w-ped), gain_g*0.5f * (bayer_pixels[1].y + bayer_pixels[1].z-ped-ped), gain_b*(bayer_pixels[1].x-ped)), MIN_RGB, MAX_RGB);
            rgb_pixels[2] = clamp((float3)(gain_r*(bayer_pixels[2].w-ped), gain_g*0.5f * (bayer_pixels[2].y + bayer_pixels[2].z-ped-ped), gain_b*(bayer_pixels[2].x-ped)), MIN_RGB, MAX_RGB);
            rgb_pixels[3] = clamp((float3)(gain_r*(bayer_pixels[3].w-ped), gain_g*0.5f * (bayer_pixels[3].y + bayer_pixels[3].z-ped-ped), gain_b*(bayer_pixels[3].x-ped)), MIN_RGB, MAX_RGB);
        }
        */
        
    }
    else
    {
        //with gamma correction
        rgb_pixels[0] = native_powr(clamp((float3)(gain_r*bayer_pixels[0].x, gain_g*0.5f * (bayer_pixels[0].y + bayer_pixels[0].z), gain_b*bayer_pixels[0].w), MIN_RGB, MAX_RGB), gamma_vec);
        rgb_pixels[1] = native_powr(clamp((float3)(gain_r*bayer_pixels[1].x, gain_g*0.5f * (bayer_pixels[1].y + bayer_pixels[1].z), gain_b*bayer_pixels[1].w), MIN_RGB, MAX_RGB), gamma_vec);
        rgb_pixels[2] = native_powr(clamp((float3)(gain_r*bayer_pixels[2].x, gain_g*0.5f * (bayer_pixels[2].y + bayer_pixels[2].z), gain_b*bayer_pixels[2].w), MIN_RGB, MAX_RGB), gamma_vec);
        rgb_pixels[3] = native_powr(clamp((float3)(gain_r*bayer_pixels[3].x, gain_g*0.5f * (bayer_pixels[3].y + bayer_pixels[3].z), gain_b*bayer_pixels[3].w), MIN_RGB, MAX_RGB), gamma_vec);
    }

/*
    //apply RGB gain, clamp, apply gamma correction
    const float3 rgb_pixels[] = {
        native_powr(clamp((float3)(gain_r*bayer_pixels[0].x, gain_g*0.5f * (bayer_pixels[0].y + bayer_pixels[0].z), gain_b*bayer_pixels[0].w), MIN_RGB, MAX_RGB), gamma_vec),
        native_powr(clamp((float3)(gain_r*bayer_pixels[1].x, gain_g*0.5f * (bayer_pixels[1].y + bayer_pixels[1].z), gain_b*bayer_pixels[1].w), MIN_RGB, MAX_RGB), gamma_vec),
        native_powr(clamp((float3)(gain_r*bayer_pixels[2].x, gain_g*0.5f * (bayer_pixels[2].y + bayer_pixels[2].z), gain_b*bayer_pixels[2].w), MIN_RGB, MAX_RGB), gamma_vec),
        native_powr(clamp((float3)(gain_r*bayer_pixels[3].x, gain_g*0.5f * (bayer_pixels[3].y + bayer_pixels[3].z), gain_b*bayer_pixels[3].w), MIN_RGB, MAX_RGB), gamma_vec)
    };
*/
/*
    float3 rgb_pixels[] = {
        clamp((float3)(gain_r*bayer_pixels[0].x, gain_g*0.5f * (bayer_pixels[0].y + bayer_pixels[0].z), gain_b*bayer_pixels[0].w), MIN_RGB, MAX_RGB),
        clamp((float3)(gain_r*bayer_pixels[1].x, gain_g*0.5f * (bayer_pixels[1].y + bayer_pixels[1].z), gain_b*bayer_pixels[1].w), MIN_RGB, MAX_RGB),
        clamp((float3)(gain_r*bayer_pixels[2].x, gain_g*0.5f * (bayer_pixels[2].y + bayer_pixels[2].z), gain_b*bayer_pixels[2].w), MIN_RGB, MAX_RGB),
        clamp((float3)(gain_r*bayer_pixels[3].x, gain_g*0.5f * (bayer_pixels[3].y + bayer_pixels[3].z), gain_b*bayer_pixels[3].w), MIN_RGB, MAX_RGB)
    };
*/


    /*
    //using gain vec is a bit slower
    const float3 rgb_pixels[] = {
        clamp((float3)(bayer_pixels[0].x, (bayer_pixels[0].y + bayer_pixels[0].z), bayer_pixels[0].w) * gain_vec, MIN_RGB, MAX_RGB),
        clamp((float3)(bayer_pixels[1].x, (bayer_pixels[1].y + bayer_pixels[1].z), bayer_pixels[1].w) * gain_vec, MIN_RGB, MAX_RGB),
        clamp((float3)(bayer_pixels[2].x, (bayer_pixels[2].y + bayer_pixels[2].z), bayer_pixels[2].w) * gain_vec, MIN_RGB, MAX_RGB),
        clamp((float3)(bayer_pixels[3].x, (bayer_pixels[3].y + bayer_pixels[3].z), bayer_pixels[3].w) * gain_vec, MIN_RGB, MAX_RGB)
    };
    */

/*
    const float3 rgb_pixels[] = {
        GAMMA(clamp((float3)(gain_r*bayer_pixels[0].x, gain_g*0.5f * (bayer_pixels[0].y + bayer_pixels[0].z), gain_b*bayer_pixels[0].w), MIN_RGB, MAX_RGB), gamma_vec),
        GAMMA(clamp((float3)(gain_r*bayer_pixels[1].x, gain_g*0.5f * (bayer_pixels[1].y + bayer_pixels[1].z), gain_b*bayer_pixels[1].w), MIN_RGB, MAX_RGB), gamma_vec),
        GAMMA(clamp((float3)(gain_r*bayer_pixels[2].x, gain_g*0.5f * (bayer_pixels[2].y + bayer_pixels[2].z), gain_b*bayer_pixels[2].w), MIN_RGB, MAX_RGB), gamma_vec),
        GAMMA(clamp((float3)(gain_r*bayer_pixels[3].x, gain_g*0.5f * (bayer_pixels[3].y + bayer_pixels[3].z), gain_b*bayer_pixels[3].w), MIN_RGB, MAX_RGB), gamma_vec)
    };
*/

    



    /*
    //no clamping results in artifacts
    const float3 rgb_pixels[] = {
        (float3)(gain_r*bayer_pixels[0].x, gain_g*0.5f * (bayer_pixels[0].y + bayer_pixels[0].z), gain_b*bayer_pixels[0].w),
        (float3)(gain_r*bayer_pixels[1].x, gain_g*0.5f * (bayer_pixels[1].y + bayer_pixels[1].z), gain_b*bayer_pixels[1].w),
        (float3)(gain_r*bayer_pixels[2].x, gain_g*0.5f * (bayer_pixels[2].y + bayer_pixels[2].z), gain_b*bayer_pixels[2].w),
        (float3)(gain_r*bayer_pixels[3].x, gain_g*0.5f * (bayer_pixels[3].y + bayer_pixels[3].z), gain_b*bayer_pixels[3].w)
    };
    */
    



    //convert RGB to YUV
    const float3 yuv_pixels[] = {
        (float3)(RGBA2Y(rgb_pixels[0]), RGBA2U(rgb_pixels[0]), RGBA2V(rgb_pixels[0])),
        (float3)(RGBA2Y(rgb_pixels[1]), RGBA2U(rgb_pixels[1]), RGBA2V(rgb_pixels[1])),
        (float3)(RGBA2Y(rgb_pixels[2]), RGBA2U(rgb_pixels[2]), RGBA2V(rgb_pixels[2])),
        (float3)(RGBA2Y(rgb_pixels[3]), RGBA2U(rgb_pixels[3]), RGBA2V(rgb_pixels[3]))
    };
        

    //1280x720: encoder alignment: line stride: 1536, plane stride: 1024
    //const int line_stride     = 1536; //params.dst_line_stride;
    //const int uv_plane_offset = 1024 * line_stride; //params.dst_plane_stride * line_stride;
    const int line_stride     = dst_line_stride;
    const int uv_plane_offset = dst_plane_stride * line_stride;

    //calculate the coordinates of the 4 Y pixels in output image
    int write_coord0 = wid_y*line_stride*2 + wid_x *2;
    //int write_coord1 = wid_y*line_stride*2 + wid_x *2 +1;
    int write_coord2 = (wid_y*2+1)*line_stride + wid_x *2;
    //int write_coord3 = (wid_y*2+1)*line_stride + wid_x *2 +1;

    //calculate the coordinates of the U and V pixels
    int write_coord_u = uv_plane_offset + wid_y*line_stride + wid_x *2;
    //int write_coord_v = write_coord_u + 1;
    
    //calculate U and V values, which should be average of the 4 pixels
    float u_av = (yuv_pixels[0].y + yuv_pixels[1].y + yuv_pixels[2].y + yuv_pixels[3].y)*0.25;
    float v_av = (yuv_pixels[0].z + yuv_pixels[1].z + yuv_pixels[2].z + yuv_pixels[3].z)*0.25;
    
    //clamp and write the data to output image
    //writing uchar2 is a bit faster than individual bytes
    uchar2 yy0 = (uchar2)(clamp(yuv_pixels[0].x * 255.0, 0.0, 255.0),clamp(yuv_pixels[1].x * 255.0, 0.0, 255.0));
    uchar2 yy1 = (uchar2)(clamp(yuv_pixels[2].x * 255.0, 0.0, 255.0),clamp(yuv_pixels[3].x * 255.0, 0.0, 255.0));
    uchar2 uv  = (uchar2)(clamp(u_av * 255.0, 0.0, 255.0), clamp(v_av * 255.0, 0.0, 255.0));

    uchar2 * yy0_ptr = (uchar2 *)&(image_out[write_coord0]);
    uchar2 * yy1_ptr = (uchar2 *)&(image_out[write_coord2]);
    uchar2 * uv_ptr  = (uchar2 *)&(image_out[write_coord_u]);

    *yy0_ptr = yy0;
    *yy1_ptr = yy1;
    *uv_ptr = uv;

/*
    //slower version, writing one byte at a time
    image_out[write_coord0]  = clamp(yuv_pixels[0].x * 255.0, 0.0, 255.0);
    image_out[write_coord1]  = clamp(yuv_pixels[1].x * 255.0, 0.0, 255.0);
    image_out[write_coord2]  = clamp(yuv_pixels[2].x * 255.0, 0.0, 255.0);
    image_out[write_coord3]  = clamp(yuv_pixels[3].x * 255.0, 0.0, 255.0);
    image_out[write_coord_u] = clamp(u_av * 255.0, 0.0, 255.0);
    image_out[write_coord_v] = clamp(v_av * 255.0, 0.0, 255.0);
*/
}


__kernel void blur_image( __read_only            image2d_t            src_image,
                                                 sampler_t  sampler,
                          __write_only __global  uchar *              dst_image,
                          __read_only            qcom_weight_image_t  weight_image,
                                                 int                  dst_line_stride)
{
    int    wid_x          = get_global_id(0);
    int    wid_y          = get_global_id(1);
    float2 src_coord      = (float2)(wid_x, wid_y);

    float val = qcom_convolve_imagef(src_image, sampler, src_coord, weight_image).x;

    int dst_idx = wid_y * dst_line_stride + wid_x;
    dst_image[dst_idx] = clamp((float)(val*255.0), 0.0, 255.0);
}



__kernel void bayer_downscale_blur_to_yuv_color(__read_only  image2d_t           bayer_image,
                                                 sampler_t           sampler,
                                                __write_only __global  uchar *   image_out,
                                                int src_offset_x, int src_offset_y,
                                                int src_width, int src_height, int src_rggb, 
                                                int dst_width, int dst_height,
                                                int dst_line_stride, int dst_plane_stride, const qcom_box_size_t box_size)
{
    const int    wid_x          = get_global_id(0);
    const int    wid_y          = get_global_id(1);
    const float2 coord          = (float2)(wid_x, wid_y);
    const float2 src_offset     = (float2)(src_offset_x/2,src_offset_y/2);

    const float zoomx  = ((float)src_width) / dst_width;
    const float zoomy  = ((float)src_height) / dst_height;
    const float2 zoom  = (float2)(zoomx,zoomy);

    float2 coords[4];
    coords[0] = src_offset + (coord)*zoom;
    coords[1] = src_offset + (coord + (float2)(0.5, 0.0)) * zoom;
    coords[2] = src_offset + (coord + (float2)(0.0, 0.5)) * zoom;
    coords[3] = src_offset + (coord + (float2)(0.5, 0.5)) * zoom;

    float4 bayer_pixels[4];
    float3 rgb_pixels[4];
    float3 yuv_pixels[4];
    for (int i=0; i<4; i++) {
        bayer_pixels[i] = qcom_box_filter_imagef(bayer_image, sampler, coords[i], box_size),
        rgb_pixels[i]   = (float3)(bayer_pixels[i].x, 0.5*(bayer_pixels[i].y + bayer_pixels[i].z), bayer_pixels[i].w);   //convert bayer to RGB
        yuv_pixels[i]   = (float3)(RGBA2Y(rgb_pixels[i]), RGBA2U(rgb_pixels[i]), RGBA2V(rgb_pixels[i]));                 //convert RGB to YUV
    }

    const int line_stride     = dst_line_stride;
    const int uv_plane_offset = dst_plane_stride * line_stride;

    //calculate the coordinates of the 4 Y pixels in output image
    int write_coord_y0 = wid_y*line_stride*2 + wid_x *2;
    int write_coord_y2 = (wid_y*2+1)*line_stride + wid_x *2;

    //calculate the coordinates of the U and V pixels
    int write_coord_uv = uv_plane_offset + wid_y*line_stride + wid_x *2;
    //int write_coord_v = write_coord_u + 1;
    
    //calculate U and V values, which should be average of the 4 pixels
    float u_av = (yuv_pixels[0].y + yuv_pixels[1].y + yuv_pixels[2].y + yuv_pixels[3].y)*0.25;
    float v_av = (yuv_pixels[0].z + yuv_pixels[1].z + yuv_pixels[2].z + yuv_pixels[3].z)*0.25;
    
    //clamp and write the data to output image
    //writing uchar2 is a bit faster than individual bytes
    uchar2 yy0 = (uchar2)(clamp(yuv_pixels[0].x * 255.0, 0.0, 255.0),clamp(yuv_pixels[1].x * 255.0, 0.0, 255.0));
    uchar2 yy1 = (uchar2)(clamp(yuv_pixels[2].x * 255.0, 0.0, 255.0),clamp(yuv_pixels[3].x * 255.0, 0.0, 255.0));
    uchar2 uv  = (uchar2)(clamp(u_av * 255.0, 0.0, 255.0), clamp(v_av * 255.0, 0.0, 255.0));

    uchar2 * yy0_ptr = (uchar2 *)&(image_out[write_coord_y0]);
    uchar2 * yy1_ptr = (uchar2 *)&(image_out[write_coord_y2]);
    uchar2 * uv_ptr  = (uchar2 *)&(image_out[write_coord_uv]);

    *yy0_ptr = yy0;
    *yy1_ptr = yy1;
    *uv_ptr = uv;
}


__kernel void bayer_to_yuv_color_norm( __read_only  image2d_t           bayer_image,
                                                 sampler_t           sampler,
                                    __write_only __global  uchar *   image_out,
                                    float gain_r, float gain_g, float gain_b, float gamma,
                                    int src_offset_x, src_offset_y,
                                    int src_width, int src_height, int src_rggb, 
                                    int dst_width, int dst_height,
                                    int dst_line_stride, int dst_plane_stride,
                                    __read_only  image2d_t scratch_image)  //mean   /__write_only __global  uchar * scratch_image
{
    const int    wid_x          = get_global_id(0);
    const int    wid_y          = get_global_id(1);
    const float2 coord          = (float2)(wid_x, wid_y);
    const float2 src_offset     = (float2)(src_offset_x/2,src_offset_y/2);

    const float zoomx  = ((float)src_width) / dst_width;
    const float zoomy  = ((float)src_height) / dst_height;
    const float2 zoom  = (float2)(zoomx,zoomy);

    float2 coords[4];
    coords[0] = src_offset + (coord)*zoom;
    coords[1] = src_offset + (coord + (float2)(0.5, 0.0)) * zoom;
    coords[2] = src_offset + (coord + (float2)(0.0, 0.5)) * zoom;
    coords[3] = src_offset + (coord + (float2)(0.5, 0.5)) * zoom;

    //FIXME: how to deal with offset if any??
    float2 scratch_coord = (coord + (float2)(0.25, 0.25)) * 2 / 16;
    //float2 scratch_coord = (coord00/zoom*2) / 16;

/*
    float2 scratch_coord = coord00 / 16;
    int scratch_width  = dst_width / 16;
    int scratch_height = dst_height / 16;
    int scratch_idx    = (wid_y*2/16) * scratch_width + (wid_x*2/16);
    uchar scratch_val  = scratch_image[scratch_idx];
    float scratch_valf = (float)(scratch_val) / 255.0 * 1.5;
*/
    
    float4 scratch_valf4 = read_imagef(scratch_image, sampler, scratch_coord);
    float scratch_valf = clamp(scratch_valf4.x * 2.0,0.0,1.0);
    uchar scratch_val  = (uchar)(scratch_valf*255.0);

    

    //compensate for subtraction of pedestal
    const float ped      = 0*16.0/256.0;          //IMX412 has pedestal value of 64 for 10bit output, so 16.0 for 8 bit
    const float ped_gain = 1.0 / (1.0 - ped);   //after subtracting the pedestal, this gain will be used to scale the range back to 0 .. 1.0
    
    gain_r *= ped_gain;
    gain_g *= ped_gain;
    gain_b *= ped_gain;

    //sample the 10-bit or 12-bit RGGB image with interpolation to get 4 RGGB pixels
    //coordinates for reading 10-bit RGGB are half the width and height
    //underlying logic assumes the 4 pixels in bayer image make up a single 4-color pixel

    const float3 MIN_RGB = (float3)(0.0, 0.0, 0.0);
    const float3 MAX_RGB = (float3)(1.0, 1.0, 1.0);

    //const float3 gamma_vec = (float3)(1.0/gamma, 1.0/gamma, 1.0/gamma);
    
    //convert 4 bayer RGGB pixels to 4 RGB pixels, subtract pedestal and apply white balance gains
    float4 bayer_pixels[4];
    float3 rgb_pixels[4];
    float3 yuv_pixels[4];

    for (int i=0; i<4; i++)
    {
        bayer_pixels[i] = read_imagef(bayer_image, sampler, coords[i]);

        float pix_r = bayer_pixels[i].x;
        float pix_b = bayer_pixels[i].w;
        
        //this if without else seems to be adding almost zero overhead (if-else significantly slows things down)
        if (src_rggb == 0){
            pix_r = bayer_pixels[i].w;
            pix_b = bayer_pixels[i].x;
        }

        
        rgb_pixels[i] = clamp((float3)(gain_r*(pix_r-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z-ped-ped), gain_b*(pix_b-ped)), MIN_RGB, MAX_RGB);
        //rgb_pixels[i] = clamp((float3)(gain_r*(bayer_pixels[i].x-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z-ped-ped), gain_b*(bayer_pixels[i].w-ped)), MIN_RGB, MAX_RGB);

        //rgb_pixels[i] = native_powr(clamp((float3)(gain_r*(pix_r-ped), gain_g*0.5f * (bayer_pixels[i].y + bayer_pixels[i].z), (pix_b-ped)), MIN_RGB, MAX_RGB), gamma_vec);

        //convert RGB to YUV
        yuv_pixels[i] = (float3)(RGBA2Y(rgb_pixels[i]), RGBA2U(rgb_pixels[i]), RGBA2V(rgb_pixels[i]));
        yuv_pixels[i].x /= scratch_valf;
    }


    const int line_stride     = dst_line_stride;
    const int uv_plane_offset = dst_plane_stride * line_stride;

    //calculate the coordinates of the 4 Y pixels in output image
    int write_coord_y0 = wid_y*line_stride*2 + wid_x *2;
    int write_coord_y2 = (wid_y*2+1)*line_stride + wid_x *2;

    //clamp and write the data to output image
    //writing uchar2 is a bit faster than individual bytes
    uchar2 yy0 = (uchar2)(clamp(yuv_pixels[0].x * 255.0, 0.0, 255.0),clamp(yuv_pixels[1].x * 255.0, 0.0, 255.0));
    uchar2 yy1 = (uchar2)(clamp(yuv_pixels[2].x * 255.0, 0.0, 255.0),clamp(yuv_pixels[3].x * 255.0, 0.0, 255.0));
    
    uchar2 * yy0_ptr = (uchar2 *)&(image_out[write_coord_y0]);
    uchar2 * yy1_ptr = (uchar2 *)&(image_out[write_coord_y2]);
    
    //hack to see the scratch image
    //yy0 = (uchar2)(scratch_val,scratch_val);
    //yy1 = (uchar2)(scratch_val,scratch_val);
    
    *yy0_ptr = yy0;
    *yy1_ptr = yy1;
    

/*
    //calculate the coordinates of the U and V pixels
    int write_coord_uv = uv_plane_offset + wid_y*line_stride + wid_x *2;

    //calculate U and V values, which should be average of the 4 pixels
    float u_av = (yuv_pixels[0].y + yuv_pixels[1].y + yuv_pixels[2].y + yuv_pixels[3].y)*0.25;
    float v_av = (yuv_pixels[0].z + yuv_pixels[1].z + yuv_pixels[2].z + yuv_pixels[3].z)*0.25;
    uchar2 uv  = (uchar2)(clamp(u_av * 255.0, 0.0, 255.0), clamp(v_av * 255.0, 0.0, 255.0));
    uchar2 * uv_ptr  = (uchar2 *)&(image_out[write_coord_uv]);

    *uv_ptr = uv;
*/
}
